package orders

import (
	"context"
	"errors"
	"fmt"
	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson"
	"payment_orders/adapters/db"
	"payment_orders/application/orders"
)

type RepositoryOrders struct{
	database db.IMongoDBInstance
	collectionName string
}

func (instance *RepositoryOrders) OpenConnection() error {
	return instance.database.Open()
}

func (instance *RepositoryOrders) CloseConnection() error {
	return instance.database.Close()
}

func NewRepositoryOrders(database db.IMongoDBInstance) *RepositoryOrders {
	return &RepositoryOrders{
		database: database,
		collectionName: "Orders",
	}
}

func (instance *RepositoryOrders) GetById(ctx context.Context, id string) (*orders.MOrders, error) {
	result := &orders.MOrders{}

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	_ = collection.FindByIDWithCtx(ctx, id, result)

	collection = nil
	if result.ID.IsZero() {
		return nil, errors.New(fmt.Sprintf("Nenhum registro encontrado para o id: %v", id))
	}

	return result, nil
}

func (instance *RepositoryOrders) GetByOwnId(ctx context.Context, id string) (*orders.MOrders, error) {
	result := &orders.MOrders{}

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	cur := collection.FindOne(ctx, bson.M{ "ownId": id})

	err := cur.Decode(&result)

	collection = nil

	if err != nil {
		return nil, err
	}

	if result.ID.IsZero() {
		return nil, errors.New(fmt.Sprintf("Nenhum registro encontrado para o id: %v", id))
	}

	return result, nil
}

func (instance *RepositoryOrders) GetByUniqueIdentifier(ctx context.Context, uniqueIdentifier string) (*orders.MOrders, error) {
	result := &orders.MOrders{}

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	cur := collection.FindOne(ctx, bson.M{ "uniqueIdentifier": uniqueIdentifier})

	err := cur.Decode(&result)

	collection = nil

	if err != nil {
		return nil, err
	}

	if result.ID.IsZero() {
		return nil, errors.New(fmt.Sprintf("Nenhum registro encontrado para o uniqueIdentifier: %v", uniqueIdentifier))
	}

	return result, nil
}

func (instance *RepositoryOrders) Save(ctx context.Context, customer orders.IOrders) (*orders.MOrders, error) {
	if instance.database.GetDbType() == db.MongoDB {
		collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

		newModel := customer.GetModel()

		var err error
		if newModel.ID.IsZero() {
			err = collection.CreateWithCtx(ctx, newModel)
		} else {
			err = collection.UpdateWithCtx(ctx, newModel)
		}
		collection = nil

		if err != nil {
			return nil, err
		}

		return newModel.GetModel(), nil
	}

	return nil, nil
}

func (instance *RepositoryOrders) Remove(ctx context.Context, customer orders.IOrders) (*orders.MOrders, error) {
	if instance.database.GetDbType() == db.MongoDB {
		collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

		newModel := customer.GetModel()

		err := collection.DeleteWithCtx(ctx, newModel)

		collection = nil
		if err != nil {
			return nil, err
		}

		return newModel.GetModel(), nil
	}
	return nil, nil
}
