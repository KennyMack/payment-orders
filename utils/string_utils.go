package utils

import (
	"crypto/sha256"
	"encoding/hex"
	"html"
	"regexp"
	"strings"
)

func OnlyNumbers(value string) string {
	re := regexp.MustCompile("[0-9]+")
	return strings.Join(re.FindAllString(value, -1), "")
}

func StringInSlice(a string, list []string) bool {
    for _, b := range list {
        if b == a {
            return true
        }
    }
    return false
}

func Crypto(value string) string {
	h := sha256.New()

	h.Write([]byte(value))
	bs := h.Sum(nil)

	return hex.EncodeToString(bs)
}

func ScapeChars(value string) string {
	return html.EscapeString(value)
}
