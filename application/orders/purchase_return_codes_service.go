package orders

import "context"

type ServicePurchaseReturnCode struct {
	repository IReturnCodeRepository
}

func NewServicePurchaseReturnCode(repository IReturnCodeRepository) *ServicePurchaseReturnCode {
	return &ServicePurchaseReturnCode{
		repository: repository,
	}
}

func (instance *ServicePurchaseReturnCode) OpenConnection() error {
	return instance.repository.OpenConnection()
}

func (instance *ServicePurchaseReturnCode) CloseConnection() error {
	return instance.repository.CloseConnection()
}

func (instance *ServicePurchaseReturnCode) GetByLR(ctx context.Context, lr string) (*MReturnCode, error) {
	res, err := instance.repository.GetByLR(ctx, lr)

	if err != nil {
		return nil, err
	}

	return res, nil
}
