package sync

import "payment_orders/adapters/queue"

type AppSyncQueue struct {
	NewMessage   chan *queue.ContentMessageInstance
}

func MakeNewAppSyncQueue() *AppSyncQueue {
	return &AppSyncQueue{
		NewMessage: make(chan *queue.ContentMessageInstance),
	}
}

func (instance *AppSyncQueue) ConnectToAMQP(consumerAmqpConn queue.IRabbitConnection) error {
	config := queue.MakeSyncRabbitMQCfg()
	consumer := queue.MakeNewConsumerFactory(consumerAmqpConn, config)
	err := consumer.CreateConsumer(config.ConsumerTag, false)
	if err != nil {
		return err
	}

	go consumer.Subscribe()

	go func() {
		for message := range consumer.NewMessage {
			instance.NewMessage <- message
			message = nil
		}
	}()

	return nil
}
