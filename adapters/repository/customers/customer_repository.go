package customers

import (
	"context"
	"fmt"
	"github.com/kamva/mgm/v3"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"payment_orders/adapters/db"
	"payment_orders/application/customers"
)

type RepositoryCustomers struct{
	database db.IMongoDBInstance
	collectionName string
}

func (instance *RepositoryCustomers) OpenConnection() error {
	return instance.database.Open()
}

func (instance *RepositoryCustomers) CloseConnection() error {
	return instance.database.Close()
}

func NewRepositoryCustomers(database db.IMongoDBInstance) *RepositoryCustomers {
	return &RepositoryCustomers{
		database: database,
		collectionName: "Customers",
	}
}

func (instance *RepositoryCustomers) GetById(ctx context.Context, id string) (*customers.MCustomers, error) {
	result := &customers.MCustomers{}

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	_ = collection.FindByIDWithCtx(ctx, id, result)

	collection = nil
	if result.ID.IsZero() {
		return nil, errors.New(fmt.Sprintf("Nenhum registro encontrado para o id: %v", id))
	}

	return result, nil
}

func (instance *RepositoryCustomers) GetByOwnId(ctx context.Context, id string) (*customers.MCustomers, error) {
	result := &customers.MCustomers{}

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	cur := collection.FindOne(ctx, bson.M{ "ownId": id})

	err := cur.Decode(&result)

	collection = nil

	if err != nil {
		return nil, err
	}

	if result.ID.IsZero() {
		return nil, errors.New(fmt.Sprintf("Nenhum registro encontrado para o id: %v", id))
	}

	return result, nil
}

func (instance *RepositoryCustomers) GetAll(ctx context.Context, start, limit int) ([]*customers.MCustomers, error) {
	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

	optSkip := int64(start)
	optLimit := int64(limit)

	opt := options.FindOptions{
		Skip: &optSkip,
		Limit: &optLimit,
	}

	var results []*customers.MCustomers

	cur, err := collection.Find(ctx, bson.D{{}}, &opt)

	if err != nil {
		return nil, err
	}

	for cur.Next(ctx) {
		var elem customers.MCustomers
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		results = append(results, &elem)
	}

	err = cur.Close(ctx)

	collection = nil
	if err != nil {
		return results, err
	}

	return results, nil
}

func (instance *RepositoryCustomers) Save(ctx context.Context, customer customers.ICustomers) (*customers.MCustomers, error) {
	if instance.database.GetDbType() == db.MongoDB {
		collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

		newModel := customer.GetModel()

		var err error
		if newModel.ID.IsZero() {
			err = collection.CreateWithCtx(ctx, newModel)
		} else {
			err = collection.UpdateWithCtx(ctx, newModel)
		}
		collection = nil

		if err != nil {
			return nil, err
		}

		return newModel.GetModel(), nil
	}

	return nil, nil
}

func (instance *RepositoryCustomers) Remove(ctx context.Context, customer customers.ICustomers) (*customers.MCustomers, error) {
	if instance.database.GetDbType() == db.MongoDB {
		collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

		newModel := customer.GetModel()

		err := collection.DeleteWithCtx(ctx, newModel)

		collection = nil
		if err != nil {
			return nil, err
		}

		return newModel.GetModel(), nil
	}
	return nil, nil
}

func (instance *RepositoryCustomers) RemoveCardFromCustomer(ctx context.Context, id string, idPaymentToken string) (*customers.MCustomers, error) {
	if instance.database.GetDbType() == db.MongoDB {
		customerDb, err := instance.GetById(ctx, id)
		if err != nil {
			return nil, err
		}
		collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

		idKey, _ := primitive.ObjectIDFromHex(id)
		filter := bson.D{{"_id", idKey}}
		update := bson.D{{"$pull", bson.D{{"creditCard", bson.D{{"idPaymentToken", idPaymentToken}}}}}}

		_, err = collection.UpdateOne(ctx, filter, update)

		collection = nil
		if err != nil {
			return nil, err
		}

		customerDb, err = instance.GetById(ctx, id)

		if err != nil {
			return nil, err
		}
		return customerDb.GetModel(), nil
	}


	return nil, nil
}
