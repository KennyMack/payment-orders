package cancellations

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"payment_orders/adapters/db"
)

type DTOOrdersCancellation struct {
	db.DefaultModel `valid:"-"bson:",inline"`
	UniqueIdentifier string `json:"uniqueIdentifier" bson:"uniqueIdentifier" valid:"required~UniqueIdentifier obrigatório"`
	Processed bool `json:"processed" bson:"processed" valid:"optional"`
	CurrentStatus string `json:"currentStatus" bson:"currentStatus" valid:"-"`
	FinalStatus string `json:"finalStatus" bson:"finalStatus" valid:"-"`
	CancellationId primitive.ObjectID `json:"cancellationId" bson:"cancellationId,omitempty" valid:"-" bson:",omitempty"`
	Refunded bool `json:"refunded" bson:"refunded" valid:"-"`
	Canceled bool `json:"canceled" bson:"canceled" valid:"-"`
	Error string `json:"error" bson:"error" valid:"-"`
}

func NewDTOOrdersCancellation() *DTOOrdersCancellation {
	orderCancellation := DTOOrdersCancellation{
		UniqueIdentifier: "",
		Processed: false,
		Refunded: false,
		Canceled: false,
		CurrentStatus: "",
		FinalStatus: "",
		CancellationId: primitive.NilObjectID,
		Error: "",
	}
	return &orderCancellation
}
