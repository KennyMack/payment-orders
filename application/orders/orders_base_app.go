package orders

import (
	dtoOrders "payment_orders/adapters/dto/orders"
	"payment_orders/adapters/queue"
	"payment_orders/application/transactions"
)

type MOrdersApp struct {
	Transaction  string
	Operation    string
	Body         dtoOrders.DTOOrders
}

type AppOrderBoleto struct {
	consumerAmqpConn queue.IRabbitConnection
	publishAmqpConn queue.IRabbitConnection
	serviceOrders IOrdersService
	serviceTransaction transactions.ITransactionsService
}

type AppOrderCard struct {
	consumerAmqpConn queue.IRabbitConnection
	publishAmqpConn queue.IRabbitConnection
	serviceOrders IOrdersService
	serviceTransaction transactions.ITransactionsService
}

type AppOrderPix struct {
	consumerAmqpConn queue.IRabbitConnection
	publishAmqpConn queue.IRabbitConnection
	serviceOrders IOrdersService
	serviceTransaction transactions.ITransactionsService
}

type AppOrders struct {
	consumerAmqpConn queue.IRabbitConnection
	publishAmqpConn queue.IRabbitConnection
}
