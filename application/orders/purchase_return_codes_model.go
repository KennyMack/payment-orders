package orders

import (
	"context"
	"payment_orders/adapters/db"
	baseRepository "payment_orders/application/baseinterfaces"
)

type MReturnCode struct {
	db.DefaultModel `valid:"-"bson:",inline"`
	LR string `json:"lr" bson:"lr" valid:"optional"`
	Description string `json:"description" bson:"description" valid:"optional"`
	Action string `json:"action" bson:"action" valid:"optional"`
	Irreversible string `json:"irreversible" bson:"irreversible" valid:"optional"`
}

type IReturnCode interface {
	GetModel() *MReturnCode
}

type IReturnCodeService interface {
	baseRepository.IBaseService
	GetByLR(ctx context.Context, lr string) (*MReturnCode, error)
}

type IReturnCodeReader interface{
	GetByLR(ctx context.Context, lr string) (*MReturnCode, error)
}

type IReturnCodeRepository interface {
	baseRepository.IBaseRepository
	IReturnCodeReader
}

func NewReturnCodeModel() *MReturnCode {
	return &MReturnCode{}
}

func (returnCode *MReturnCode) GetModel() *MReturnCode {
	return returnCode
}
