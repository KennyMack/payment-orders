package triggers

import (
	"context"
	"fmt"
	"log"
	dtoOrders "payment_orders/adapters/dto/orders"
	"payment_orders/adapters/queue"
	"payment_orders/application/baseinterfaces"
	"payment_orders/application/customers"
	"payment_orders/application/orders"
	"strconv"
	"strings"
)

type AppTriggers struct {
	publishAmqpConn queue.IRabbitConnection
	serviceTriggers ITriggerService
	serviceOrders orders.IOrdersService
	serviceCustomers customers.ICustomersService
}

func NewAppTriggers(
	publishAmqpConn queue.IRabbitConnection,
	serviceTriggers ITriggerService,
	serviceOrders orders.IOrdersService,
	serviceCustomers customers.ICustomersService) *AppTriggers {
	return &AppTriggers{
		publishAmqpConn: publishAmqpConn,
		serviceTriggers: serviceTriggers,
		serviceOrders: serviceOrders,
		serviceCustomers: serviceCustomers,
	}
}

func (instance *AppTriggers) OpenConnection(service baseinterfaces.IBaseService, message *MTriggerData) bool {
	errConn := service.OpenConnection()
	if errConn != nil {
		msgErr := fmt.Sprintf("Não foi possível processar o gatilho %s. err: %v", message.GetModel().Event, errConn)
		log.Printf(msgErr)
		return false
	}

	return true
}

func (instance *AppTriggers) CloseConnection(service baseinterfaces.IBaseService) {
	_ = service.CloseConnection()
}

func (instance *AppTriggers) ApplyTriggerData(ctx context.Context, message *MTriggerData) {
	messageToPublish := ""
	event := message.Event
	if strings.ToLower(message.Event) == INVOICE_STATUS_CHANGED {
		if !instance.OpenConnection(instance.serviceOrders, message) {
			return
		}
		paidCents := 0
		intVar, err := strconv.Atoi(message.PaidCents)
		if err != nil {
			paidCents = intVar
		}

		info := &orders.MOrderTriggerInfo{
			Status: message.Status,
			PaymentMethod: message.PaymentMethod,
			PaidAt: message.PaidAt,
			PaidCents: paidCents,
		}

		_, _ = instance.serviceOrders.UpdateTriggerInfo(ctx, message.Id, info)
		if strings.ToUpper(message.Status) == dtoOrders.PAID {
			event = "PAYMENT.AUTHORIZED"
		}

		if strings.ToUpper(message.Status) == dtoOrders.REFUNDED {
			event = "ORDER.REFUNDED"
		}

		if strings.ToUpper(message.Status) == dtoOrders.CANCELED {
			event = "ORDER.CANCELED"
		}

		messageToPublish = fmt.Sprintf("{ \"id\": \"%v\", \"status\": \"%v\", \"payment\": { \"id\": \"%v\" } }", message.Id, message.Status, message.Id)
	}

	if len(messageToPublish) > 0 {
		instance.sendToWebHook(ctx, strings.ToUpper(event), messageToPublish)
	}
}

func (instance *AppTriggers) sendToWebHook(ctx context.Context, event, message string) {
	s := fmt.Sprintf(`{"event": "%v", "resource": %v, "error": "" }`, event, message)

	_ =  instance.publishMessage(ctx, s)
}

func (instance *AppTriggers) publishMessage(ctx context.Context, message string) error {
	configProducer := queue.MakeWebHookRabbitMQCfg()
	producer := queue.MakeNewProducerFactory(instance.publishAmqpConn, configProducer)
	err := producer.CreateProducer(false)

	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}
	producer.PublishStringMessage(message)
	return nil
}

func (instance *AppTriggers) ProcessTrigger(ctx context.Context, message *MTriggerData) {
	if !instance.OpenConnection(instance.serviceTriggers, message) {
		return
	}
	message.Processed = true
	message.Event = strings.ToLower(message.Event)
	_, err := instance.serviceTriggers.Create(ctx, message)
	if err != nil {
		log.Print(err)
		return
	}

	instance.ApplyTriggerData(ctx, message)
}
