package payment

import (
	"context"
	"encoding/json"
	"fmt"
	"payment_orders/utils/environment"
	"payment_orders/utils/request"
	"time"
)

const (
	createChargeUrl          = "%s/charge?"
	createInvoiceUrl          = "%s/invoices?"
	captureInvoiceUrl         = "%s/invoices/%s/capture?"
	refundInvoiceUrl          = "%s/invoices/%s/refund?"
	cancelInvoiceUrl          = "%s/invoices/%s/cancel?"
	generateSecondInvoiceUrl  = "%s/invoices/%s/duplicate?"
	getInvoiceByIdUrl         = "%s/invoices/%s?"
	getInvoiceAllUrl          = "%s/invoices?%s&%s"
	sendInvoiceToEmailUrl     = "%s/invoices/%s/send_email?"
)

type IuguInvoicesService struct{
	baseIUGUURL string
}

func NewIUGUInvoices() *IuguInvoicesService {
	return &IuguInvoicesService{
		baseIUGUURL: environment.GetIUGU_BASE_URL(),
	}
}

func (instance *IuguInvoicesService) CreateInvoice(context context.Context, invoice IuguInvoiceBody) (interface{}, error) {
	url := fmt.Sprintf(createInvoiceUrl, instance.baseIUGUURL)

	objJson, err := json.Marshal(invoice)

	if err != nil {
		return nil, err
	}
	header := getIUGUAuthHeader()

	result, err := request.Post(context, url, objJson, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IuguInvoicesService) DirectCharge(context context.Context, invoice IuguChargeBody) (interface{}, error) {
	url := fmt.Sprintf(createChargeUrl, instance.baseIUGUURL)

	objJson, err := json.Marshal(invoice)

	if err != nil {
		return nil, err
	}
	header := getIUGUAuthHeader()

	result, err := request.Post(context, url, objJson, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IuguInvoicesService) CaptureInvoice(context context.Context, invoiceId string) (interface{}, error) {
	url := fmt.Sprintf(captureInvoiceUrl, instance.baseIUGUURL, invoiceId)

	header := getIUGUAuthHeader()

	result, err := request.Post(context, url, nil, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IuguInvoicesService) RefundInvoice(context context.Context, invoiceId string, partialValue float64) (interface{}, error) {
	url := fmt.Sprintf(refundInvoiceUrl, instance.baseIUGUURL, invoiceId)

	header := getIUGUAuthHeader()

	partialValueRefundCents := partialValue * 100

	refundObj := make(map[string]float64)

	if partialValueRefundCents > 0 {
		refundObj["partial_value_refund_cents"] = partialValueRefundCents
	}

	objJson, err := json.Marshal(refundObj)

	if err != nil {
		return nil, err
	}

	result, err := request.Post(context, url, objJson, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IuguInvoicesService) CancelInvoice(context context.Context, invoiceId string) (interface{}, error) {
	url := fmt.Sprintf(cancelInvoiceUrl, instance.baseIUGUURL, invoiceId)

	header := getIUGUAuthHeader()

	result, err := request.Put(context, url, nil, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IuguInvoicesService) GenerateSecondInvoice(context context.Context, invoiceId string, newDueDate time.Time) (interface{}, error) {
	url := fmt.Sprintf(generateSecondInvoiceUrl, instance.baseIUGUURL, invoiceId)

	header := getIUGUAuthHeader()

	dueDate := newDueDate.Format("2006-01-02")


	secondInvoice := make(map[string]string)

	secondInvoice["due_date"] = dueDate

	objJson, err := json.Marshal(secondInvoice)

	if err != nil {
		return nil, err
	}

	result, err := request.Post(context, url, objJson, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IuguInvoicesService) GetInvoiceById(context context.Context, clientId string) (interface{}, error) {
	url := fmt.Sprintf(getInvoiceByIdUrl, instance.baseIUGUURL, clientId)

	header := getIUGUAuthHeader()

	result, err := request.Get(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IuguInvoicesService) GetInvoiceAll(context context.Context, start, limit int) (interface{}, error) {
	urlLimit := ""
	urlStart := fmt.Sprintf("start=%v", start)

	if limit > 0 {
		urlLimit = fmt.Sprintf("limit=%v", limit)
	}

	url := fmt.Sprintf(getInvoiceAllUrl, instance.baseIUGUURL, urlLimit, urlStart)

	header := getIUGUAuthHeader()

	result, err := request.Get(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IuguInvoicesService) SendInvoiceToEmail(context context.Context, invoiceId string) (interface{}, error) {
	url := fmt.Sprintf(sendInvoiceToEmailUrl, instance.baseIUGUURL, invoiceId)

	header := getIUGUAuthHeader()

	result, err := request.Put(context, url, nil, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}
