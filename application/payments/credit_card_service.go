package payments

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"payment_orders/adapters/dto/payments"
)

type ServiceCardClientToken struct{
	repository ICardClientTokenRepository
}

func NewServiceCardClientToken(repository ICardClientTokenRepository) *ServiceCardClientToken {
	return &ServiceCardClientToken{
		repository: repository,
	}
}

func (instance *ServiceCardClientToken) OpenConnection() error {
	return instance.repository.OpenConnection()
}

func (instance *ServiceCardClientToken) CloseConnection() error {
	return instance.repository.CloseConnection()
}

func (instance *ServiceCardClientToken) Create(ctx context.Context, model *payments.DTOCardClientToken) (*MCardClientToken, error) {
	dbModel := NewCardClientTokenModel(model)

	_, err := dbModel.IsValid()

	if err != nil {
		return nil, err
	}

	if !dbModel.ID.IsZero() {
		return nil, errors.New("Id deve ser nulo para novos cartões")
	}

	newModel, err := instance.repository.Save(ctx, dbModel)

	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceCardClientToken) Change(ctx context.Context, id string, model *payments.DTOCardClientToken) (*MCardClientToken, error) {
	dbModel := NewCardClientTokenModel(model)

	_, err := dbModel.IsValid()

	if err != nil {
		return nil, err
	}

	if dbModel.ID.IsZero() {
		return nil, errors.New("Id deve ser informado")
	}

	oldModel, err := instance.repository.GetById(ctx, dbModel.ID.Hex())

	if oldModel == nil {
		return nil, errors.New( fmt.Sprintf("Nenhum registro encontrado para o id informado: Id: %v", dbModel.ID))
	}
	dbModel.CreatedAt = oldModel.CreatedAt

	newModel, err := instance.repository.Save(ctx, dbModel)

	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceCardClientToken) Remove(ctx context.Context, id string) (*MCardClientToken, error) {
	oldModel, err := instance.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	if oldModel == nil {
		return nil, errors.New( fmt.Sprintf("Nenhum registro encontrado para o id informado: Id: %v", id))
	}

	return instance.repository.Remove(ctx, oldModel)
}

func (instance *ServiceCardClientToken) GetById(ctx context.Context, id string) (*MCardClientToken, error) {
	res, err := instance.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	return res, nil
}


func (instance *ServiceCardClientToken) GetByCustomerId(ctx context.Context, customerId primitive.ObjectID) ([]*MCardClientToken, error) {
	return instance.repository.GetByCustomerId(ctx, customerId)
}

func (instance *ServiceCardClientToken) GetByCardToken(ctx context.Context, CardToken string, processed bool) (*MCardClientToken, error) {
	return instance.repository.GetByCardToken(ctx, CardToken, processed)
}

func (instance *ServiceCardClientToken) UpdateToProcessed(ctx context.Context, id string) (*MCardClientToken, error){
	card, err := instance.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	card.Processed = true

	card, err = instance.repository.Save(ctx, card)

	if err != nil {
		return nil, err
	}

	return card, nil
}
