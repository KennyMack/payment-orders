package customers

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	dtoCustomers "payment_orders/adapters/dto/customers"
	"payment_orders/adapters/dto/orders"
	dtoCardClient "payment_orders/adapters/dto/payments"
	"payment_orders/adapters/queue"
	cardToken "payment_orders/application/payments"
	"payment_orders/application/transactions"
	"payment_orders/utils"
	"strings"
)

type AppCustomers struct {
	publishAmqpConn queue.IRabbitConnection
	serviceCustomer ICustomersService
	serviceCardClientToken cardToken.ICardClientTokenService
	serviceTransaction transactions.ITransactionsService
}

func NewAppCustomers(
	publishAmqpConn queue.IRabbitConnection,
	serviceCustomer ICustomersService,
	serviceCardClientToken cardToken.ICardClientTokenService,
	serviceTransaction transactions.ITransactionsService,) *AppCustomers {
	return &AppCustomers{
		publishAmqpConn: publishAmqpConn,
		serviceCustomer: serviceCustomer,
		serviceTransaction: serviceTransaction,
		serviceCardClientToken: serviceCardClientToken,
	}
}

func (instance *AppCustomers) publishMessage(ctx context.Context, message string) error {
	configProducer := queue.MakeSyncRabbitMQCfg()
	producer := queue.MakeNewProducerFactory(instance.publishAmqpConn, configProducer)
	err := producer.CreateProducer(false)

	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}
	producer.PublishStringMessage(message)
	_ = instance.publishAmqpConn.CloseConnection()
	return nil
}

func (instance *AppCustomers) publishMessageToWebHook(ctx context.Context, message string) error {
	configProducer := queue.MakeWebHookRabbitMQCfg()
	producer := queue.MakeNewProducerFactory(instance.publishAmqpConn, configProducer)
	err := producer.CreateProducer(false)

	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}
	producer.PublishStringMessage(message)
	_ = instance.publishAmqpConn.CloseConnection()

	return nil
}

func (instance *AppCustomers) sendToWebHook(ctx context.Context, event, message string, err error) {
	errText := ""
	if err != nil {
		errText = utils.ScapeChars(err.Error())
	}
	s := fmt.Sprintf(`{"event": "%v", "resource": %v, "error": "%v" }`, event, message, errText)

	_ =  instance.publishMessageToWebHook(ctx, s)
}

func (instance *AppCustomers) mustSendToWebHook(ctx context.Context,
	message *queue.ContentMessageInstance,
	dtoCustomer *dtoCustomers.DTOCustomers,
	err error) {
	if message.Message.DeliveryTag >= message.DeliveryLimit {
		log.Printf("Webhook - Customer %s - UniqueIdentifier %s", dtoCustomer.ID.Hex(), dtoCustomer.UniqueIdentifier)
		jsonHook, errFind := json.Marshal(dtoCustomer)
		if errFind == nil {
			instance.sendToWebHook(ctx, "CUSTOMER.REGISTER", string(jsonHook), err)
		}
	}
}

func (instance *AppCustomers) NewMessage(message *queue.ContentMessageInstance) {
	customerApp := orders.DTOOrderCustomerCancellation{}

	errParse := json.Unmarshal(message.Body, &customerApp)
	if errParse != nil {
		log.Printf("Falha ao converter o JSON. err: %v", errParse)
		log.Printf("Body: %v ", string(message.Body))
		_ = message.Message.Nack(false, false)
		return
	}

	operation := customerApp.Operation
	transactionId := customerApp.Transaction
	log.Printf("Processando transação: %v", transactionId)

	ctx := context.Background()
	_, _ = instance.serviceTransaction.StartTransaction(ctx, transactionId, operation)

	dtoCustomer := &customerApp.Body.Customer

	dbCustomer, err := instance.processCustomer(ctx, customerApp.Operation, dtoCustomer)

	if err != nil {
		msgErr := fmt.Sprintf("Não foi possível processar a transação %s. err: %v", customerApp.Transaction, err)

		log.Printf(msgErr)
		_, _ = instance.serviceTransaction.AddMessageError(ctx, transactionId, operation, msgErr)

		instance.mustSendToWebHook(ctx, message, dtoCustomer, errors.New(msgErr))
		_ = message.Message.Nack(false, !(message.Message.DeliveryTag >= message.DeliveryLimit))
		ctx.Done()
		return
	}
	_, _ = instance.serviceTransaction.UpdateCustomerId(ctx, transactionId, operation, dbCustomer.GetModel().ID.Hex())

	if len(dtoCustomer.CardToken) > 0 {
		_ = instance.processCardCustomerToken(ctx, operation, dbCustomer.GetModel().ID, dtoCustomer.CardToken, false)
	}

	if len(dtoCustomer.RemoveCardToken) > 0 {
		_ = instance.processCardCustomerToken(ctx, operation, dbCustomer.GetModel().ID, dtoCustomer.RemoveCardToken, true)
	}

	_, _ = instance.serviceTransaction.UpdateToCreated(ctx, transactionId, operation)

	_ = message.Message.Ack(false)

	s, _ := json.Marshal(customerApp)
	_ = instance.publishMessage(ctx, string(s))
	ctx.Done()
}

func (instance *AppCustomers) processCustomer(ctx context.Context, operation string, dtoCustomer *dtoCustomers.DTOCustomers) (ICustomers, error) {
	var dbCustomer ICustomers = nil
	var err error

	if operation == queue.CREATE ||
		operation == queue.CHANGE {
		oldCustomer, _ := instance.serviceCustomer.GetByOwnId(ctx, dtoCustomer.OwnId)

		if oldCustomer == nil {
			dbCustomer, err = instance.serviceCustomer.Create(ctx, dtoCustomer)
		} else {
			dtoCustomer.ID = oldCustomer.ID
			dbCustomer, err = instance.serviceCustomer.Change(ctx, oldCustomer.ID.Hex(), dtoCustomer)
		}
	} else if operation == queue.REMOVE {
		oldCustomer, err := instance.serviceCustomer.GetByOwnId(ctx, dtoCustomer.OwnId)
		if err != nil {
			return nil, err
		}
		dbCustomer, err = instance.serviceCustomer.Remove(ctx, oldCustomer.ID.Hex())
	}

	return dbCustomer, err
}

func (instance *AppCustomers) processCardCustomerToken(ctx context.Context, operation string, customerId primitive.ObjectID, cardToken string, remove bool ) error {
	if operation == queue.CREATE ||
		operation == queue.CHANGE {
		tokens := strings.Split(cardToken, ";")
		for i := 0; i < len(tokens); i++ {
			tokenCard := tokens[i]
			oldCardToken, err := instance.serviceCardClientToken.GetByCardToken(ctx, tokenCard, false)

			if err != nil {
				return err
			}

			if oldCardToken == nil || remove {
				dtoCreateCard := dtoCardClient.NewDTOCardClientToken()
				dtoCreateCard.CustomerId = customerId
				dtoCreateCard.CardToken = tokenCard
				dtoCreateCard.SetAsDefault = false
				dtoCreateCard.Remove = remove
				dtoCreateCard.Processed = false

				_, err = instance.serviceCardClientToken.Create(ctx, dtoCreateCard)
			}
		}
	}

	return nil
}

