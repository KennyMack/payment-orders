package transactions

import (
	"context"
)

type AppTransaction struct {
	// serviceCustomer customers.ICustomersService
	serviceTransaction ITransactionsService
}

func NewAppTransaction(// serviceCustomer customers.ICustomersService,
	serviceTransaction ITransactionsService) *AppTransaction {
	return &AppTransaction{
		// serviceCustomer: serviceCustomer,
		serviceTransaction: serviceTransaction,
	}
}

func (instance *AppTransaction) SendCustomersToIugu(ctx context.Context) {
	// criar na transação uma checagem se ela já foi enviada para o iugu
	// buscar todos os clientes vinculadas Às transaçoes que ainda estão com o uniqueIdentifier = "AGUARDANDO"
}
