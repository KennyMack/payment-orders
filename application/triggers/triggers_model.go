package triggers

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"payment_orders/adapters/db"
	baseRepository "payment_orders/application/baseinterfaces"
	"payment_orders/utils"
	"strings"
)

type MTriggerData struct {
	db.DefaultModel `valid:"-"bson:",inline"`
	ControlId              string `bson:"controlId" json:"control_id"`
	Processed              bool `bson:"processed" json:"processed"`
	Event                  string `bson:"Event" json:"event"`
	Id                     string `bson:"Id" json:"id"`
	AccountId              string `bson:"AccountId" json:"account_id"`
	Status                 string `bson:"Status" json:"status"`
	SubscriptionId         string `bson:"SubscriptionId" json:"subscription_id"`
	Source                 string `bson:"Source" json:"source"`
	OrderId                string `bson:"OrderId" json:"order_id"`
	ExternalReference      string `bson:"ExternalReference" json:"external_reference"`
	PaymentMethod          string `bson:"PaymentMethod" json:"payment_method"`
	PaidAt                 string `bson:"PaidAt" json:"paid_at"`
	PayerCpfCnpj           string `bson:"PayerCpfCnpj" json:"payer_cpf_cnpj"`
	PixEndToEndId          string `bson:"PixEndToEndId" json:"pix_end_to_end_id"`
	PaidCents              string `bson:"PaidCents" json:"paid_cents"`
	Lr                     string `bson:"Lr" json:"lr"`
	Action                 string `bson:"Action" json:"action"`
	NumberOfInstallments   string `bson:"NumberOfInstallments" json:"number_of_installments"`
	Amount                 string `bson:"Amount" json:"amount"`
	AmountCents            string `bson:"AmountCents" json:"amount_cents"`
	Total                  string `bson:"Total" json:"total"`
	Taxes                  string `bson:"Taxes" json:"taxes"`
	Fines                  string `bson:"Fines" json:"fines"`
	Discount               string `bson:"Discount" json:"discount"`
	CustomerName           string `bson:"CustomerName" json:"customer_name"`
	CustomerEmail          string `bson:"CustomerEmail" json:"customer_email"`
	ExpiresAt              string `bson:"ExpiresAt" json:"expires_at"`
	PlanIdentifier         string `bson:"PlanIdentifier" json:"plan_identifier"`
	Feedback               string `bson:"Feedback" json:"feedback"`
	ChargeLimitCents       string `bson:"ChargeLimitCents" json:"charge_limit_cents"`
	AgreementEffect        string `bson:"AgreementEffect" json:"agreement_effect"`
	WithdrawRequestId      string `bson:"WithdrawRequestId" json:"withdraw_request_id"`
	DefaultPaymentMethodId string `bson:"DefaultPaymentMethodId" json:"default_payment_method_id"`
	CreditCardToken        string `bson:"CreditCardToken" json:"credit_card_token"`
	CreditCardYear         string `bson:"CreditCardYear" json:"credit_card_year"`
	CreditCardMonth        string `bson:"CreditCardMonth" json:"credit_card_month"`
	CreditCardBrand        string `bson:"CreditCardBrand" json:"credit_card_brand"`
	CreditCardHolder       string `bson:"CreditCardHolder" json:"credit_card_holder"`
	CreditCardMaskedNumber string `bson:"CreditCardMaskedNumber" json:"credit_card_masked_number"`
}

type ITriggerData interface {
	IsValid() (bool, error)
	GetModel() *MTriggerData
}

type ITriggerService interface {
	baseRepository.IBaseService
	Create(ctx context.Context, model *MTriggerData) (*MTriggerData, error)
	UpdateToProcessed(ctx context.Context, model *MTriggerData) (*MTriggerData, error)
	GetByControlId(ctx context.Context, id string) (*MTriggerData, error)
}

type ITriggerReader interface{
	GetByControlId(ctx context.Context, id string) (*MTriggerData, error)
}

type ITriggerWriter interface {
	Save(ctx context.Context, order ITriggerData) (*MTriggerData, error)
}

type ITriggerRepository interface {
	baseRepository.IBaseRepository
	ITriggerReader
	ITriggerWriter
}

func (model *MTriggerData) GetModel() *MTriggerData {
	return model
}

func IsValidEvent(event string) bool {
	options := []string{
		ALL,
		CUSTOMER_PAYMENT_METHOD_NEW,
		INVOICE_CREATED,
		INVOICE_STATUS_CHANGED,
		INVOICE_REFUND,
		INVOICE_PAYMENT_FAILED,
		INVOICE_DUNNING_ACTION,
		INVOICE_DUE,
		INVOICE_INSTALLMENT_RELEASED,
		INVOICE_RELEASED,
		INVOICE_BANK_SLIP_STATUS,
		INVOICE_PARTIALLY_REFUNDED,
		SUBSCRIPTION_SUSPENDED,
		SUBSCRIPTION_ACTIVATED,
		SUBSCRIPTION_CREATED,
		SUBSCRIPTION_RENEWED,
		SUBSCRIPTION_EXPIRED,
		SUBSCRIPTION_CHANGED,
		REFERRALS_VERIFICATION,
		REFERRALS_BANK_VERIFICATION,
		TRANSFER_REQUEST_STATUS_CHANGED,
		WITHDRAW_REQUEST_CREATED,
		WITHDRAW_REQUEST_STATUS_CHANGED,
		DEPOSIT_ACCEPTED,
		DEPOSIT_REJECTED,
	}

	return utils.StringInSlice(strings.ToLower(event), options)
}

func (model *MTriggerData) IsValid() (bool, error) {
	if !IsValidEvent(model.Event) {
		return false, errors.New(fmt.Sprintf("Evento \"%s\" não é válido", model.Event ))
	}
	return true, nil
}

func NewTriggerDataModel(dtoModel *MTriggerData) *MTriggerData {
	value := uuid.New()

	data := MTriggerData{
		Event: "NO.DATA",
		ControlId: value.String(),
		Processed: false,
	}

	if dtoModel != nil {
		dto := dtoModel.GetModel()
		data.Event = dto.Event
		data.Processed = dto.Processed
		data.Id = dto.Id
		data.AccountId = dto.AccountId
		data.Status = dto.Status
		data.SubscriptionId = dto.SubscriptionId
		data.Source = dto.Source
		data.OrderId = dto.OrderId
		data.ExternalReference = dto.ExternalReference
		data.PaymentMethod = dto.PaymentMethod
		data.PaidAt = dto.PaidAt
		data.PayerCpfCnpj = dto.PayerCpfCnpj
		data.PixEndToEndId = dto.PixEndToEndId
		data.PaidCents = dto.PaidCents
		data.Lr = dto.Lr
		data.Action = dto.Action
		data.NumberOfInstallments = dto.NumberOfInstallments
		data.Amount = dto.Amount
		data.AmountCents = dto.AmountCents
		data.Total = dto.Total
		data.Taxes = dto.Taxes
		data.Fines = dto.Fines
		data.Discount = dto.Discount
		data.CustomerName = dto.CustomerName
		data.CustomerEmail = dto.CustomerEmail
		data.ExpiresAt = dto.ExpiresAt
		data.PlanIdentifier = dto.PlanIdentifier
		data.Feedback = dto.Feedback
		data.ChargeLimitCents = dto.ChargeLimitCents
		data.AgreementEffect = dto.AgreementEffect
		data.WithdrawRequestId = dto.WithdrawRequestId
		data.DefaultPaymentMethodId = dto.DefaultPaymentMethodId
		data.CreditCardToken = dto.CreditCardToken
		data.CreditCardYear = dto.CreditCardYear
		data.CreditCardMonth = dto.CreditCardMonth
		data.CreditCardBrand = dto.CreditCardBrand
		data.CreditCardHolder = dto.CreditCardHolder
		data.CreditCardMaskedNumber = dto.CreditCardMaskedNumber
	}

	return &data
}

const (
	ALL  = "all"
	CUSTOMER_PAYMENT_METHOD_NEW  = "customer_payment_method.new"
	INVOICE_CREATED  = "invoice.created"
	INVOICE_STATUS_CHANGED  = "invoice.status_changed"
	INVOICE_REFUND  = "invoice.refund"
	INVOICE_PAYMENT_FAILED  = "invoice.payment_failed"
	INVOICE_DUNNING_ACTION  = "invoice.dunning_action"
	INVOICE_DUE  = "invoice.due"
	INVOICE_INSTALLMENT_RELEASED  = "invoice.installment_released"
	INVOICE_RELEASED  = "invoice.released"
	INVOICE_BANK_SLIP_STATUS  = "invoice.bank_slip_status"
	INVOICE_PARTIALLY_REFUNDED  = "invoice.partially_refunded"
	SUBSCRIPTION_SUSPENDED  = "subscription.suspended"
	SUBSCRIPTION_ACTIVATED  = "subscription.activated"
	SUBSCRIPTION_CREATED  = "subscription.created"
	SUBSCRIPTION_RENEWED  = "subscription.renewed"
	SUBSCRIPTION_EXPIRED  = "subscription.expired"
	SUBSCRIPTION_CHANGED  = "subscription.changed"
	REFERRALS_VERIFICATION  = "referrals.verification"
	REFERRALS_BANK_VERIFICATION  = "referrals.bank_verification"
	TRANSFER_REQUEST_STATUS_CHANGED  = "transfer_request.status_changed"
	WITHDRAW_REQUEST_CREATED  = "withdraw_request.created"
	WITHDRAW_REQUEST_STATUS_CHANGED  = "withdraw_request.status_changed"
	DEPOSIT_ACCEPTED  = "deposit.accepted"
	DEPOSIT_REJECTED  = "deposit.rejected"
)
