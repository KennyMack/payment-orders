package db

// DefaultModel struct contains a model's default fields.
type DefaultModel struct {
	IDField    `bson:",inline"`
	DateFields `bson:",inline"`
}

// Creating function calls the inner fields' defined hooks
func (model *DefaultModel) Creating() error {
	return model.DateFields.Creating()
}

// Saving function calls the inner fields' defined hooks
func (model *DefaultModel) Saving() error {
	return model.DateFields.Saving()
}
