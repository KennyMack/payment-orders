FROM golang:1.19-buster as builder

WORKDIR /app
EXPOSE 80
EXPOSE 443
EXPOSE 8081

COPY go.* ./
RUN go mod download

COPY . ./

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -o payment-orders

FROM debian:buster-slim AS final
RUN set -x && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    ca-certificates && \
    rm -rf /var/lib/apt/lists/*

COPY --from=builder /app/payment-orders /app/payment-orders

#ENTRYPOINT ["/app/payment-orders", "/secrets"]
ENTRYPOINT ["/app/payment-orders", "DOCKER"]
#CMD ["tail", "-f", "/dev/null"]
