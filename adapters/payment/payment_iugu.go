package payment

import (
	"context"
	"encoding/json"
	"fmt"
	"payment_orders/utils/environment"
	"payment_orders/utils/request"
	"strconv"
)

const (
	generateTokenUrl        = "%s/payment_token"
	createPaymentMethodUrl  = "%s/customers/%s/payment_methods"
	changePaymentMethodUrl  = "%s/customers/%s/payment_methods/%s?"
	removePaymentMethodUrl  = "%s/customers/%s/payment_methods/%s?"
	getPaymentMethodByIdUrl = "%s/customers/%s/payment_methods/%s"
	getAllPaymentMethodUrl  = "%s/customers/%s/payment_methods?"
)

type IUGUPayment struct{
	baseIUGUURL string
}

func NewIUGUPayment() *IUGUPayment {
	return &IUGUPayment{
		baseIUGUURL: environment.GetIUGU_BASE_URL(),
	}
}

func (instance *IUGUPayment) GenerateToken(context context.Context, token IuguTokenBody) (interface{}, error) {
	url := fmt.Sprintf(generateTokenUrl, instance.baseIUGUURL)

	IuguProduction, err := strconv.ParseBool(environment.GetIUGU_PRODUCTION())

	if err != nil {
		return nil, err
	}

	token.Test = !IuguProduction

	objJson, err := json.Marshal(token)

	if err != nil {
		return nil, err
	}

	header := getIUGUAuthHeader()

	result, err := request.Post(context, url, objJson, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUPayment) CreatePaymentMethod(context context.Context, customerId string, paymentMethod IuguPaymentMethodBody) (interface{}, error) {
	url := fmt.Sprintf(createPaymentMethodUrl, instance.baseIUGUURL, customerId)

	objJson, err := json.Marshal(paymentMethod)

	if err != nil {
		return nil, err
	}
	header := getIUGUAuthHeader()

	result, err := request.Post(context, url, objJson, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUPayment) ChangePaymentMethod(context context.Context, customerId, paymentId, description string) (interface{}, error) {
	url := fmt.Sprintf(changePaymentMethodUrl, instance.baseIUGUURL, customerId, paymentId)

	paymentChangeObj := make(map[string]string)

	paymentChangeObj["description"] = description

	objJson, err := json.Marshal(paymentChangeObj)

	if err != nil {
		return nil, err
	}
	header := getIUGUAuthHeader()

	result, err := request.Put(context, url, objJson, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUPayment) RemovePaymentMethod(context context.Context, customerId, paymentId string) (interface{}, error) {
	url := fmt.Sprintf(removePaymentMethodUrl, instance.baseIUGUURL, customerId, paymentId)

	header := getIUGUAuthHeader()

	result, err := request.Delete(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUPayment) GetPaymentMethodById(context context.Context, customerId, paymentId string) (interface{}, error) {
	url := fmt.Sprintf(getPaymentMethodByIdUrl, instance.baseIUGUURL, customerId, paymentId)

	header := getIUGUAuthHeader()

	result, err := request.Get(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUPayment) GetAllPaymentMethod(context context.Context, customerId string) (interface{}, error) {
	url := fmt.Sprintf(getAllPaymentMethodUrl, instance.baseIUGUURL, customerId)

	header := getIUGUAuthHeader()

	result, err := request.Get(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}
