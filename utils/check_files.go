package utils

import (
	"errors"
	"os"
)

func FileExists (path string) (bool, error) {
	if _, err := os.Stat(path); errors.Is(err, os.ErrNotExist) {
		return false, err
	}

	return true, nil
}

