package orders

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	dtoOrders "payment_orders/adapters/dto/orders"
	"payment_orders/adapters/queue"
	"payment_orders/application/customers"
	"payment_orders/utils"
)

func NewAppOrders(
	consumerAmqpConn queue.IRabbitConnection,
	publishAmqpConn queue.IRabbitConnection) *AppOrders {
	return &AppOrders{
		consumerAmqpConn,
		publishAmqpConn,
	}
}

func (instance *AppOrders) ConnectToAMQP(ctx context.Context) error {
	config := queue.MakeOrdersCustomerRabbitMQCfg()
	consumer := queue.MakeNewConsumerFactory(instance.consumerAmqpConn, config)
	err := consumer.CreateConsumer(config.ConsumerTag, false)
	if err != nil {
		return err
	}

	go consumer.Subscribe()

	go func() {
		for message := range consumer.NewMessage {
			instance.newMessage(message)
			message = nil
		}
	}()

	return nil
}

func (instance *AppOrders) publishToOrderBoleto(ctx context.Context, order *dtoOrders.DTOOrderCustomerCancellation) error {
	cfg := queue.MakeOrdersBoletoRabbitMQCfg()

	byteJson, err := json.Marshal(order)
	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}

	err = instance.publishMessage(ctx, cfg, string(byteJson))
	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}

	return nil
}

func (instance *AppOrders) publishToOrderCard(ctx context.Context, order *dtoOrders.DTOOrderCustomerCancellation) error {
	cfg := queue.MakeOrdersCardRabbitMQCfg()

	byteJson, err := json.Marshal(order)
	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}

	err = instance.publishMessage(ctx, cfg, string(byteJson))
	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}

	return nil
}

func (instance *AppOrders) publishToOrderPix(ctx context.Context, order *dtoOrders.DTOOrderCustomerCancellation) error {
	cfg := queue.MakeOrdersPixRabbitMQCfg()

	byteJson, err := json.Marshal(order)
	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}

	err = instance.publishMessage(ctx, cfg, string(byteJson))
	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}

	return nil
}

func (instance *AppOrders) publishToCustomer(ctx context.Context, order *dtoOrders.DTOOrderCustomerCancellation) error {
	cfg := queue.MakeCustomersRabbitMQCfg()

	byteJson, err := json.Marshal(order)
	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}

	err = instance.publishMessage(ctx, cfg, string(byteJson))
	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}

	return nil
}

func (instance *AppOrders) publishMessage(ctx context.Context, cfg queue.RabbitMQConfig, message string) error {
	producer := queue.MakeNewProducerFactory(instance.publishAmqpConn, cfg)
	err := producer.CreateProducer(false)

	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}
	producer.PublishStringMessage(message)
	_ = instance.publishAmqpConn.CloseConnection()
	return nil
}

func (instance *AppOrders) mustSendToWebHook(ctx context.Context,
	message *queue.ContentMessageInstance,
	dtoOrder *dtoOrders.DTOOrderCustomerCancellation,
	err error,
	force bool) {
	if message.Message.DeliveryTag >= message.DeliveryLimit || force {
		log.Printf("Webhook - Order customer %s", dtoOrder.Transaction)
		jsonHook, errFind := json.Marshal(dtoOrder)
		if errFind == nil {
			instance.sendToWebHook(ctx, "ORDER_CUSTOMER.REGISTER", string(jsonHook), err)
		}
	}
}

func (instance *AppOrders) sendToWebHook(ctx context.Context, event, message string, err error) {
	errText := ""
	if err != nil {
		errText = utils.ScapeChars(err.Error())
	}
	s := fmt.Sprintf(`{"event": "%v", "resource": %v, "error": "%v" }`, event, message, errText)

	_ =  instance.publishMessageToWebHook(ctx, s)
}

func (instance *AppOrders) publishMessageToWebHook(ctx context.Context, message string) error {
	configProducer := queue.MakeWebHookRabbitMQCfg()
	producer := queue.MakeNewProducerFactory(instance.publishAmqpConn, configProducer)
	err := producer.CreateProducer(false)

	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}
	producer.PublishStringMessage(message)

	_ = instance.publishAmqpConn.CloseConnection()
	return nil
}

func (instance *AppOrders) newMessage(message *queue.ContentMessageInstance) {
	orderCustomerApp := dtoOrders.DTOOrderCustomerCancellation{}

	errParse := json.Unmarshal(message.Body, &orderCustomerApp)
	if errParse != nil {
		log.Printf("Falha ao converter o JSON. err: %v", errParse)
		log.Printf("Body: %v ", string(message.Body))
		_ = message.Message.Nack(false, false)
		return
	}

	transactionId := orderCustomerApp.Transaction
	customerProcessed := orderCustomerApp.CustomerProcessed
	orderProcessed := orderCustomerApp.OrderProcessed
	log.Printf("Order customer transação: %v", transactionId)

	ctx := context.Background()
	var err error

	model := customers.NewCustomerModel(&orderCustomerApp.Body.Customer)
	if len(model.OwnId) > 0 {
		_, err = model.IsValid()
		if err != nil {
			orderCustomerApp.CustomerError = utils.ScapeChars(err.Error())
			instance.mustSendToWebHook(ctx, message, &orderCustomerApp, err, true)
			_ = message.Message.Ack(false)
			ctx.Done()
			return
		}
	} else {
		model = nil
	}

	modelOrder := NewOrderModel(&orderCustomerApp.Body.Order)
	if len(modelOrder.OwnId) > 0 {
		_, err = modelOrder.IsValid()
		if err != nil {
			orderCustomerApp.OrderError = utils.ScapeChars(err.Error())
			instance.mustSendToWebHook(ctx, message, &orderCustomerApp, err, true)
			_ = message.Message.Ack(false)
			ctx.Done()
			return
		}
	} else {
		modelOrder = nil
	}

	if model != nil && !customerProcessed {
		err = instance.publishToCustomer(ctx, &orderCustomerApp)
	} else if modelOrder != nil && !orderProcessed && utils.StringInSlice("bank_slip", orderCustomerApp.Body.Order.PayableWith) {
		err = instance.publishToOrderBoleto(ctx, &orderCustomerApp)
	} else if modelOrder != nil &&  !orderProcessed && utils.StringInSlice("credit_card", orderCustomerApp.Body.Order.PayableWith) {
		err = instance.publishToOrderCard(ctx, &orderCustomerApp)
	} else if modelOrder != nil &&  !orderProcessed && utils.StringInSlice("pix", orderCustomerApp.Body.Order.PayableWith) {
		err = instance.publishToOrderPix(ctx, &orderCustomerApp)
	}

	if err != nil {
		msgErr := fmt.Sprintf("Não foi possível processar Order customer da transação %s. err: %v", transactionId, err)

		log.Printf(msgErr)

		instance.mustSendToWebHook(ctx, message, &orderCustomerApp, errors.New(msgErr), false)
		_ = message.Message.Nack(false, !(message.Message.DeliveryTag >= message.DeliveryLimit))

		ctx.Done()
		return
	}
	_ = message.Message.Ack(false)
	ctx.Done()
}
