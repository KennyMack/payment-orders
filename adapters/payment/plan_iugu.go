package payment

import (
	"context"
	"encoding/json"
	"fmt"
	"payment_orders/utils/environment"
	"payment_orders/utils/request"
)

const (
	createPlanUrl          = "%s/plans?"
	changePlanUrl          = "%s/plans/%s?"
	removePlanUrl          = "%s/plans/%s?"
	getPlanByIdUrl         = "%s/plans/%s?"
	getPlanByIdentifierUrl = "%s/plans/identifier/%s?"
	getAllPlansUrl         = "%s/plans/?%s&%s"
)

type IUGUPlan struct{
	baseIUGUURL string
}

func NewIUGUPlan() *IUGUPlan {
	return &IUGUPlan{
		baseIUGUURL: environment.GetIUGU_BASE_URL(),
	}
}

func (instance *IUGUPlan) CreatePlan(context context.Context, plan IuguPlanBody) (interface{}, error) {
	url := fmt.Sprintf(createPlanUrl, instance.baseIUGUURL)

	objJson, err := json.Marshal(plan)

	if err != nil {
		return nil, err
	}
	header := getIUGUAuthHeader()

	result, err := request.Post(context, url, objJson, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUPlan) ChangePlan(context context.Context, planId string, plan IuguPlanBody) (interface{}, error) {
	url := fmt.Sprintf(changePlanUrl, instance.baseIUGUURL, planId)

	objJson, err := json.Marshal(plan)

	if err != nil {
		return nil, err
	}
	header := getIUGUAuthHeader()

	result, err := request.Put(context, url, objJson, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUPlan) RemovePlan(context context.Context, planId string) (interface{}, error) {
	url := fmt.Sprintf(removePlanUrl, instance.baseIUGUURL, planId)

	header := getIUGUAuthHeader()

	result, err := request.Delete(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUPlan) GetPlanById(context context.Context, planId string) (interface{}, error) {
	url := fmt.Sprintf(getPlanByIdUrl, instance.baseIUGUURL, planId)

	header := getIUGUAuthHeader()

	result, err := request.Get(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUPlan) GetPlanByIdentifier(context context.Context, identifier string) (interface{}, error) {
	url := fmt.Sprintf(getPlanByIdentifierUrl, instance.baseIUGUURL, identifier)

	header := getIUGUAuthHeader()

	result, err := request.Get(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUPlan) GetAllPlans(context context.Context, start, limit int) (interface{}, error) {
	urlLimit := ""
	urlStart := fmt.Sprintf("start=%v", start)

	if limit > 0 {
		urlLimit = fmt.Sprintf("limit=%v", limit)
	}

	url := fmt.Sprintf(getAllPlansUrl, instance.baseIUGUURL, urlLimit, urlStart)

	header := getIUGUAuthHeader()

	result, err := request.Get(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}
