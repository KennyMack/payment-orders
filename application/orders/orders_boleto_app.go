package orders

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	dtoOrders "payment_orders/adapters/dto/orders"
	"payment_orders/adapters/queue"
	"payment_orders/application/transactions"
	"payment_orders/utils"
)

func NewAppOrderBoleto(
	publishAmqpConn queue.IRabbitConnection,
	serviceOrders IOrdersService,
	serviceTransaction transactions.ITransactionsService) *AppOrderBoleto {
	return &AppOrderBoleto{
		publishAmqpConn: publishAmqpConn,
		serviceOrders: serviceOrders,
		serviceTransaction: serviceTransaction,
	}
}

func (instance *AppOrderBoleto) publishMessage(ctx context.Context, message string) error {
	configProducer := queue.MakeSyncRabbitMQCfg()
	producer := queue.MakeNewProducerFactory(instance.publishAmqpConn, configProducer)
	err := producer.CreateProducer(false)

	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}
	producer.PublishStringMessage(message)

	_ = instance.publishAmqpConn.CloseConnection()
	return nil
}

func (instance *AppOrderBoleto) publishMessageToWebHook(ctx context.Context, message string) error {
	configProducer := queue.MakeWebHookRabbitMQCfg()
	producer := queue.MakeNewProducerFactory(instance.publishAmqpConn, configProducer)
	err := producer.CreateProducer(false)

	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}
	producer.PublishStringMessage(message)

	_ = instance.publishAmqpConn.CloseConnection()

	return nil
}

func (instance *AppOrderBoleto) sendToWebHook(ctx context.Context, event, message string, err error) {
	errText := ""
	if err != nil {
		errText = utils.ScapeChars(err.Error())
	}
	s := fmt.Sprintf(`{"event": "%v", "resource": %v, "error": "%v" }`, event, message, errText)

	_ =  instance.publishMessageToWebHook(ctx, s)
}

func (instance *AppOrderBoleto) mustSendToWebHook(ctx context.Context,
	message *queue.ContentMessageInstance,
	dtoCustomer *dtoOrders.DTOOrders,
	err error) {
	if message.Message.DeliveryTag >= message.DeliveryLimit {
		log.Printf("Webhook - Order %s - UniqueIdentifier %s", dtoCustomer.ID.Hex(), dtoCustomer.UniqueIdentifier)
		jsonHook, errFind := json.Marshal(dtoCustomer)
		if errFind == nil {
			instance.sendToWebHook(ctx, "ORDER.REGISTER", string(jsonHook), err)
		}
	}
}

func (instance *AppOrderBoleto) NewMessage(message *queue.ContentMessageInstance) {
	orderApp := dtoOrders.DTOOrderCustomerCancellation{}

	errParse := json.Unmarshal(message.Body, &orderApp)
	if errParse != nil {
		log.Printf("Falha ao converter o JSON. err: %v", errParse)
		log.Printf("Body: %v ", string(message.Body))
		_ = message.Message.Nack(false, false)
		return
	}

	operation := orderApp.Operation
	transactionId := orderApp.Transaction
	log.Printf("Processando transação: %v", transactionId)

	ctx := context.Background()
	_, _ = instance.serviceTransaction.StartTransaction(ctx, transactionId, operation)

	dtoOrder := &orderApp.Body.Order

	dbCustomer, err := instance.processOrder(ctx, orderApp.Operation, dtoOrder)

	if err != nil {
		msgErr := fmt.Sprintf("Não foi possível processar a transação %s. err: %v", orderApp.Transaction, err)

		log.Printf(msgErr)
		_, _ = instance.serviceTransaction.AddMessageError(ctx, transactionId, operation, msgErr)

		instance.mustSendToWebHook(ctx, message, dtoOrder, errors.New(msgErr))
		_ = message.Message.Nack(false, !(message.Message.DeliveryTag >= message.DeliveryLimit))

		ctx.Done()
		return
	}
	_, _ = instance.serviceTransaction.UpdateOrderId(ctx, transactionId, operation, dbCustomer.GetModel().ID.Hex())

	_, _ = instance.serviceTransaction.UpdateToCreated(ctx, transactionId, operation)

	_ = message.Message.Ack(false)

	s, _ := json.Marshal(orderApp)
	_ = instance.publishMessage(ctx, string(s))
	ctx.Done()
}

func (instance *AppOrderBoleto) processOrder(ctx context.Context, operation string, dtoOrder *dtoOrders.DTOOrders) (IOrders, error) {
	var dbOrder IOrders = nil
	var err error

	if operation == queue.CREATE ||
		operation == queue.CHANGE {
		oldOrder, _ := instance.serviceOrders.GetByOwnId(ctx, dtoOrder.OwnId)

		if oldOrder == nil {
			dbOrder, err = instance.serviceOrders.Create(ctx, dtoOrder)
		} else {
			dtoOrder.ID = oldOrder.ID
			dbOrder, err = instance.serviceOrders.Change(ctx, oldOrder.ID.Hex(), dtoOrder)
		}
	} else if operation == queue.REMOVE {
		oldCustomer, err := instance.serviceOrders.GetByOwnId(ctx, dtoOrder.OwnId)
		if err != nil {
			return nil, err
		}
		dbOrder, err = instance.serviceOrders.Remove(ctx, oldCustomer.ID.Hex())
	}

	return dbOrder, err
}
