package payment

import (
	b64 "encoding/base64"
	"payment_orders/utils/environment"
)

func getIUGUAuthHeader() map[string]string {
	header := make(map[string]string)
	token := environment.GetIUGU_TOKEN()
	header["Authorization"] = "Basic " + b64.URLEncoding.EncodeToString([]byte(token))

	return header
}
