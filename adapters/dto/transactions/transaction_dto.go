package transactions

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"payment_orders/adapters/db"
	"time"
)

type DTOTransactions struct {
	db.DefaultModel   `valid:"-"bson:",inline"`
	MovementDate       time.Time `valid:"required~Data movimento"`
	TransactionId      string `valid:"required~Id da transação"`
	Operation          string `valid:"required~Operação"`
	OrderId            primitive.ObjectID `valid:"-"`
	CustomerId         primitive.ObjectID `valid:"-"`
	PaymentId          primitive.ObjectID `valid:"-"`
	CancellationId      primitive.ObjectID `valid:"-"`
	Created            bool `valid:"-"`
	SentToServer       bool `valid:"-"`
	Completed          bool `valid:"-"`
	RetryCount         int `valid:"-"`
	ErrorMessage       []string `valid:"-"`
}

func NewDTOTransactions() *DTOTransactions {
	transaction := DTOTransactions{}
	return &transaction
}
