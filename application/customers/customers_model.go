package customers

import (
	"context"
	"errors"
	validator "github.com/asaskevich/govalidator"
	"payment_orders/adapters/db"
	"payment_orders/adapters/dto/customers"
	baseRepository "payment_orders/application/baseinterfaces"
	"payment_orders/utils/validations"
)

func init () {
	validator.SetFieldsRequiredByDefault(true)
}

type MCustomerAddress struct {
	Street string `json:"street" bson:"street" valid:"required~Rua é obrigatório"`
	Complement string `json:"complement" bson:"complement" valid:"-"`
	Neighborhood string `json:"neighborhood" bson:"neighborhood" valid:"required~Endereço é obrigatório"`
	City string `json:"city" bson:"city" valid:"required~Cidade é obrigatório"`
	State string `json:"state" bson:"state" valid:"required~Estado é obrigatório"`
	ZipCode string `json:"zipCode" bson:"zipCode" valid:"required~Cep é obrigatório"`
	Number string `json:"number" bson:"number" valid:"required~Número do endereço é obrigatório"`
	PhoneNumber string `json:"phoneNumber" bson:"phoneNumber" valid:"required~Telefone é obrigatório"`
}

type MCustomerCreditCard struct {
	Brand string `bson:"brand" valid:"required~Bandeira do cartão é obrigatória"`
	First6 string `bson:"first6" valid:"required~Primeiros digitos do cartão é obrigatório"`
	Last4 string `bson:"last4" valid:"required~Ultimos digitos do cartão é obrigatório"`
	HolderName string `bson:"holderName" valid:"required~Nome do dono do cartão é obrigatório"`
	Year string `bson:"year" valid:"required~Ano de expiração é obrigatório"`
	Month string `bson:"month" valid:"required~Mês de expiração é obrigatório"`
	Test bool `bson:"test" valid:"required~Tipo do cartão é obrigatório"`
	DisplayNumber string `bson:"displayNumber" valid:"required~Numero no cartão é obrigatório"`
	SetAsDefault bool `bson:"setAsDefault" valid:"required"`
	Description string `bson:"description" valid:"required~Descrição do cartão é obrigatório"`
	IdPaymentToken string `bson:"idPaymentToken" valid:"required~Token de pagamento é obrigatório"`
}

type MCustomers struct {
	db.DefaultModel `valid:"-"bson:",inline"`
	Email string `bson:"email" valid:"required~E-mail é obrigatório"`
	UniqueIdentifier string `bson:"uniqueIdentifier" valid:"required~Identificador unico é obrigatório"`
	FullName string `bson:"fullName" valid:"required~Nome é obrigatório"`
	TypeDoc string `bson:"typeDoc" valid:"required~Tipo do documento é obrigatório"`
	NumberDoc string `bson:"numberDoc" valid:"required~Número do documento é obrigatório"`
	CreditCard []*MCustomerCreditCard `bson:"creditCard,omitempty" valid:"optional"`
	Address *MCustomerAddress `bson:"address,omitempty" valid:"optional"`
	OwnId string `bson:"ownId" valid:"required~Identificador de origem é obrigatório"`
}

type ICustomers interface {
	IsValid() (bool, error)
	GetModel() *MCustomers
	GetDTO() *customers.DTOCustomers
}

type ICustomersService interface {
	baseRepository.IBaseService
	Create(ctx context.Context, model *customers.DTOCustomers) (*MCustomers, error)
	Change(ctx context.Context, id string, model *customers.DTOCustomers) (*MCustomers, error)
	Remove(ctx context.Context, id string) (*MCustomers, error)
	GetById(ctx context.Context, id string) (*MCustomers, error)
	GetByOwnId(ctx context.Context, id string) (*MCustomers, error)
	UpdateUniqueIdentifier(ctx context.Context, id, uniqueIdentifier string) (*MCustomers, error)
	AddCardToCustomer(ctx context.Context, id string, card *customers.DTOCustomerCreditCard) (*MCustomers, error)
	RemoveCardFromCustomer(ctx context.Context, id string, idPaymentToken string) (*MCustomers, error)
	GetAll(ctx context.Context, start, limit int) ([]*MCustomers, error)
}

type ICustomerReader interface{
	GetById(ctx context.Context, id string) (*MCustomers, error)
	GetByOwnId(ctx context.Context, id string) (*MCustomers, error)
	GetAll(ctx context.Context, start, limit int) ([]*MCustomers, error)
}

type ICustomerWriter interface {
	Save(ctx context.Context, customer ICustomers) (*MCustomers, error)
	RemoveCardFromCustomer(ctx context.Context, id string, idPaymentToken string) (*MCustomers, error)
	Remove(ctx context.Context, customer ICustomers) (*MCustomers, error)
}

type ICustomerRepository interface {
	baseRepository.IBaseRepository
	ICustomerReader
	ICustomerWriter
}

func NewCustomerModel(dtoModel *customers.DTOCustomers) *MCustomers {
	customer := MCustomers{
		UniqueIdentifier: "AGUARDANDO",
		TypeDoc: TYPEDOCCPF,
		CreditCard: []*MCustomerCreditCard{},
		Address: NewCustomerAddressModel(nil),
	}

	if dtoModel != nil {
		customer.ID = dtoModel.ID
		customer.Email = dtoModel.Email
		customer.CreatedAt = dtoModel.CreatedAt
		customer.UpdatedAt = dtoModel.UpdatedAt
		customer.UniqueIdentifier = dtoModel.UniqueIdentifier
		customer.FullName = dtoModel.FullName
		customer.TypeDoc = dtoModel.TypeDoc
		customer.NumberDoc = dtoModel.NumberDoc
		customer.OwnId = dtoModel.OwnId

		if dtoModel.CreditCard != nil {
			for i := 0;i < len(dtoModel.CreditCard); i++  {
				customer.CreditCard =
					append(customer.CreditCard,
						NewCustomerCreditCardModel(dtoModel.CreditCard[i]))
			}
		}

		if dtoModel.Address != nil {
			customer.Address = NewCustomerAddressModel(dtoModel.Address)
		}
	}

	return &customer
}

func NewCustomerCreditCardModel(dtoModel *customers.DTOCustomerCreditCard) *MCustomerCreditCard {
	creditCard := MCustomerCreditCard{
		Test: true,
		SetAsDefault: true,
	}

	if dtoModel != nil {
		creditCard.Brand = dtoModel.Brand
		creditCard.First6 = dtoModel.First6
		creditCard.Last4 = dtoModel.Last4
		creditCard.HolderName = dtoModel.HolderName
		creditCard.Year = dtoModel.Year
		creditCard.Month = dtoModel.Month
		creditCard.Test = dtoModel.Test
		creditCard.DisplayNumber = dtoModel.DisplayNumber
		creditCard.SetAsDefault = dtoModel.SetAsDefault
		creditCard.Description = dtoModel.Description
		creditCard.IdPaymentToken = dtoModel.IdPaymentToken
	}

	return &creditCard
}

func NewCustomerAddressModel(dtoModel *customers.DTOCustomerAddress) *MCustomerAddress {
	address := MCustomerAddress{}

	if dtoModel != nil {
		address.Street = dtoModel.Street
		address.Complement = dtoModel.Complement
		address.Neighborhood = dtoModel.Neighborhood
		address.City = dtoModel.City
		address.State = dtoModel.State
		address.ZipCode = dtoModel.ZipCode
		address.Number = dtoModel.Number
		address.PhoneNumber = dtoModel.PhoneNumber
	}

	return &address
}

const (
	TYPEDOCCPF = "CPF"
	TYPEDOCCNPJ = "CNPJ"
)

func (customer *MCustomers) IsValid() (bool, error) {
	_, err := validator.ValidateStruct(customer)
	if err != nil {
		return false, err
	}

	if customer.TypeDoc == TYPEDOCCPF &&
		!validations.IsCPF(customer.NumberDoc) {
		return false, errors.New("cpf informado não é válido")
	} else if customer.TypeDoc == TYPEDOCCNPJ &&
		!validations.IsCNPJ(customer.NumberDoc) {
		return false, errors.New("cnpj informado não é válido")
	} else if customer.TypeDoc != TYPEDOCCNPJ &&
		customer.TypeDoc != TYPEDOCCPF {
		return false, errors.New("tipo de documento informado não é válido")
	}

	return true, nil
}

func (customer *MCustomers) GetModel() *MCustomers {
	return customer
}

func (customer *MCustomers) GetDTO() *customers.DTOCustomers {
	dto := customers.DTOCustomers{
		DefaultModel: customer.DefaultModel,
		Email: customer.Email,
		UniqueIdentifier: customer.UniqueIdentifier,
		FullName: customer.FullName,
		TypeDoc: customer.TypeDoc,
		NumberDoc: customer.NumberDoc,
		OwnId: customer.OwnId,
		CardToken: "",
		CreditCard: []*customers.DTOCustomerCreditCard{},
		Address: customers.NewDTOCustomerAddress(),
	}

	if customer.CreditCard != nil {
		for i := 0; i < len(customer.CreditCard); i++  {
			dtoCard := customers.NewDTOCustomerCreditCard()

			dtoCard.Brand = customer.CreditCard[i].Brand
			dtoCard.First6 = customer.CreditCard[i].First6
			dtoCard.Last4 = customer.CreditCard[i].Last4
			dtoCard.HolderName = customer.CreditCard[i].HolderName
			dtoCard.Year = customer.CreditCard[i].Year
			dtoCard.Month = customer.CreditCard[i].Month
			dtoCard.Test = customer.CreditCard[i].Test
			dtoCard.DisplayNumber = customer.CreditCard[i].DisplayNumber
			dtoCard.SetAsDefault = customer.CreditCard[i].SetAsDefault
			dtoCard.Description = customer.CreditCard[i].Description
			dtoCard.IdPaymentToken = customer.CreditCard[i].IdPaymentToken

			dto.CreditCard =
				append(dto.CreditCard, dtoCard)
		}
	}

	if customer.Address != nil {
		dtoAddress := customers.NewDTOCustomerAddress()

		dtoAddress.Street = customer.Address.Street
		dtoAddress.Complement = customer.Address.Complement
		dtoAddress.Neighborhood = customer.Address.Neighborhood
		dtoAddress.City = customer.Address.City
		dtoAddress.State = customer.Address.State
		dtoAddress.ZipCode = customer.Address.ZipCode
		dtoAddress.Number = customer.Address.Number
		dtoAddress.PhoneNumber = customer.Address.PhoneNumber


		dto.Address = dtoAddress
	}

	return &dto
}
