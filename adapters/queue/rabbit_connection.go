package queue

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"log"
	"payment_orders/utils/environment"
)

var amqpuri string

type IRabbitConnection interface {
	OpenConnection() error
	CloseConnection() error
	IsOpen() bool
	GetConnection() *amqp.Connection
}

type RabbitConnectionInstance struct {
	Connection    *amqp.Connection
}

func MakeNewRabbitConnection() *RabbitConnectionInstance {
	return &RabbitConnectionInstance{
		Connection: nil,
	}
}

func (instance *RabbitConnectionInstance) failOnError(err error, msg string) {
	if err != nil {
		log.Printf("%s: %s", msg, err)
	}
}

func (instance *RabbitConnectionInstance) IsOpen() bool {
	if instance.Connection == nil {
		return false
	}

	return !instance.Connection.IsClosed()
}

func (instance *RabbitConnectionInstance) OpenConnection() error {
	// Buscando os dados para conexão
	amqpuri = environment.GetAMPQ_URI()
	if instance.Connection == nil {
		var err error
		// Conectando ao Rabbit
		instance.Connection, err = amqp.Dial(amqpuri)
		instance.failOnError(err, "Não foi possível conectar ao RABBITMQ")
		if err != nil {
			return err
		}
	} else if instance.Connection.IsClosed() {
		// Conectando ao Rabbit
		newConnection, err := amqp.Dial(amqpuri)
		instance.failOnError(err, "Não foi possível conectar ao RABBITMQ")
		if err != nil {
			return err
		}
		instance.Connection = newConnection
	}

	return nil
}

func (instance *RabbitConnectionInstance) CloseConnection() error {
	if instance.IsOpen() {
		return instance.Connection.Close()
	}
	return nil
}

func (instance *RabbitConnectionInstance) GetConnection() *amqp.Connection {
	return instance.Connection
}
