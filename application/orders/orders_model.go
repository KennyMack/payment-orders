package orders

import (
	"context"
	"errors"
	"fmt"
	validator "github.com/asaskevich/govalidator"
	"payment_orders/adapters/db"
	dtoOrders "payment_orders/adapters/dto/orders"
	baseRepository "payment_orders/application/baseinterfaces"
	"payment_orders/utils"
	"payment_orders/utils/validations"
)

func init () {
	validator.SetFieldsRequiredByDefault(true)
}

const (
	ALL = "all"
	CREDIT_CARD = "credit_card"
	BANK_SLIP = "bank_slip"
	PIX = "pix"
)

type MOrderItems struct {
	Description string `bson:"description" valid:"required~Descrição do item obrigatória"`
	Quantity int `bson:"quantity" valid:"required~Quantidade do item obrigatória"`
	PriceCents int `bson:"priceCents" valid:"required~Valor do item obrigatória"`
}

type MOrderPayerAddress struct {
	Street string `json:"street" bson:"street" valid:"optional"`
	Complement string `json:"complement" bson:"complement" valid:"optional"`
	District string `json:"district" bson:"district" valid:"optional"`
	City string `json:"city" bson:"city" valid:"optional"`
	State string `json:"state" bson:"state" valid:"optional"`
	Country string `json:"country" bson:"country" valid:"optional"`
	ZipCode string `json:"zip_code" bson:"zipCode" valid:"optional"`
	Number string `json:"number" bson:"number" valid:"optional"`
}

type MOrderPayer struct {
	CpfCnpj string `json:"cpf_cnpj" bson:"cpfCnpj" valid:"required~Documento é obrigatório"`
	Name string `json:"name" bson:"name" valid:"required~Nome é obrigatório"`
	PhonePrefix string `json:"phonePrefix" bson:"phonePrefix" valid:"optional"`
	Phone string `json:"phone" bson:"phone" valid:"optional"`
	Email string `json:"email" bson:"email" valid:"required~Email é obrigatório"`
	Address *MOrderPayerAddress `json:"address" bson:"address" valid:"optional"`
}

type MOrders struct {
	db.DefaultModel `valid:"-"bson:",inline"`
	UniqueIdentifier string `json:"uniqueIdentifier" bson:"uniqueIdentifier" valid:"required~UniqueIdentifier obrigatório"`
	Status string `json:"status" bson:"status" valid:"required~Status obrigatório"`
	OwnId string `json:"ownId" bson:"ownId" valid:"required~OwnId obrigatório"`
	OrderId string `json:"orderId" bson:"orderId" valid:"required~orderId obrigatório"`
	Email string `json:"email" bson:"email" valid:"required~Email obrigatório"`
	DueDate string `json:"dueDate" bson:"dueDate" valid:"required~DueDate obrigatório"`
	EnsureWorkdayDueDate bool `json:"ensureWorkdayDueDate" bson:"ensureWorkdayDueDate" valid:"required~EnsureWorkdayDueDate obrigatório"`
	Items []*MOrderItems `json:"items" bson:"items" valid:"required~Items obrigatório"`
	ReturnUrl string `json:"returnUrl" bson:"returnUrl" valid:"optional"`
	ExpiredUrl string `json:"expiredUrl" bson:"expiredUrl" valid:"optional"`
	NotificationUrl string `json:"notificationUrl" bson:"notificationUrl" valid:"optional"`
	IgnoreCanceledEmail bool `json:"ignoreCanceledEmail" bson:"ignoreCanceledEmail" valid:"optional"`
	Fines bool `json:"fines" bson:"fines" valid:"optional"`
	LatePaymentFine int `json:"latePaymentFine" bson:"latePaymentFine" valid:"optional"`
	LatePaymentFineCents int `json:"latePaymentFineCents" bson:"latePaymentFineCents" valid:"optional"`
	PerDayInterest bool `json:"perDayInterest" bson:"perDayInterest" valid:"optional"`
	PerDayInterestValue int `json:"perDayInterestValue" bson:"perDayInterestValue" valid:"optional"`
	PerDayInterestCents int `json:"perDayInterestCents" bson:"perDayInterestCents" valid:"optional"`
	DiscountCents int `json:"discountCents" bson:"discountCents" valid:"optional"`
	CustomerId string `json:"customerId" bson:"customerId" valid:"required~CustomerId obrigatório"`
	IgnoreDueEmail bool `json:"ignoreDueEmail" bson:"ignoreDueEmail" valid:"optional"`
	SubscriptionId string `json:"subscriptionId" bson:"subscriptionId" valid:"optional"`
	PayableWith []string `json:"payableWith" bson:"payableWith" valid:"required~Payable With obrigatório"`
	EarlyPaymentDiscount bool `json:"earlyPaymentDiscount" bson:"earlyPaymentDiscount" valid:"optional"`
	EarlyPaymentDiscounts []string `json:"earlyPaymentDiscounts" bson:"earlyPaymentDiscounts" valid:"optional"`
	Payer *MOrderPayer `json:"payer" bson:"payer" valid:"required~Pagador é obrigatório"`
	CreditCard string `json:"creditCard" bson:"creditCard" valid:"optional"`
	Months int `json:"months" bson:"months" valid:"optional"`
	BankSlipExtraDays int `json:"bank_slip_extra_days" bson:"bank_slip_extra" valid:"optional"`
	KeepDunning bool `json:"keep_dunning" bson:"keep_dunning" valid:"optional"`
	DigitalLine string `json:"digitalLine" bson:"digitalLine" valid:"optional"`
	BarcodeData string `json:"barcodeData" bson:"barcodeData" valid:"optional"`
	BarcodeUrl string `json:"barcodeUrl" bson:"barcodeUrl" valid:"optional"`
	SecureUrl string `json:"secureUrl" bson:"secureUrl" valid:"optional"`
	SecureId string `json:"secureId" bson:"secureId" valid:"optional"`
	ChargePdf string `json:"pdf" bson:"pdf" valid:"optional"`
	ChargeMessage string `json:"chargeMessage" bson:"chargeMessage" valid:"optional"`
	ChargeStatus string `json:"chargeStatus" bson:"chargeStatus" valid:"optional"`
	ChargeToken string `json:"chargeToken" bson:"chargeToken" valid:"optional"`
	LR string `json:"lr" bson:"lr" valid:"optional"`
	Message string `json:"message" bson:"message" valid:"optional"`
	ChargeSuccess bool `json:"chargeSuccess" bson:"chargeSuccess" valid:"optional"`
	ChargeInfoMessage string `json:"chargeInfoMessage" bson:"chargeInfoMessage" valid:"optional"`

	PixStatus string `json:"pixStatus" bson:"pixStatus" valid:"optional"`
	PixQrCode string `json:"pixQrCode" bson:"pixQrCode" valid:"optional"`
	PixQrCodeText string `json:"pixQrCodeText" bson:"pixQrCodeText" valid:"optional"`
	PixPayerCpfCnpj string `json:"pixPayerCpfCnpj" bson:"pixPayerCpfCnpj" valid:"optional"`
	PixPayerName string `json:"pixPayerName" bson:"pixPayerName" valid:"optional"`
	PixEndToEndId string `json:"pixEndToEndId" bson:"pixEndToEndId" valid:"optional"`
	PixEndToEndRefundId string `json:"pixEndToEndRefundId" bson:"pixEndToEndRefundId" valid:"optional"`
}

type MOrderTriggerInfo struct {
	Status string
	PaymentMethod string
	PaidCents int
	PaidAt string
}

type IOrders interface {
	IsValid() (bool, error)
	GetModel() *MOrders
	GetDTO() *dtoOrders.DTOOrders
}

type IOrdersService interface {
	baseRepository.IBaseService
	Create(ctx context.Context, model *dtoOrders.DTOOrders) (*MOrders, error)
	Change(ctx context.Context, id string, model *dtoOrders.DTOOrders) (*MOrders, error)
	Remove(ctx context.Context, id string) (*MOrders, error)
	GetById(ctx context.Context, id string) (*MOrders, error)
	GetByOwnId(ctx context.Context, id string) (*MOrders, error)
	GetByUniqueIdentifier(ctx context.Context, uniqueIdentifier string) (*MOrders, error)
	UpdateUniqueIdentifier(ctx context.Context, id, uniqueIdentifier string) (*MOrders, error)
	UpdateInfo(ctx context.Context, id, status, digitalLine, barcodeData, barcodeUrl, secureUrl, secureId string) (*MOrders, error)
	UpdateTriggerInfo(ctx context.Context, uniqueIdentifier string, info *MOrderTriggerInfo) (*MOrders, error)
	GetNextStatus(currentStatus string) []string
}

type IOrdersReader interface{
	GetById(ctx context.Context, id string) (*MOrders, error)
	GetByOwnId(ctx context.Context, id string) (*MOrders, error)
	GetByUniqueIdentifier(ctx context.Context, uniqueIdentifier string) (*MOrders, error)
}

type IOrdersWriter interface {
	Save(ctx context.Context, order IOrders) (*MOrders, error)
	Remove(ctx context.Context, order IOrders) (*MOrders, error)
}

type IOrdersRepository interface {
	baseRepository.IBaseRepository
	IOrdersReader
	IOrdersWriter
}

func NewOrderModel(dtoModel *dtoOrders.DTOOrders) *MOrders {
	order := MOrders{
		Status: "DRAFT",
		UniqueIdentifier: "AGUARDANDO",
		Items: make([]*MOrderItems, 0, 0),
		EarlyPaymentDiscounts: make([]string, 0, 0),
		PayableWith: []string{ ALL },
		IgnoreDueEmail: false,
		IgnoreCanceledEmail: false,
		EnsureWorkdayDueDate: true,
		LatePaymentFine: 0,
		LatePaymentFineCents: 0,
		CreditCard: "",
		Months: 1,
		BankSlipExtraDays: 0,
		KeepDunning: true,
		Payer: NewOrderPayerModel(nil),
	}

	if dtoModel != nil {
		order.ID = dtoModel.ID
		order.CreatedAt = dtoModel.CreatedAt
		order.UpdatedAt = dtoModel.UpdatedAt
		order.Status = dtoModel.Status
		order.OwnId = dtoModel.OwnId
		order.OrderId = dtoModel.OrderId
		order.Email = dtoModel.Email
		order.DueDate = dtoModel.DueDate
		order.UniqueIdentifier = dtoModel.UniqueIdentifier
		order.EnsureWorkdayDueDate = dtoModel.EnsureWorkdayDueDate
		order.ReturnUrl = dtoModel.ReturnUrl
		order.ExpiredUrl = dtoModel.ExpiredUrl
		order.NotificationUrl = dtoModel.NotificationUrl
		order.IgnoreCanceledEmail = dtoModel.IgnoreCanceledEmail
		order.Fines = dtoModel.Fines
		order.LatePaymentFine = dtoModel.LatePaymentFine
		order.LatePaymentFineCents = dtoModel.LatePaymentFineCents
		order.PerDayInterest = dtoModel.PerDayInterest
		order.PerDayInterestValue = dtoModel.PerDayInterestValue
		order.PerDayInterestCents = dtoModel.PerDayInterestCents
		order.DiscountCents = dtoModel.DiscountCents
		order.CustomerId = dtoModel.CustomerId
		order.IgnoreDueEmail = dtoModel.IgnoreDueEmail
		order.SubscriptionId = dtoModel.SubscriptionId
		order.PayableWith = dtoModel.PayableWith
		order.CreditCard = dtoModel.CreditCard
		order.Months = dtoModel.Months
		order.BankSlipExtraDays = dtoModel.BankSlipExtraDays
		order.KeepDunning = dtoModel.KeepDunning
		order.EarlyPaymentDiscount = dtoModel.EarlyPaymentDiscount
		order.EarlyPaymentDiscounts = dtoModel.EarlyPaymentDiscounts
		order.DigitalLine = dtoModel.DigitalLine
		order.BarcodeData = dtoModel.BarcodeData
		order.BarcodeUrl = dtoModel.BarcodeUrl
		order.SecureUrl = dtoModel.SecureUrl
		order.SecureId = dtoModel.SecureId

		order.ChargePdf = dtoModel.ChargePdf
		order.ChargeMessage = dtoModel.ChargeMessage
		order.ChargeStatus = dtoModel.ChargeStatus
		order.ChargeToken = dtoModel.ChargeToken
		order.ChargeSuccess = dtoModel.ChargeSuccess
		order.ChargeInfoMessage = dtoModel.ChargeInfoMessage

		order.PixStatus = dtoModel.PixStatus
		order.PixQrCode = dtoModel.PixQrCode
		order.PixQrCodeText = dtoModel.PixQrCodeText
		order.PixPayerCpfCnpj = dtoModel.PixPayerCpfCnpj
		order.PixPayerName = dtoModel.PixPayerName
		order.PixEndToEndId = dtoModel.PixEndToEndId
		order.PixEndToEndRefundId = dtoModel.PixEndToEndRefundId

		order.Payer = NewOrderPayerModel(dtoModel.Payer)

		if dtoModel.Items != nil  &&
			len(dtoModel.Items) > 0 {
			for i := 0;i < len(dtoModel.Items); i++  {
				order.Items =
					append(order.Items,
						NewOrderItemModel(dtoModel.Items[i]))
			}
		}

	}

	return &order
}

func NewOrderItemModel(dtoModel *dtoOrders.DTOOrderItems) *MOrderItems {
	orderItem := MOrderItems{}

	if dtoModel != nil {
		orderItem.Description = dtoModel.Description
		orderItem.PriceCents = dtoModel.PriceCents
		orderItem.Quantity = dtoModel.Quantity
	}

	return &orderItem
}

func NewOrderPayerAddressModel(dtoModel *dtoOrders.DTOOrderPayerAddress) *MOrderPayerAddress {
	payerAddress := MOrderPayerAddress{}

	if dtoModel != nil {
		payerAddress.Street = dtoModel.Street
		payerAddress.Complement = dtoModel.Complement
		payerAddress.District = dtoModel.District
		payerAddress.City = dtoModel.City
		payerAddress.State = dtoModel.State
		payerAddress.ZipCode = dtoModel.ZipCode
		payerAddress.Number = dtoModel.Number
		payerAddress.Country = dtoModel.Country
	}

	return &payerAddress
}

func NewOrderPayerModel(dtoModel *dtoOrders.DTOOrderPayer) *MOrderPayer {
	payer := MOrderPayer{
		Address: NewOrderPayerAddressModel(nil),
	}

	if dtoModel != nil {
		payer.Address = NewOrderPayerAddressModel(dtoModel.Address)
		payer.CpfCnpj = dtoModel.CpfCnpj
		payer.Name = dtoModel.Name
		payer.PhonePrefix = dtoModel.PhonePrefix
		payer.Phone = dtoModel.Phone
		payer.Email = dtoModel.Email
	}

	return &payer
}

func (order *MOrders) IsValid() (bool, error) {
	_, err := validator.ValidateStruct(order)
	if err != nil {
		return false, err
	}

	if order.Payer == nil {
		return false, errors.New("Informe o Pagador")
	}

	if order.Payer.Address == nil {
		return false, errors.New("Informe o endereço do Pagador")
	}

	cpfCnpj := utils.OnlyNumbers(order.Payer.CpfCnpj)

	if len(cpfCnpj) != 11 &&
		len(cpfCnpj) != 14 {
		return false, errors.New("CPF/CNPJ do Pagador é inválido")
	}

	if len(cpfCnpj) == 11 &&
		!validations.IsCPF(cpfCnpj) {
		return false, errors.New("CPF do Pagador é inválido")
	}

	if len(cpfCnpj) == 14 &&
		!validations.IsCNPJ(cpfCnpj) {
		return false, errors.New("CPF do Pagador é inválido")
	}

	options := []string{
		ALL,
		CREDIT_CARD,
		BANK_SLIP,
		PIX,
	}

	for i := 0; i < len(order.PayableWith); i++ {
		if !utils.StringInSlice(order.PayableWith[i], options) {
			return false, errors.New(fmt.Sprintf("Opção de pagamento \"%s\" não é válida.", order.PayableWith[i]))
		}
	}

	if utils.StringInSlice(CREDIT_CARD, order.PayableWith) &&
		len(order.CreditCard) <= 0 {
		return false, errors.New("token do cartão deve ser informado para o tipo 'credit_card'")
	}

	if len(order.Items) <= 0 {
		return true, errors.New("Nenhum item informado")
	}

	for i := 0; i < len(order.Items); i++ {
		if order.Items[i].PriceCents <= 0 {
			return false, errors.New(fmt.Sprintf("Valor do produto \"%s\" é inválido.", order.Items[i].Description))
		}
		if order.Items[i].Quantity <= 0 {
			return false, errors.New(fmt.Sprintf("Quantidade do produto \"%s\" é inválido.", order.Items[i].Description))
		}
	}

	return true, nil
}

func (order *MOrders) GetModel() *MOrders {
	return order
}

func (order *MOrders) GetDTO() *dtoOrders.DTOOrders {
	dto := dtoOrders.DTOOrders{
		DefaultModel: order.DefaultModel,
		Status: order.Status,
		OwnId: order.OwnId,
		OrderId: order.OrderId,
		Email: order.Email,
		DueDate: order.DueDate,
		UniqueIdentifier: order.UniqueIdentifier,
		EnsureWorkdayDueDate: order.EnsureWorkdayDueDate,
		ReturnUrl: order.ReturnUrl,
		ExpiredUrl: order.ExpiredUrl,
		NotificationUrl: order.NotificationUrl,
		IgnoreCanceledEmail: order.IgnoreCanceledEmail,
		Fines: order.Fines,
		LatePaymentFine: order.LatePaymentFine,
		LatePaymentFineCents: order.LatePaymentFineCents,
		PerDayInterest: order.PerDayInterest,
		PerDayInterestValue: order.PerDayInterestValue,
		PerDayInterestCents: order.PerDayInterestCents,
		DiscountCents: order.DiscountCents,
		CustomerId: order.CustomerId,
		IgnoreDueEmail: order.IgnoreDueEmail,
		SubscriptionId: order.SubscriptionId,
		PayableWith: order.PayableWith,
		CreditCard: order.CreditCard,
		Months: order.Months,
		BankSlipExtraDays: order.BankSlipExtraDays,
		KeepDunning: order.KeepDunning,
		EarlyPaymentDiscount: order.EarlyPaymentDiscount,
		EarlyPaymentDiscounts: order.EarlyPaymentDiscounts,
		DigitalLine: order.DigitalLine,
		BarcodeData: order.BarcodeData,
		BarcodeUrl: order.BarcodeUrl,
		SecureUrl: order.SecureUrl,
		SecureId: order.SecureId,
		ChargePdf: order.ChargePdf,
		ChargeMessage: order.ChargeMessage,
		ChargeStatus: order.ChargeStatus,
		ChargeToken: order.ChargeToken,
		ChargeSuccess: order.ChargeSuccess,
		ChargeInfoMessage: order.ChargeInfoMessage,
		PixStatus: order.PixStatus,
		PixQrCode: order.PixQrCode,
		PixQrCodeText: order.PixQrCodeText,
		PixPayerCpfCnpj: order.PixPayerCpfCnpj,
		PixPayerName: order.PixPayerName,
		PixEndToEndId: order.PixEndToEndId,
		PixEndToEndRefundId: order.PixEndToEndRefundId,

		Payer: dtoOrders.NewDTOOrderPayer(),

		Items: make([]*dtoOrders.DTOOrderItems, 0, 0),
	}

	if order.Payer != nil {
		if order.Payer.Address != nil {
			dto.Payer.Address.Street = order.Payer.Address.Street
			dto.Payer.Address.Complement = order.Payer.Address.Complement
			dto.Payer.Address.District = order.Payer.Address.District
			dto.Payer.Address.City = order.Payer.Address.City
			dto.Payer.Address.State = order.Payer.Address.State
			dto.Payer.Address.ZipCode = order.Payer.Address.ZipCode
			dto.Payer.Address.Number = order.Payer.Address.Number
			dto.Payer.Address.Country = order.Payer.Address.Country
		}

		dto.Payer.CpfCnpj = order.Payer.CpfCnpj
		dto.Payer.Name = order.Payer.Name
		dto.Payer.PhonePrefix = order.Payer.PhonePrefix
		dto.Payer.Phone = order.Payer.Phone
		dto.Payer.Email = order.Payer.Email
	}

	for i := 0; i < len(order.Items); i++ {
		dtoItem := dtoOrders.NewDTOOrderItems()
		dtoItem.PriceCents = order.Items[i].PriceCents
		dtoItem.Quantity = order.Items[i].Quantity
		dtoItem.Description = order.Items[i].Description

		dto.Items = append(dto.Items, dtoItem)
	}

	return &dto
}
