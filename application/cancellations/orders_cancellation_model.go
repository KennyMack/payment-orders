package cancellations

import (
	"context"
	"errors"
	validator "github.com/asaskevich/govalidator"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"payment_orders/adapters/db"
	"payment_orders/adapters/dto/cancellations"
	baseRepository "payment_orders/application/baseinterfaces"
)

func init () {
	validator.SetFieldsRequiredByDefault(true)
}

func NewOrdersCancellationModel(dtoModel *cancellations.DTOOrdersCancellation) *MOrdersCancellation {
	orderToCancel := MOrdersCancellation{
		Processed: false,
		UniqueIdentifier: "",
		CurrentStatus: "",
		FinalStatus: "",
		CancellationId: primitive.NilObjectID,
		Error: "",
	}

	if dtoModel != nil{
		orderToCancel.UniqueIdentifier = dtoModel.UniqueIdentifier
		orderToCancel.Processed = dtoModel.Processed
		orderToCancel.CurrentStatus = dtoModel.CurrentStatus
		orderToCancel.FinalStatus = dtoModel.FinalStatus
		orderToCancel.CancellationId = dtoModel.CancellationId
		orderToCancel.Error = dtoModel.Error
	}

	return &orderToCancel
}

type MOrdersCancellation struct {
	db.DefaultModel `valid:"-"bson:",inline"`
	UniqueIdentifier string `json:"uniqueIdentifier" bson:"uniqueIdentifier" valid:"required~UniqueIdentifier obrigatório"`
	Processed bool `bson:"processed" valid:"optional"`
	CurrentStatus string `bson:"currentStatus" valid:"-"`
	FinalStatus string `bson:"finalStatus" valid:"-"`
	CancellationId primitive.ObjectID `bson:"cancellationId,omitempty" valid:"-" bson:",omitempty"`
	Refunded bool `json:"refunded" bson:"refunded" valid:"optional"`
	Canceled bool `json:"canceled" bson:"canceled" valid:"optional"`
	Error string `bson:"error" valid:"-"`
}

type IOrdersCancellation interface {
	IsValid() (bool, error)
	GetModel() *MOrdersCancellation
	GetDTO() *cancellations.DTOOrdersCancellation
}

type IOrdersCancellationReader interface{
	GetById(ctx context.Context, id string) (*MOrdersCancellation, error)
	GetByUniqueIdentifier(ctx context.Context, uniqueIdentifier string, processed bool) (*MOrdersCancellation, error)
}

type IOrdersCancellationWriter interface{
	Save(ctx context.Context, order IOrdersCancellation) (*MOrdersCancellation, error)
	Remove(ctx context.Context, order IOrdersCancellation) (*MOrdersCancellation, error)
}

type IOrdersCancellationRepository interface {
	baseRepository.IBaseRepository
	IOrdersCancellationReader
	IOrdersCancellationWriter
}

type IOrdersCancellationService interface {
	baseRepository.IBaseService
	Create(ctx context.Context, model *cancellations.DTOOrdersCancellation) (*MOrdersCancellation, error)
	Change(ctx context.Context, id string, model *cancellations.DTOOrdersCancellation) (*MOrdersCancellation, error)
	Remove(ctx context.Context, id string) (*MOrdersCancellation, error)
	GetById(ctx context.Context, id string) (*MOrdersCancellation, error)
	GetByUniqueIdentifier(ctx context.Context, uniqueIdentifier string, processed bool) (*MOrdersCancellation, error)
	UpdateToProcessed(ctx context.Context, id string) (*MOrdersCancellation, error)
	UpdateToCanceled(ctx context.Context, id string) (*MOrdersCancellation, error)
	UpdateToRefunded(ctx context.Context, id string) (*MOrdersCancellation, error)
	UpdateStatus(ctx context.Context, id string, currentStatus string, finalStatus string) (*MOrdersCancellation, error)
	UpdateErrorMessage(ctx context.Context, id string, errorMessage string) (*MOrdersCancellation, error)
}

func (orderToCancel *MOrdersCancellation) IsValid() (bool, error) {
	_, err := validator.ValidateStruct(orderToCancel)
	if err != nil {
		return false, err
	}

	if orderToCancel.UniqueIdentifier == ""  {
		return false, errors.New("Código do pedido Obrigatório")
	}

	return true, nil
}

func (orderToCancel *MOrdersCancellation) GetModel() *MOrdersCancellation {
	return orderToCancel
}

func (orderToCancel *MOrdersCancellation) GetDTO() *cancellations.DTOOrdersCancellation {
	dto := cancellations.DTOOrdersCancellation{
		DefaultModel: orderToCancel.DefaultModel,
		UniqueIdentifier: orderToCancel.UniqueIdentifier,
		Processed: orderToCancel.Processed,
		CurrentStatus: orderToCancel.CurrentStatus,
		FinalStatus: orderToCancel.FinalStatus,
		CancellationId: orderToCancel.CancellationId,
		Refunded: orderToCancel.Refunded,
		Canceled: orderToCancel.Canceled,
		Error: orderToCancel.Error,
	}

	return &dto
}
