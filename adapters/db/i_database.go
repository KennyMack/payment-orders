package db

type IDatabase interface {
	Open() error
	Close() error
}
