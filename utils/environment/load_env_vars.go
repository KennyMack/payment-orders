package environment

import (
	"github.com/joho/godotenv"
	"log"
	"os"
	"payment_orders/utils"
)

func LoadEnvironment() error {
	applicationArgs := os.Args[1:]
	var currentDir string
	var err error

	if len(applicationArgs) <= 0 {
		currentDir, err = os.Getwd()
	} else {
		currentDir = applicationArgs[0]
	}

	if currentDir == "DOCKER" {
		return nil
	}

	if err != nil {
		log.Fatalf("Não foi possível carregar o diretório inicial do projeto. Err: %s", err)
	}
	log.Printf("Carregando os arquivos de ambiente da pasta: %s", currentDir)

	return loadEnv(currentDir)
}


func loadEnv(path string) error {
	exists, err := utils.FileExists(path + "/.env")

	if !exists {
		log.Fatalf("Arquivo .env não localizado. Err: %s", err)
	}

	err = godotenv.Load(path + "/.env")
	if err != nil {
		log.Fatalf("Arquivo .env não foi carregado. Err: %s", err)
	}

	exists, err = utils.FileExists(path + "/.env.development")
	if exists {
		err = godotenv.Overload(path + "/.env.development")
		if err != nil {
			log.Fatalf("Arquivo .env.development não foi carregado. Err: %s", err)
		}
		return err
	}

	exists, err = utils.FileExists(path + "/.env.production")
	if exists {
		err = godotenv.Overload(path + "/.env.production")
		if err != nil {
			log.Fatalf("Arquivo .env.production não foi carregado. Err: %s", err)
		}
		return err
	}
	return nil
}
