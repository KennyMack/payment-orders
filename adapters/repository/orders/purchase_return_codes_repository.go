package orders

import (
	"context"
	"errors"
	"fmt"
	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson"
	"payment_orders/adapters/db"
	"payment_orders/application/orders"
)

type RepositoryPurchaseReturnCode struct{
	database db.IMongoDBInstance
	collectionName string
}

func (instance *RepositoryPurchaseReturnCode) OpenConnection() error {
	return instance.database.Open()
}

func (instance *RepositoryPurchaseReturnCode) CloseConnection() error {
	return instance.database.Close()
}

func NewRepositoryPurchaseReturnCode(database db.IMongoDBInstance) *RepositoryPurchaseReturnCode {
	return &RepositoryPurchaseReturnCode{
		database: database,
		collectionName: "PurchaseReturnCodes",
	}
}

func (instance *RepositoryPurchaseReturnCode) GetByLR(ctx context.Context, id string) (*orders.MReturnCode, error) {
	result := &orders.MReturnCode{}

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	cur := collection.FindOne(ctx, bson.M{ "LR": id})

	err := cur.Decode(&result)

	collection = nil

	if err != nil {
		return nil, err
	}

	if result.ID.IsZero() {
		return nil, errors.New(fmt.Sprintf("Nenhum registro encontrado para o id: %v", id))
	}

	return result, nil
}
