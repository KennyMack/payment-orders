package customers

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"payment_orders/adapters/dto/customers"
	"strings"
)

type ServiceCustomers struct {
	repository ICustomerRepository
}

func NewServiceCustomers(repository ICustomerRepository) *ServiceCustomers {
	return &ServiceCustomers{
		repository: repository,
	}
}

func (instance *ServiceCustomers) OpenConnection() error {
	return instance.repository.OpenConnection()
}

func (instance *ServiceCustomers) CloseConnection() error {
	return instance.repository.CloseConnection()
}

func (instance *ServiceCustomers) Create(ctx context.Context, model *customers.DTOCustomers) (*MCustomers, error) {
	dbModel := NewCustomerModel(model)

	_, err := dbModel.IsValid()

	if err != nil {
		return nil, err
	}

	if !dbModel.ID.IsZero() {
		return nil, errors.New("Id deve ser nulo para novos clientes")
	}

	newModel, err := instance.repository.Save(ctx, dbModel)

	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceCustomers) Change(ctx context.Context, id string, model *customers.DTOCustomers) (*MCustomers, error) {
	dbModel := NewCustomerModel(model)

	_, err := dbModel.IsValid()

	if err != nil {
		return nil, err
	}

	if dbModel.ID.IsZero() {
		return nil, errors.New("Id deve ser informado")
	}

	oldModel, err := instance.repository.GetById(ctx, dbModel.ID.Hex())

	if oldModel == nil {
		return nil, errors.New( fmt.Sprintf("Nenhum registro encontrado para o id informado: Id: %v", dbModel.ID))
	}
	dbModel.CreatedAt = oldModel.CreatedAt

	if (len(oldModel.UniqueIdentifier) > 0 ||
		strings.ToUpper(oldModel.UniqueIdentifier) != "AGUARDANDO") &&
		strings.ToUpper(dbModel.UniqueIdentifier) == "AGUARDANDO" {
		dbModel.UniqueIdentifier = oldModel.UniqueIdentifier
	}

	newModel, err := instance.repository.Save(ctx, dbModel)

	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceCustomers) UpdateUniqueIdentifier(ctx context.Context, id, uniqueIdentifier string) (*MCustomers, error) {
	customerDb, err := instance.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	customerDb.UniqueIdentifier = uniqueIdentifier

	newModel, err := instance.repository.Save(ctx, customerDb)
	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance * ServiceCustomers) AddCardToCustomer(ctx context.Context, id string, card *customers.DTOCustomerCreditCard) (*MCustomers, error)  {
	customerDb, err := instance.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	newCard := NewCustomerCreditCardModel(card)

	customerDb.CreditCard = append(customerDb.CreditCard, newCard)

	newModel, err := instance.repository.Save(ctx, customerDb)
	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance * ServiceCustomers) RemoveCardFromCustomer(ctx context.Context, id string, idPaymentToken string ) (*MCustomers, error)  {
	return instance.repository.RemoveCardFromCustomer(ctx, id, idPaymentToken)
}

func (instance *ServiceCustomers) Remove(ctx context.Context, id string) (*MCustomers, error) {
	oldModel, err := instance.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	if oldModel == nil {
		return nil, errors.New( fmt.Sprintf("Nenhum registro encontrado para o id informado: Id: %v", id))
	}

	return instance.repository.Remove(ctx, oldModel)
}

func (instance *ServiceCustomers) GetById(ctx context.Context, id string) (*MCustomers, error) {
	res, err := instance.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (instance *ServiceCustomers) GetByOwnId(ctx context.Context, id string) (*MCustomers, error) {
	res, err := instance.repository.GetByOwnId(ctx, id)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (instance *ServiceCustomers) GetAll(ctx context.Context, start, limit int) ([]*MCustomers, error) {
	res, err:= instance.repository.GetAll(ctx, start, limit)

	if err != nil {
		return nil, err
	}

	return res, nil
}
