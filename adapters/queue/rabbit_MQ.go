package queue

import (
	"context"
	"encoding/json"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"log"
	"payment_orders/utils/environment"
	"strconv"
	"time"
)

func (instance *RabbitMQInstance) failOnError(err error, msg string) {
	if err != nil {
		log.Printf("%s: %s", msg, err)
	}
}

type ContentMessageInstance struct {
	Body          []byte
	DeliveryLimit uint64
	Message       amqp.Delivery
}

type RabbitMQInstance struct {
	Connection    IRabbitConnection
	Channel       *amqp.Channel
	Config        RabbitMQConfig
	instance      string
	verbose       bool
	FailOnQueue   chan FailOnQueue
	Running       chan int
	ChannelClosed chan *amqp.Error
	NewMessage    chan *ContentMessageInstance// chan []byte// chan amqp.Delivery
}

func MakeNewRabbitMQConsumer(rabbitConn IRabbitConnection, config RabbitMQConfig) *RabbitMQInstance{
	if config.ConsumerCount <= 0 {
		config.ConsumerCount = 1
	}

	verb, _ := strconv.ParseBool(environment.GetVERBOSE_AMQP())
	return &RabbitMQInstance{
		Connection:    rabbitConn,
		Channel:       nil,
		instance:      config.Configuration,
		Config:        config,
		Running:       nil,
		ChannelClosed: nil,
		verbose:       verb,
		FailOnQueue:   make(chan FailOnQueue),
	}
}

func MakeNewRabbitMQProducer(rabbitConn IRabbitConnection, config RabbitMQConfig) *RabbitMQInstance{
	if config.ConsumerCount <= 0 {
		config.ConsumerCount = 1
	}
	return &RabbitMQInstance{
		Connection: rabbitConn,
		Channel: nil,
		Config: config,
		Running: nil,
		ChannelClosed: nil,
		FailOnQueue: make(chan FailOnQueue),
	}
}

func (instance *RabbitMQInstance) Close()  {
	if instance.Connection != nil {
		_ = instance.Connection.CloseConnection()
	}
	if instance.Channel != nil {
		_ = instance.Channel.Close()
	}
}

func (instance *RabbitMQInstance) createExchange(exchangeName, exchangeType string, arguments amqp.Table) error {
	err := instance.Channel.ExchangeDeclare(
		exchangeName, // name of the exchange
		exchangeType, // type direct|fanout|topic|x-custom
		true,                 // durable
		false,             // delete when unused
		false,                // internal
		false,                // noWait
		arguments,                    // arguments
	)
	instance.failOnError(err, "Não foi possivel declarar a Exchange")
	return err
}

func (instance *RabbitMQInstance) bindQueueToExchange(queueName, routingKey, exchangeName string) error {
	err := instance.Channel.QueueBind(
		queueName,
		routingKey,
		exchangeName,
		false,
		nil,
	)
	instance.failOnError(err, "Não foi possível unir a fila à exchange")
	return err
}

func (instance *RabbitMQInstance) createQueue(queueName string, arguments amqp.Table) (amqp.Queue, error){
	return instance.Channel.QueueDeclare(
		queueName,       // name
		instance.Config.QueueDurable,    // durable
		instance.Config.QueueAutoDelete, // delete when unused
		instance.Config.QueueExclusive,  // exclusive
		instance.Config.QueueNoWait,     // no-wait
		arguments,                       // arguments
	)
}

func (instance *RabbitMQInstance) configureQos(prefetchCount, prefetchSize int) error {
	return instance.Channel.Qos(
		prefetchCount, prefetchSize, false)
}

func (instance *RabbitMQInstance) createConsumer(tag string, arguments amqp.Table) (<-chan amqp.Delivery, error)  {
	return instance.Channel.Consume(
		instance.Config.QueueName,        // queue
		tag,                              // consumer
		instance.Config.ChannelAutoAck,   // auto-ack
		instance.Config.ChannelExclusive, // exclusive
		instance.Config.ChannelNoLocal,   // no-local
		instance.Config.ChannelNoWait,    // no-wait
		arguments,                         // args
	)
}

func (instance *RabbitMQInstance) Producer(createDeadLetter bool) error  {
	var err error
	// amqpuri = environment.GetAMPQ_URI()

	exchangeDeadLetterName := ""
	queueDeadLetterName := ""
	routingKeyDeadLetterName := ""

	if createDeadLetter {
		exchangeDeadLetterName = fmt.Sprintf("%s_dead", instance.Config.ExchangeName)
		routingKeyDeadLetterName = fmt.Sprintf("%s_dead", instance.Config.RoutingKey)
		queueDeadLetterName = fmt.Sprintf("%s_dead", instance.Config.QueueName)
	}

	// Conectando ao Rabbit
	err = instance.Connection.OpenConnection()
	if err != nil {
		return err
	}

	// Criando o Channel para o Rabbit
	err = instance.OpenChannel()
	if err != nil {
		return err
	}

	// Criando o Exchange da conexão
	err = instance.createExchange(
		instance.Config.ExchangeName,
		instance.Config.ExchangeType,
		nil)
	if err != nil {
		return err
	}

	// Criando a fila
	var queueArgs amqp.Table = nil
	if createDeadLetter {
		queueArgs = amqp.Table{
			// "x-message-ttl": 5000,
			"x-dead-letter-exchange": exchangeDeadLetterName,
			"x-dead-letter-routing-key": routingKeyDeadLetterName,
			"x-delivery-limit": 3,
		}
	}

	_, err = instance.createQueue(
		instance.Config.QueueName,
		queueArgs)
	instance.failOnError(err, "Não foi possível conectar à fila")
	if err != nil {
		return err
	}

	// Anexando a fila à exchange
	err = instance.bindQueueToExchange(
		instance.Config.QueueName,
		instance.Config.RoutingKey,
		instance.Config.ExchangeName)
	if err != nil {
		return err
	}

	// Checando se deve gerar a fila de deadletter
	if createDeadLetter {
		// criando a exchange da fila de deadletter
		err = instance.createExchange(
			exchangeDeadLetterName,
			"direct",
			nil)
		if err != nil {
			return err
		}

		// criando a fila de deadletter
		_, err = instance.createQueue(
			queueDeadLetterName,
			nil)
		if err != nil {
			return err
		}

		// Anexando a fila à exchange de deadletter
		err = instance.bindQueueToExchange(
			queueDeadLetterName,
			routingKeyDeadLetterName,
			exchangeDeadLetterName)
		if err != nil {
			return err
		}
	}

	return err
}

func (instance *RabbitMQInstance) PublishMessage(message []byte) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	err := instance.Channel.PublishWithContext(ctx, "",
		instance.Config.QueueName, false, false,
		amqp.Publishing{
			ContentType: "text/plain",
			Headers: amqp.Table{
				"x-delivery-limit": 3,
			},
			Body:message,
			DeliveryMode: instance.Config.MessageDeliveryMode,
		})
	instance.failOnError(err, "Não foi possível publicar a mensagem")

	if instance.verbose {
		log.Printf("Mensagem publicada")
	}
}

func (instance *RabbitMQInstance) Consumer(createDeadLetter bool) error {
	var err error
	exchangeDeadLetterName := ""
	queueDeadLetterName := ""
	routingKeyDeadLetterName := ""

	if createDeadLetter {
		exchangeDeadLetterName = fmt.Sprintf("%s_dead", instance.Config.ExchangeName)
		routingKeyDeadLetterName = fmt.Sprintf("%s_dead", instance.Config.RoutingKey)
		queueDeadLetterName = fmt.Sprintf("%s_dead", instance.Config.QueueName)
	}

	// Conectando ao Rabbit
	err = instance.Connection.OpenConnection()
	if err != nil {
		return err
	}

	// Criando o Channel para o Rabbit
	err = instance.OpenChannel()
	if err != nil {
		return err
	}

	// Criando o Exchange da conexão
	err = instance.createExchange(
		instance.Config.ExchangeName,
		instance.Config.ExchangeType,
		nil)
	if err != nil {
		return err
	}

	// Criando a fila
	var queueArgs amqp.Table = nil
	if createDeadLetter {
		queueArgs = amqp.Table{
			// "x-message-ttl": 5000,
			"x-dead-letter-exchange": exchangeDeadLetterName,
			"x-dead-letter-routing-key": routingKeyDeadLetterName,
			"x-delivery-limit": 3,
		}
	}

	_, err = instance.createQueue(
		instance.Config.QueueName,
		queueArgs)
	instance.failOnError(err, "Não foi possível conectar à fila")
	if err != nil {
		return err
	}

	// Anexando a fila à exchange
	err = instance.bindQueueToExchange(
		instance.Config.QueueName,
		instance.Config.RoutingKey,
		instance.Config.ExchangeName)

	if err != nil {
		return err
	}

	err = instance.configureQos(1, 0)
	if err != nil {
		return err
	}

	// Checando se deve gerar a fila de deadletter
	if createDeadLetter {
		// criando a exchange da fila de deadletter
		err = instance.createExchange(
			exchangeDeadLetterName,
			"direct",
			nil)

		if err != nil {
			return err
		}

		// criando a fila de deadletter
		_, err = instance.createQueue(
			queueDeadLetterName,
			nil)

		instance.failOnError(err, "Não foi possível criar a fila de deadletter")
		if err != nil {
			return err
		}

		// Anexando a fila à exchange de deadletter
		err = instance.bindQueueToExchange(
			queueDeadLetterName,
			routingKeyDeadLetterName,
			exchangeDeadLetterName)

		if err != nil {
			return err
		}

		err = instance.configureQos(1, 0)
		if err != nil {
			return err
		}
	}

	instance.NewMessage = make(chan *ContentMessageInstance)

	return err
}

/*
func (instance *RabbitMQInstance) OpenConnection() error {
	// Buscando os dados para conexão
	amqpuri = environment.GetAMPQ_URI()
	if instance.Connection == nil {
		var err error
		// Conectando ao Rabbit
		instance.Connection, err = amqp.Dial(amqpuri)
		instance.failOnError(err, "Não foi possível conectar ao RABBITMQ")
		if err != nil {
			return err
		}
	} else if instance.Connection.IsClosed() {
		// Conectando ao Rabbit
		newConnection, err := amqp.Dial(amqpuri)
		instance.failOnError(err, "Não foi possível conectar ao RABBITMQ")
		if err != nil {
			return err
		}
		instance.Connection = newConnection
	}

	return nil
}
*/

func (instance *RabbitMQInstance) OpenChannel() error {
	err := instance.Connection.OpenConnection()
	if err != nil {
		return err
	}

	conn := instance.Connection.GetConnection()
	if instance.Channel == nil {
		var err error
		instance.Channel, err = conn.Channel()
		instance.failOnError(err, "Não foi possível conectar ao channel do RABBITMQ")
		if err != nil {
			return err
		}
	} else if instance.Channel.IsClosed() {
		newChannel, err := conn.Channel()
		instance.failOnError(err, "Não foi possível conectar ao channel do RABBITMQ")
		if err != nil {
			return err
		}

		instance.Channel = newChannel
	}
	return nil
}

func (instance *RabbitMQInstance) Subscribe() {
	instance.Running = make(chan int)

	instance.ChannelClosed = instance.Channel.NotifyClose(make(chan *amqp.Error, 10))
	go func() {
		for err := range instance.ChannelClosed {
			if err != nil {
				instance.Running <- 1

				instance.failOnError(err, "A conexão com RABBIT foi encerrada")
				instance.FailOnQueue <- FailOnQueue{
					Error: err,
					Message: "A conexão com RABBIT foi encerrada.",
				}
			}
		}
	}()

	// Gerando os consumers para a fila
	for i := 0; i < instance.Config.ConsumerCount; i++ {
		tag := fmt.Sprintf("%s-%v", instance.Config.ConsumerTag, i + 1)
		deliveredMessage, err := instance.createConsumer(tag, nil)
		instance.failOnError(err, "Não foi possível registrar o consumidor")
		if err != nil {
			return
		}

		go func() {
			for message := range deliveredMessage {
				if instance.verbose {
					log.Printf("[#%s] Mensagem recebida", tag)
					log.Printf("%s", message.Body)
				}
				var messageParsed map[string]interface{}


				var count uint64 = 10

				if val, ok := message.Headers["x-delivery-limit"]; ok {
					u, _ := strconv.ParseUint(fmt.Sprintf("%v", val), 0, 64)

					count = (uint64)(u)
				}

				errParse := json.Unmarshal([]byte(message.Body), &messageParsed)
				if errParse != nil {
					log.Printf("Problema para converter a msg, err: %s", errParse)

					message.Nack(false, !(message.DeliveryTag >= count))
					continue
				}

				// message.Ack(false)
				instance.NewMessage <- &ContentMessageInstance{
					Body: message.Body,
					Message: message,
					DeliveryLimit: count,
				}
			}
		}()
	}
	log.Printf("[%s] Aguardando mensagens", instance.Config.ConsumerTag)
	<-instance.Running
}
