package orders

import "payment_orders/adapters/queue"

type AppOrdersCardQueue struct {
	NewMessage   chan *queue.ContentMessageInstance
}

func MakeNewAppOrdersCardQueue() *AppOrdersCardQueue {
	return &AppOrdersCardQueue{
		NewMessage: make(chan *queue.ContentMessageInstance),
	}
}

func (instance *AppOrdersCardQueue) ConnectToAMQP(consumerAmqpConn queue.IRabbitConnection) error {
	config := queue.MakeOrdersCardRabbitMQCfg()
	consumer := queue.MakeNewConsumerFactory(consumerAmqpConn, config)
	err := consumer.CreateConsumer(config.ConsumerTag, false)
	if err != nil {
		return err
	}

	go consumer.Subscribe()

	go func() {
		for message := range consumer.NewMessage {
			instance.NewMessage <- message
			message = nil
		}
	}()

	return nil
}
