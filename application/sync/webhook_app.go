package sync

import (
	"context"
	"encoding/json"
	"log"
	"payment_orders/adapters/queue"
	"payment_orders/application/triggers"
	"payment_orders/utils/environment"
	"payment_orders/utils/request"
)

type MWebHookEvent struct {
	Event string `json:"event"`
	Resource interface{} `json:"resource"`
	Error string `json:"error"`
}

type AppWebHook struct {
	consumerAmqpConn queue.IRabbitConnection
	publishAmqpConn queue.IRabbitConnection
}

func NewAppWebHook(
	consumerAmqpConn queue.IRabbitConnection,
	publishAmqpConn queue.IRabbitConnection) *AppWebHook {
	return &AppWebHook{
		consumerAmqpConn: consumerAmqpConn,
		publishAmqpConn: publishAmqpConn,
	}
}

func (instance *AppWebHook) ConnectToAMQP(ctx context.Context) error {
	config := queue.MakeWebHookRabbitMQCfg()
	consumer := queue.MakeNewConsumerFactory(instance.consumerAmqpConn, config)
	err := consumer.CreateConsumer(config.ConsumerTag, false)
	if err != nil {
		return err
	}

	go consumer.Subscribe()

	go func() {
		for message := range consumer.NewMessage {
			instance.newMessage(message)
		}
	}()
	return nil
}
func (instance *AppWebHook) newMessage(message *queue.ContentMessageInstance) {
	messageApp := MWebHookEvent{}

	errParse := json.Unmarshal(message.Body, &messageApp)
	if errParse != nil {
		log.Printf("Falha ao converter o JSON. err: %v", errParse)
		log.Printf("Body: %v ", string(message.Body))
		_ = message.Message.Nack(false, false)
		return
	}

	_ = message.Message.Ack(false)
	ctx := context.Background()
	event := messageApp.Event
	var err error
	if event == "CUSTOMER.REGISTER" ||
		event == "ORDER.REGISTER" ||
		event == "PAYMENT.AUTHORIZED" ||
		event == "ORDER.PENDING" ||
		event == "ORDER_CUSTOMER.REGISTER" ||
		event == "ORDER.CANCELED" ||
		event == "ORDER.REFUNDED" ||
		triggers.IsValidEvent(event) {
		go func() {
			err = instance.ProcessEvent(ctx, messageApp)
			if err != nil {
				log.Printf("Falha ao processar o webhook '%v' err: %v ", event, err)
				log.Printf("Webhook body: %v ", string(message.Body))
			}
		}()
	}
	ctx.Done()
}

func (instance *AppWebHook) ProcessEvent(ctx context.Context, message MWebHookEvent) error {
	data, err := json.Marshal(message)

	if err != nil {
		log.Printf("Falha ao gerar o objeto para Post err: %v ", err)
		return err
	}

	notifyUrl := environment.GetNOTIFY_URL()

	_, err = request.Post(ctx, notifyUrl, data, nil)

	return err
}



