package cancellations

import (
	"context"
	"encoding/json"
	"fmt"
	"errors"
	"log"
	"payment_orders/adapters/dto/cancellations"
	"payment_orders/adapters/dto/orders"
	"payment_orders/adapters/queue"
	"payment_orders/application/transactions"
	"payment_orders/utils"
)

type AppOrdersCancellation struct {
	publishAmqpConn queue.IRabbitConnection
	serviceOrdersCancellation IOrdersCancellationService
	serviceTransaction transactions.ITransactionsService
}

func NewAppOrdersCancellation(
	publishAmqpConn queue.IRabbitConnection,
	serviceOrdersCancellation IOrdersCancellationService,
	serviceTransaction transactions.ITransactionsService) *AppOrdersCancellation {
	return &AppOrdersCancellation{
		publishAmqpConn: publishAmqpConn,
		serviceOrdersCancellation: serviceOrdersCancellation,
		serviceTransaction: serviceTransaction,
	}
}

func (instance *AppOrdersCancellation) publishMessage(ctx context.Context, message string) error {
	configProducer := queue.MakeSyncRabbitMQCfg()
	producer := queue.MakeNewProducerFactory(instance.publishAmqpConn, configProducer)

	err := producer.CreateProducer(false)

	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}
	producer.PublishStringMessage(message)

	_ = instance.publishAmqpConn.CloseConnection()

	return nil
}

func (instance *AppOrdersCancellation) publishMessageToWebHook(ctx context.Context, message string) error {
	configProducer := queue.MakeWebHookRabbitMQCfg()
	producer := queue.MakeNewProducerFactory(instance.publishAmqpConn, configProducer)
	err := producer.CreateProducer(false)

	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}
	producer.PublishStringMessage(message)
	_ = instance.publishAmqpConn.CloseConnection()

	return nil
}

func (instance *AppOrdersCancellation) sendToWebHook(ctx context.Context, event, message string, err error) {
	errText := ""
	if err != nil {
		errText = utils.ScapeChars(err.Error())
	}
	s := fmt.Sprintf(`{"event": "%v", "resource": %v, "error": "%v" }`, event, message, errText)

	_ =  instance.publishMessageToWebHook(ctx, s)
}

func (instance *AppOrdersCancellation) mustSendToWebHook(ctx context.Context,
	message *queue.ContentMessageInstance,
	dtoOrderToCancel *cancellations.DTOOrdersCancellation,
	err error) {
	if message.Message.DeliveryTag >= message.DeliveryLimit {
		log.Printf("Webhook - Order Cancellation %s - UniqueIdentifier %s", dtoOrderToCancel.ID.Hex(), dtoOrderToCancel.UniqueIdentifier)
		jsonHook, errFind := json.Marshal(dtoOrderToCancel)
		if errFind == nil {
			instance.sendToWebHook(ctx, "ORDER.CANCEL", string(jsonHook), err)
		}
	}
}

func (instance *AppOrdersCancellation) NewMessage(message *queue.ContentMessageInstance) {
	orderToCancelApp := orders.DTOOrderCustomerCancellation{}

	errParse := json.Unmarshal(message.Body, &orderToCancelApp)
	if errParse != nil {
		log.Printf("Falha ao converter o JSON. err: %v", errParse)
		log.Printf("Body: %v ", string(message.Body))
		_ = message.Message.Nack(false, false)
		return
	}

	operation := orderToCancelApp.Operation
	transactionId := orderToCancelApp.Transaction
	log.Printf("Processando transação: %v", transactionId)

	ctx := context.Background()
	_, _ = instance.serviceTransaction.StartTransaction(ctx, transactionId, operation)

	dtoCancellation := &orderToCancelApp.Body.Cancellation

	dbCancellation, err := instance.processOrderCancellation(ctx, orderToCancelApp.Operation, dtoCancellation)

	if err != nil {
		msgErr := fmt.Sprintf("Não foi possível processar a transação %s. err: %v", orderToCancelApp.Transaction, err)

		log.Printf(msgErr)
		_, _ = instance.serviceTransaction.AddMessageError(ctx, transactionId, operation, msgErr)

		instance.mustSendToWebHook(ctx, message, dtoCancellation, errors.New(msgErr))
		_ = message.Message.Nack(false, !(message.Message.DeliveryTag >= message.DeliveryLimit))
		ctx.Done()
		return
	}
	_, _ = instance.serviceTransaction.UpdateCancellationId(ctx, transactionId, operation, dbCancellation.GetModel().ID.Hex())

	_, _ = instance.serviceTransaction.UpdateToCreated(ctx, transactionId, operation)

	_ = message.Message.Ack(false)

	s, _ := json.Marshal(orderToCancelApp)
	_ = instance.publishMessage(ctx, string(s))
	ctx.Done()
}

func (instance *AppOrdersCancellation) processOrderCancellation(ctx context.Context, operation string, dtoOrderToCancel *cancellations.DTOOrdersCancellation) (IOrdersCancellation, error) {
	var dbCancellation IOrdersCancellation = nil
	var err error

	if operation == queue.CREATE ||
		operation == queue.CHANGE {
		dbCancellation, err = instance.serviceOrdersCancellation.Create(ctx, dtoOrderToCancel)
	}

	return dbCancellation, err
}

