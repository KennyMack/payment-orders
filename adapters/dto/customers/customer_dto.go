package customers

import (
	"payment_orders/adapters/db"
)

type DTOCustomerAddress struct {
	Street string `json:"street" valid:"required~Rua é obrigatório"`
	Complement string `json:"complement" valid:"complement,optional"`
	Neighborhood string `json:"neighborhood" valid:"required~Endereço é obrigatório"`
	City string `json:"city" valid:"required~Cidade é obrigatório"`
	State string `json:"state" valid:"required~Estado é obrigatório"`
	ZipCode string `json:"zipCode" valid:"required~Cep é obrigatório"`
	Number string `json:"number" valid:"required~Número do endereço é obrigatório"`
	PhoneNumber string `json:"phoneNumber" valid:"required~Telefone é obrigatório"`
}

type DTOCustomerCreditCard struct {
	Brand string `json:"brand" valid:"required~Bandeira do cartão é obrigatória"`
	First6 string `json:"first6" valid:"required~Primeiros digitos do cartão é obrigatório"`
	Last4 string `json:"last4" valid:"required~Ultimos digitos do cartão é obrigatório"`
	HolderName string `json:"holderName" valid:"required~Nome do dono do cartão é obrigatório"`
	Year string `json:"year" valid:"required~Ano de expiração é obrigatório"`
	Month string `json:"month" valid:"required~Mês de expiração é obrigatório"`
	Test bool `json:"test" valid:"required~Tipo do cartão é obrigatório"`
	DisplayNumber string `json:"displayNumber" valid:"required~Numero no cartão é obrigatório"`
	SetAsDefault bool `json:"setAsDefault" valid:"required"`
	Description string `json:"description" valid:"required~Descrição do cartão é obrigatório"`
	IdPaymentToken string `json:"idPaymentToken" valid:"required~Token de pagamento é obrigatório"`
}

type DTOCustomers struct {
	db.DefaultModel `bson:",inline"`
	Email string `json:"email" valid:"required~E-mail é obrigatório"`
	UniqueIdentifier string `json:"uniqueIdentifier" valid:"required~Identificador unico é obrigatório"`
	FullName string `json:"fullName" valid:"required~Nome é obrigatório"`
	TypeDoc string `json:"typeDoc" valid:"required~Tipo do documento é obrigatório"`
	NumberDoc string `json:"numberDoc" valid:"required~Número do documento é obrigatório"`
	CardToken string `json:"cardToken" valid:"optional"`
	RemoveCardToken string `json:"removeCardToken" valid:"optional"`
	CreditCard []*DTOCustomerCreditCard `json:"creditCard" valid:"optional"`
	Address *DTOCustomerAddress `json:"address" valid:"optional"`
	OwnId string `json:"ownId" valid:"required~Identificador de origem é obrigatório"`
}

func NewDTOCustomer() *DTOCustomers {
	customer := DTOCustomers{
		UniqueIdentifier: "AGUARDANDO",
		TypeDoc: TYPEDOCCPF,
		CreditCard: []*DTOCustomerCreditCard{},
		Address: NewDTOCustomerAddress(),
	}
	return &customer
}

func NewDTOCustomerCreditCard() *DTOCustomerCreditCard {
	creditCard := DTOCustomerCreditCard{
		Test: true,
		SetAsDefault: true,
	}
	return &creditCard
}

func NewDTOCustomerAddress() *DTOCustomerAddress {
	return &DTOCustomerAddress{}
}

const (
	TYPEDOCCPF = "CPF"
	TYPEDOCCNPJ = "CNPJ"
)

