package cancellations

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"payment_orders/adapters/dto/cancellations"
)

type ServiceOrdersCancellation struct {
	repository IOrdersCancellationRepository
}

func NewServiceOrdersCancellation(repository IOrdersCancellationRepository, ) *ServiceOrdersCancellation {
	return &ServiceOrdersCancellation{
		repository: repository,
	}
}

func (instance *ServiceOrdersCancellation) OpenConnection() error {
	return instance.repository.OpenConnection()
}

func (instance *ServiceOrdersCancellation) CloseConnection() error {
	return instance.repository.CloseConnection()
}

func (instance *ServiceOrdersCancellation) Create(ctx context.Context, model *cancellations.DTOOrdersCancellation) (*MOrdersCancellation, error) {
	dbModel := NewOrdersCancellationModel(model)

	_, err := dbModel.IsValid()

	if err != nil {
		return nil, err
	}

	if !dbModel.ID.IsZero() {
		return nil, errors.New("Id deve ser nulo para novos cancelamentos")
	}

	newModel, err := instance.repository.Save(ctx, dbModel)

	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceOrdersCancellation) Change(ctx context.Context, id string, model *cancellations.DTOOrdersCancellation) (*MOrdersCancellation, error) {
	dbModel := NewOrdersCancellationModel(model)

	_, err := dbModel.IsValid()

	if err != nil {
		return nil, err
	}

	if dbModel.ID.IsZero() {
		return nil, errors.New("Id deve ser informado")
	}

	oldModel, err := instance.repository.GetById(ctx, dbModel.ID.Hex())

	if oldModel == nil {
		return nil, errors.New( fmt.Sprintf("Nenhum registro encontrado para o id informado: Id: %v", dbModel.ID))
	}
	dbModel.CreatedAt = oldModel.CreatedAt

	newModel, err := instance.repository.Save(ctx, dbModel)

	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceOrdersCancellation) Remove(ctx context.Context, id string) (*MOrdersCancellation, error) {
	oldModel, err := instance.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	if oldModel == nil {
		return nil, errors.New( fmt.Sprintf("Nenhum registro encontrado para o id informado: Id: %v", id))
	}

	return instance.repository.Remove(ctx, oldModel)
}

func (instance *ServiceOrdersCancellation) GetById(ctx context.Context, id string) (*MOrdersCancellation, error) {
	res, err := instance.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (instance *ServiceOrdersCancellation) GetByUniqueIdentifier(ctx context.Context, uniqueIdentifier string, processed bool) (*MOrdersCancellation, error) {
	res, err := instance.repository.GetByUniqueIdentifier(ctx, uniqueIdentifier, processed)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (instance *ServiceOrdersCancellation) UpdateToProcessed(ctx context.Context, id string) (*MOrdersCancellation, error){
	card, err := instance.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	card.Processed = true

	card, err = instance.repository.Save(ctx, card)

	if err != nil {
		return nil, err
	}

	return card, nil
}

func (instance *ServiceOrdersCancellation) UpdateErrorMessage(ctx context.Context, id string, errorMessage string) (*MOrdersCancellation, error) {
	cancellationDb, err := instance.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	cancellationDb.Error = errorMessage

	cancellationDb, err = instance.repository.Save(ctx, cancellationDb)

	if err != nil {
		return nil, err
	}

	return cancellationDb, nil
}

func (instance *ServiceOrdersCancellation) UpdateStatus(ctx context.Context, id string, currentStatus string, finalStatus string) (*MOrdersCancellation, error) {
	cancellationDb, err := instance.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	cancellationDb.CurrentStatus = currentStatus
	cancellationDb.FinalStatus = finalStatus

	cancellationDb, err = instance.repository.Save(ctx, cancellationDb)

	if err != nil {
		return nil, err
	}

	return cancellationDb, nil
}

func (instance *ServiceOrdersCancellation) UpdateToCanceled(ctx context.Context, id string) (*MOrdersCancellation, error) {
	cancellationDb, err := instance.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	cancellationDb.Canceled = true

	cancellationDb, err = instance.repository.Save(ctx, cancellationDb)

	if err != nil {
		return nil, err
	}

	return cancellationDb, nil
}
func (instance *ServiceOrdersCancellation) UpdateToRefunded(ctx context.Context, id string) (*MOrdersCancellation, error) {
	cancellationDb, err := instance.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	cancellationDb.Refunded = true

	cancellationDb, err = instance.repository.Save(ctx, cancellationDb)

	if err != nil {
		return nil, err
	}

	return cancellationDb, nil
}
