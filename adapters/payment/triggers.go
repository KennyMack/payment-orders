package payment

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"payment_orders/application/triggers"
	"strings"
)

func ParseTriggerBody(values url.Values) string {
	obj := make([]string, 0, 0)
	for key, value := range values {
		keyParse := key
		valueParse := ""
		if len(value) > 0 {
			valueParse = value[0]
		}
		if key[:5] == "data[" {
			keyParse = key[5: len(key) - 1]
		}

		keyParse = strings.ReplaceAll(keyParse, "][", "_")

		obj = append(obj, fmt.Sprintf("\"%v\":\"%v\"", keyParse, valueParse))
	}
	return fmt.Sprintf("{ %v }",strings.Join(obj, ","))
}

func ConvertToObject (jsonStr string) (*triggers.MTriggerData, error) {
	data := triggers.MTriggerData{}

	err := json.Unmarshal([]byte(jsonStr), &data)

	if err != nil {
		return nil, err
	}
	return &data, nil
}

func DecodeJsonToObject (r *http.Request)  (*triggers.MTriggerData, error) {
	decoder := json.NewDecoder(r.Body)
    var t triggers.MTriggerData
    err := decoder.Decode(&t)
	if err != nil {
		return nil, err
	}

    return &t, nil
}

