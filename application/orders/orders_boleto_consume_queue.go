package orders

import "payment_orders/adapters/queue"

type AppOrdersBoletoQueue struct {
	NewMessage   chan *queue.ContentMessageInstance
}

func MakeNewAppOrdersBoletoQueue() *AppOrdersBoletoQueue {
	return &AppOrdersBoletoQueue{
		NewMessage: make(chan *queue.ContentMessageInstance),
	}
}

func (instance *AppOrdersBoletoQueue) ConnectToAMQP(consumerAmqpConn queue.IRabbitConnection) error {
	config := queue.MakeOrdersBoletoRabbitMQCfg()
	consumer := queue.MakeNewConsumerFactory(consumerAmqpConn, config)
	err := consumer.CreateConsumer(config.ConsumerTag, false)
	if err != nil {
		return err
	}

	go consumer.Subscribe()

	go func() {
		for message := range consumer.NewMessage {
			instance.NewMessage <- message
			message = nil
		}
	}()

	return nil
}
