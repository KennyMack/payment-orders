package triggers

import (
	"context"
	"errors"
	"fmt"
	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson"
	"payment_orders/adapters/db"
	"payment_orders/application/triggers"
)

type RepositoryTriggers struct{
	database *db.MongoDBInstance
	collectionName string
}

func (instance *RepositoryTriggers) OpenConnection() error {
	return instance.database.Open()
}

func (instance *RepositoryTriggers) CloseConnection() error {
	return instance.database.Close()
}

func NewRepositoryTriggers(database *db.MongoDBInstance) *RepositoryTriggers {
	return &RepositoryTriggers{
		database: database,
		collectionName: "Triggers",
	}
}

func (instance *RepositoryTriggers) GetByControlId(ctx context.Context, id string) (*triggers.MTriggerData, error) {
	result := &triggers.MTriggerData{}

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	cur := collection.FindOne(ctx, bson.M{ "controlId": id})

	err := cur.Decode(&result)

	collection = nil

	if err != nil {
		return nil, err
	}

	if result.ID.IsZero() {
		return nil, errors.New(fmt.Sprintf("Nenhum registro encontrado para o id: %v", id))
	}

	return result, nil
}

func (instance *RepositoryTriggers) Save(ctx context.Context, trigger triggers.ITriggerData) (*triggers.MTriggerData, error) {
	if instance.database.DbType == db.MongoDB {
		collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

		newModel := trigger.GetModel()

		var err error
		if newModel.ID.IsZero() {
			err = collection.CreateWithCtx(ctx, newModel)
		} else {
			err = collection.UpdateWithCtx(ctx, newModel)
		}
		collection = nil

		if err != nil {
			return nil, err
		}

		return newModel.GetModel(), nil
	}

	return nil, nil
}
