package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"payment_orders/adapters/db"
	"payment_orders/adapters/queue"
	repoCustomer "payment_orders/adapters/repository/customers"
	repoorder "payment_orders/adapters/repository/orders"
	repocard "payment_orders/adapters/repository/payments"
	repot "payment_orders/adapters/repository/transactions"
	repocancel "payment_orders/adapters/repository/cancellations"
	"payment_orders/adapters/web/server"
	"payment_orders/application/cancellations"
	"payment_orders/application/customers"
	"payment_orders/application/orders"
	servcard "payment_orders/application/payments"
	syncApp "payment_orders/application/sync"
	"payment_orders/application/transactions"
	"payment_orders/utils/environment"
	"syscall"
	"time"
)

func createServer(port string) {
	web.MakeNewWebServer().Serve(port)
}

func openConnection(conn db.IMongoDBInstance, message *queue.ContentMessageInstance) bool {
	errConn := conn.Open()
	if errConn != nil {
		_ = message.Message.Nack(false, !(message.Message.DeliveryTag >= message.DeliveryLimit))
		msgErr := fmt.Sprintf("Não foi possível processar a transação. err: %v", errConn)

		log.Printf(msgErr)
		return false
	}
	return true
}


func openConnInit(conn db.IMongoDBInstance) bool {
	errConn := conn.Open()
	if errConn != nil {
		msgErr := fmt.Sprintf("Não foi possível processar a transação. err: %v", errConn)

		log.Printf(msgErr)
		return false
	}
	return true
}

func closeConnection(conn db.IMongoDBInstance) {
	/*errConn := conn.Close()
	if errConn != nil {
		msgErr := fmt.Sprintf("Não foi possível processar a transação. err: %v", errConn)

		log.Printf(msgErr)
	}*/
}

func customerConsumerAmqp(instanceDb db.IMongoDBInstance, consumerConn, publisherConn queue.IRabbitConnection) {
	amqp := customers.MakeNewAppCustomersQueue()

	err := amqp.ConnectToAMQP(consumerConn)

	if err != nil {
		log.Printf("Erro: %v", err)
		return
	}
	go func() {
		for message := range amqp.NewMessage {
			if !instanceDb.IsConnected() {
				if !openConnection(instanceDb, message) {
					return
				}
			}

			repository := repoCustomer.NewRepositoryCustomers(instanceDb)
			repositoryCardClient := repocard.NewRepositoryCardClientToken(instanceDb)
			repositoryTransaction := repot.NewRepositoryTransactions(instanceDb)

			serviceCardClient := servcard.NewServiceCardClientToken(repositoryCardClient)
			service := customers.NewServiceCustomers(repository)
			serviceTransaction := transactions.NewServiceTransactions(repositoryTransaction)

			appCustomers := customers.NewAppCustomers(
				publisherConn,
				service,
				serviceCardClient,
				serviceTransaction)

			appCustomers.NewMessage(message)

			// closeConnection(instanceDb)
		}
	}()

}

func orderBoletoConsumerAmqp(instanceDb db.IMongoDBInstance, consumerConn, publisherConn queue.IRabbitConnection) {
	amqp := orders.MakeNewAppOrdersBoletoQueue()

	err := amqp.ConnectToAMQP(consumerConn)

	if err != nil {
		log.Printf("Erro: %v", err)
		return
	}
	go func() {
		for message := range amqp.NewMessage {
			if !instanceDb.IsConnected() {
				if !openConnection(instanceDb, message) {
					return
				}
			}

			repository := repoorder.NewRepositoryOrders(instanceDb)
			repositoryTransaction := repot.NewRepositoryTransactions(instanceDb)

			service := orders.NewServiceOrders(repository)
			serviceTransaction := transactions.NewServiceTransactions(repositoryTransaction)

			app := orders.NewAppOrderBoleto(
				publisherConn,
				service,
				serviceTransaction)

			app.NewMessage(message)

			// closeConnection(instanceDb)
		}
	}()
}

func orderCardConsumerAmqp(instanceDb db.IMongoDBInstance, consumerConn, publisherConn queue.IRabbitConnection) {
	amqp := orders.MakeNewAppOrdersCardQueue()

	err := amqp.ConnectToAMQP(consumerConn)

	if err != nil {
		log.Printf("Erro: %v", err)
		return
	}
	go func() {
		for message := range amqp.NewMessage {
			if !instanceDb.IsConnected() {
				if !openConnection(instanceDb, message) {
					return
				}
			}

			repository := repoorder.NewRepositoryOrders(instanceDb)
			repositoryTransaction := repot.NewRepositoryTransactions(instanceDb)

			service := orders.NewServiceOrders(repository)
			serviceTransaction := transactions.NewServiceTransactions(repositoryTransaction)

			app := orders.NewAppOrderCard(
				publisherConn,
				service,
				serviceTransaction)

			app.NewMessage(message)

			// closeConnection(instanceDb)
		}
	}()
}

func orderPixConsumerAmqp(instanceDb db.IMongoDBInstance, consumerConn, publisherConn queue.IRabbitConnection) {
	amqp := orders.MakeNewAppOrdersPixQueue()

	err := amqp.ConnectToAMQP(consumerConn)

	if err != nil {
		log.Printf("Erro: %v", err)
		return
	}
	go func() {
		for message := range amqp.NewMessage {
			if !instanceDb.IsConnected() {
				if !openConnection(instanceDb, message) {
					return
				}
			}

			repository := repoorder.NewRepositoryOrders(instanceDb)
			repositoryTransaction := repot.NewRepositoryTransactions(instanceDb)

			service := orders.NewServiceOrders(repository)
			serviceTransaction := transactions.NewServiceTransactions(repositoryTransaction)

			app := orders.NewAppOrderPix(
				publisherConn,
				service,
				serviceTransaction)

			app.NewMessage(message)

			// closeConnection(instanceDb)
		}
	}()
}

func orderCancellationAmqp(instanceDb db.IMongoDBInstance, consumerConn, publisherConn queue.IRabbitConnection) {
	amqp := cancellations.MakeNewAppOrdersCancellationQueue()

	err := amqp.ConnectToAMQP(consumerConn)

	if err != nil {
		log.Printf("Erro: %v", err)
		return
	}
	go func() {
		for message := range amqp.NewMessage {
			if !instanceDb.IsConnected() {
				if !openConnection(instanceDb, message) {
					return
				}
			}

			repository := repocancel.NewRepositoryOrdersCancellation(instanceDb)
			repositoryTransaction := repot.NewRepositoryTransactions(instanceDb)

			service := cancellations.NewServiceOrdersCancellation(repository)
			serviceTransaction := transactions.NewServiceTransactions(repositoryTransaction)

			app := cancellations.NewAppOrdersCancellation(
				publisherConn,
				service,
				serviceTransaction)

			app.NewMessage(message)

			// closeConnection(instanceDb)
		}
	}()
}

func syncConsumerAmqp(instanceDb db.IMongoDBInstance, consumerConn, publisherConn queue.IRabbitConnection) {
	amqp := syncApp.MakeNewAppSyncQueue()

	err := amqp.ConnectToAMQP(consumerConn)

	if err != nil {
		log.Printf("Erro: %v", err)
		return
	}
	go func() {
		for message := range amqp.NewMessage {
			if !instanceDb.IsConnected() {
				if !openConnection(instanceDb, message) {
					return
				}
			}

			repository := repoCustomer.NewRepositoryCustomers(instanceDb)
			repositoryCardClient := repocard.NewRepositoryCardClientToken(instanceDb)
			repositoryTransaction := repot.NewRepositoryTransactions(instanceDb)
			repositoryOrder := repoorder.NewRepositoryOrders(instanceDb)
			repositoryOrderRetCode := repoorder.NewRepositoryPurchaseReturnCode(instanceDb)
			repositoryOrderCancellation := repocancel.NewRepositoryOrdersCancellation(instanceDb)

			serviceCustomer := customers.NewServiceCustomers(repository)
			serviceCardClient := servcard.NewServiceCardClientToken(repositoryCardClient)
			serviceTransaction := transactions.NewServiceTransactions(repositoryTransaction)
			serviceOrder := orders.NewServiceOrders(repositoryOrder)
			serviceOrderRetCode := orders.NewServicePurchaseReturnCode(repositoryOrderRetCode)
			serviceOrderCancellation := cancellations.NewServiceOrdersCancellation(repositoryOrderCancellation)

			app := syncApp.NewAppSync(
				publisherConn,
				serviceCustomer,
				serviceOrder,
				serviceOrderRetCode,
				serviceCardClient,
				serviceOrderCancellation,
				serviceTransaction)

			app.NewMessage(message)

			// closeConnection(instanceDb)
		}
	}()
}

func webHookAMQP(consumerConn, publisherConn queue.IRabbitConnection) {
	ctx := context.Background()
	webHookApp := syncApp.NewAppWebHook(
		consumerConn,
		publisherConn)

	err := webHookApp.ConnectToAMQP(ctx)

	go func() {
		for {
			log.Print("Solicitando pedidos pendentes")
			ctx := context.Background()
			message := syncApp.MWebHookEvent{
				Event: "ORDER.PENDING",
			}
			_ = webHookApp.ProcessEvent(ctx, message)
			time.Sleep(30 * time.Minute)
		}
	}()

	if err != nil {
		log.Printf("Erro: %v", err)
		return
	}
}

func appOrderAMQP(consumerConn, publisherConn queue.IRabbitConnection) {
	ctx := context.Background()
	webHookApp := orders.NewAppOrders(consumerConn, publisherConn)

	err := webHookApp.ConnectToAMQP(ctx)

	if err != nil {
		log.Printf("Erro: %v", err)
		return
	}
}


func main() {
	var port string
	envRunning := os.Getenv("ENV_RUNNING")
	if envRunning != "HEROKU" {
		err := environment.LoadEnvironment()
		if err != nil {
			log.Fatalf("Não foi possível carregar as variáveis de ambiente. Err: %s", err)
		}
		port = environment.GetSERVER_PORT()
	} else {
		port = os.Getenv("PORT")
	}

	log.Print(port)

	log.Printf("Estamos no ar, %v", time.Now())

	consumerConn := queue.MakeNewRabbitConnection()
	publisherConn := queue.MakeNewRabbitConnection()
	err := consumerConn.OpenConnection()
	if err != nil {
		log.Fatalf("Não foi possível conectar ao Rabbit.MQ. Err: %s", err)
	}
	err = publisherConn.OpenConnection()
	if err != nil {
		log.Fatalf("Não foi possível conectar ao Rabbit.MQ. Err: %s", err)
	}

	go createServer(port)
	dbCustomer := db.NewMongoDB()
	dbOrderBoleto := db.NewMongoDB()
	dbOrderCard := db.NewMongoDB()
	dbOrderPix := db.NewMongoDB()
	dbSync := db.NewMongoDB()
	dbOrderCancellation := db.NewMongoDB()
	if !openConnInit(dbCustomer) ||
		!openConnInit(dbOrderBoleto) ||
		!openConnInit(dbOrderCard) ||
		!openConnInit(dbOrderPix) ||
		!openConnInit(dbOrderCancellation) ||
		!openConnInit(dbSync) {
		return
	}

	go webHookAMQP(consumerConn, publisherConn)
	go appOrderAMQP(consumerConn, publisherConn)
	go customerConsumerAmqp(dbCustomer, consumerConn, publisherConn)
	go orderBoletoConsumerAmqp(dbOrderBoleto, consumerConn, publisherConn)
	go orderCardConsumerAmqp(dbOrderCard, consumerConn, publisherConn)
	go orderPixConsumerAmqp(dbOrderPix, consumerConn, publisherConn)
	go orderCancellationAmqp(dbOrderCancellation, consumerConn, publisherConn)

	go syncConsumerAmqp(dbSync, consumerConn, publisherConn)

	quitChannel := make(chan os.Signal, 1)
	signal.Notify(quitChannel, syscall.SIGINT, syscall.SIGTERM)
	<-quitChannel
	closeConnection(dbCustomer)
	closeConnection(dbOrderBoleto)
	closeConnection(dbOrderCard)
	closeConnection(dbOrderPix)
	closeConnection(dbSync)
	closeConnection(dbOrderCancellation)

	log.Println("Estamos encerrando nossas transmissões!")

	/*
	serveropt := options.ServerAPI(options.ServerAPIVersion1)
	clientOptions := options.Client().
		ApplyURI(environment.GetMONGODB_URI()).
		SetServerAPIOptions(serveropt)

	err := mgm.SetDefaultConfig(nil, environment.GetMONGODB_DATABASE(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}
	_, mongoClient1, db1, _ := mgm.DefaultConfigs()

	log.Printf("mongoClient1: %v", &mongoClient1)

	err = mgm.SetDefaultConfig(nil, environment.GetMONGODB_DATABASE(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}
	_, mongoClient2, db2, _ := mgm.DefaultConfigs()

	log.Printf("mongoClient2: %v", &mongoClient2)

	log.Printf("buscando db1: %v", &mongoClient1)
	collection := mgm.NewCollection(db1, "Customers")

	_, _ = collection.Find(mgm.Ctx(), bson.D{{}})

	time.Sleep(10 * time.Second)

	log.Printf("Desconectando bd da instance: %v", &mongoClient1)
	err = mongoClient1.Disconnect(mgm.Ctx())
	if err != nil {
		log.Fatal(err)
	}


	log.Printf("buscando db2: %v", &mongoClient2)
	collection2 := mgm.NewCollection(db2, "Customers")

	_, _ = collection2.Find(mgm.Ctx(), bson.D{{}})

	time.Sleep(10 * time.Second)

	log.Printf("Desconectando bd da instance: %v", &mongoClient2)
	err = mongoClient2.Disconnect(mgm.Ctx())
	if err != nil {
		log.Fatal(err)
	}

	quitChannel := make(chan os.Signal, 1)
	signal.Notify(quitChannel, syscall.SIGINT, syscall.SIGTERM)
	<-quitChannel

	log.Println("Estamos encerrando nossas transmissões!")
	*/
}
