package payment

import (
	"context"
	"encoding/json"
	"fmt"
	"payment_orders/utils/environment"
	"payment_orders/utils/request"
)

const (
	createSignatureUrl       = "%s/subscriptions/"
	activateSignatureUrl     = "%s/subscriptions/%s/activate?"
	suspendSignatureUrl      = "%s/subscriptions/%s/suspend?"
	changeSignatureUrl       = "%s/subscriptions/%s?"
	changePlanOfSignatureUrl = "%s/subscriptions/%s/change_plan/%s/?"
	removeSignatureUrl       = "%s/subscriptions/%s?"
	getSignatureByIdUrl      = "%s/subscriptions/%s?"
	getAllSignaturesUrl      = "%s/subscriptions/?%s&%s"
)

type IUGUSignature struct{
	baseIUGUURL string
}

func NewIUGUSignature() *IUGUSignature {
	return &IUGUSignature{
		baseIUGUURL: environment.GetIUGU_BASE_URL(),
	}
}

func (instance *IUGUSignature) CreateSignature(context context.Context, signature IuguSignatureBody) (interface{}, error) {
	url := fmt.Sprintf(createSignatureUrl, instance.baseIUGUURL)

	objJson, err := json.Marshal(signature)

	if err != nil {
		return nil, err
	}
	header := getIUGUAuthHeader()

	result, err := request.Post(context, url, objJson, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUSignature) ActivateSignature(context context.Context, signatureId string) (interface{}, error) {
	url := fmt.Sprintf(activateSignatureUrl, instance.baseIUGUURL, signatureId)

	header := getIUGUAuthHeader()

	result, err := request.Post(context, url, nil, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUSignature) SuspendSignature(context context.Context, signatureId string) (interface{}, error) {
	url := fmt.Sprintf(suspendSignatureUrl, instance.baseIUGUURL, signatureId)

	header := getIUGUAuthHeader()

	result, err := request.Post(context, url, nil, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUSignature) ChangeSignature(context context.Context, signatureId string, signature IuguSignatureBody, changes IuguSignatureChangeBody) (interface{}, error) {
	url := fmt.Sprintf(changeSignatureUrl, instance.baseIUGUURL, signatureId)

	objJson, err := json.Marshal(signature)

	if err != nil {
		return nil, err
	}
	header := getIUGUAuthHeader()

	result, err := request.Put(context, url, objJson, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUSignature) ChangePlanOfSignature(context context.Context, signatureId, newPlanIdentifier string) (interface{}, error) {
	url := fmt.Sprintf(changePlanOfSignatureUrl, instance.baseIUGUURL, signatureId, newPlanIdentifier)

	header := getIUGUAuthHeader()

	result, err := request.Post(context, url, nil, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUSignature) RemoveSignature(context context.Context, signatureId string) (interface{}, error) {
	url := fmt.Sprintf(removeSignatureUrl, instance.baseIUGUURL, signatureId)

	header := getIUGUAuthHeader()

	result, err := request.Delete(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUSignature) GetSignatureById(context context.Context, signatureId string) (interface{}, error) {
	url := fmt.Sprintf(getSignatureByIdUrl, instance.baseIUGUURL, signatureId)

	header := getIUGUAuthHeader()

	result, err := request.Get(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUSignature) GetAllSignatures(context context.Context, start, limit int) (interface{}, error) {
	urlLimit := ""
	urlStart := fmt.Sprintf("start=%v", start)

	if limit > 0 {
		urlLimit = fmt.Sprintf("limit=%v", limit)
	}

	url := fmt.Sprintf(getAllSignaturesUrl, instance.baseIUGUURL, urlLimit, urlStart)

	header := getIUGUAuthHeader()

	result, err := request.Get(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}
