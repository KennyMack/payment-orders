package validations

import "regexp"

func ClearString(str string, chars string) string {
	pattern := "[^" + chars + "]+"
	r, _ := regexp.Compile(pattern)
	return r.ReplaceAllString(str, "")
}
