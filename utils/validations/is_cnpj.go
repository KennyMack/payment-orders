package validations

import (
	"fmt"
	"strconv"
)

func sumDigit(s string, table []int) int {

	if len(s) != len(table) {
		return 0
	}

	sum := 0

	for i, v := range table {
		c := string(s[i])
		d, err := strconv.Atoi(c)
		if err == nil {
			sum += v * d
		}
	}

	return sum
}

func IsCNPJ(cpf string) bool {
	valClear := ClearString(cpf, "\\d")

	if len(valClear) != 14 {
		return false
	}

	if valClear == "00000000000000" ||
		valClear == "11111111111111" ||
		valClear == "22222222222222" ||
		valClear == "33333333333333" ||
		valClear == "44444444444444" ||
		valClear == "55555555555555" ||
		valClear == "66666666666666" ||
		valClear == "77777777777777" ||
		valClear == "88888888888888" ||
		valClear == "99999999999999" {
		return false
	}

	firstPart := valClear[:12]
	sum1 := sumDigit(firstPart, []int{5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2})
	rest1 := sum1 % 11
	d1 := 0

	if rest1 >= 2 {
		d1 = 11 - rest1
	}

	secondPart := fmt.Sprintf("%s%d", firstPart, d1)
	sum2 := sumDigit(secondPart, []int{6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2})
	rest2 := sum2 % 11
	d2 := 0

	if rest2 >= 2 {
		d2 = 11 - rest2
	}

	finalPart := fmt.Sprintf("%s%d", secondPart, d2)

	return finalPart == valClear
}
