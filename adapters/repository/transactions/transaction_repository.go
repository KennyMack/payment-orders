package transactions

import (
	"context"
	"fmt"
	"github.com/kamva/mgm/v3"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"payment_orders/adapters/db"
	"payment_orders/application/transactions"
)

type RepositoryTransactions struct{
	database db.IMongoDBInstance
	collectionName string
}

func NewRepositoryTransactions(database db.IMongoDBInstance) *RepositoryTransactions {
	return &RepositoryTransactions{
		database: database,
		collectionName: "Transactions",
	}
}
func (instance *RepositoryTransactions) OpenConnection() error {
	return instance.database.Open()
}

func (instance *RepositoryTransactions) CloseConnection() error {
	return instance.database.Close()
}

func (instance *RepositoryTransactions) GetById(ctx context.Context, id string) (*transactions.MTransactions, error) {
	result := &transactions.MTransactions{}

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	_ = collection.FindByIDWithCtx(ctx, id, result)
	collection = nil

	if result.ID.IsZero() {
		return nil, errors.New(fmt.Sprintf("Nenhum registro encontrado para o id: %v", id))
	}

	return result, nil
}

func (instance *RepositoryTransactions) GetByTransactionId(ctx context.Context, transactionId, operation string, completed bool) (*transactions.MTransactions, error) {
	var result transactions.MTransactions

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	cur := collection.FindOne(ctx, bson.M{ "transactionId": transactionId, "operation": operation, "completed": completed })

	collection = nil
	err := cur.Decode(&result)

	if err != nil {
		return nil, err
	}

	if result.ID.IsZero() {
		return nil, errors.New(fmt.Sprintf("Nenhum registro encontrado para o id: %v", transactionId))
	}

	return &result, nil
}

func (instance *RepositoryTransactions) GetAll(ctx context.Context, start, limit int) ([]*transactions.MTransactions, error) {
	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

	optSkip := int64(start)
	optLimit := int64(limit)

	opt := options.FindOptions{
		Skip: &optSkip,
		Limit: &optLimit,
	}

	var results []*transactions.MTransactions

	cur, err := collection.Find(ctx, bson.D{{}}, &opt)

	if err != nil {
		return nil, err
	}

	for cur.Next(ctx) {
		var elem transactions.MTransactions
		err := cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		results = append(results, &elem)
	}

	err = cur.Close(ctx)

	collection = nil
	if err != nil {
		return results, err
	}

	return results, nil
}

func (instance *RepositoryTransactions) Save(ctx context.Context, transaction transactions.ITransactions) (*transactions.MTransactions, error) {
	if instance.database.GetDbType() == db.MongoDB {
		collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

		newModel := transaction.GetModel()

		var err error
		if newModel.ID.IsZero() {
			err = collection.CreateWithCtx(ctx, newModel)
		} else {
			err = collection.UpdateWithCtx(ctx, newModel)
		}

		collection = nil
		if err != nil {
			return nil, err
		}

		return newModel.GetModel(), nil
	}

	return nil, nil
}

func (instance *RepositoryTransactions) Remove(ctx context.Context, transaction transactions.ITransactions) (*transactions.MTransactions, error) {
	if instance.database.GetDbType() == db.MongoDB {
		collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

		newModel := transaction.GetModel()

		err := collection.DeleteWithCtx(ctx, newModel)

		collection = nil
		if err != nil {
			return nil, err
		}

		return newModel.GetModel(), nil
	}
	return nil, nil
}
