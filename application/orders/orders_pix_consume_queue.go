package orders

import "payment_orders/adapters/queue"

type AppOrdersPixQueue struct {
	NewMessage   chan *queue.ContentMessageInstance
}

func MakeNewAppOrdersPixQueue() *AppOrdersPixQueue {
	return &AppOrdersPixQueue{
		NewMessage: make(chan *queue.ContentMessageInstance),
	}
}

func (instance *AppOrdersPixQueue) ConnectToAMQP(consumerAmqpConn queue.IRabbitConnection) error {
	config := queue.MakeOrdersPixRabbitMQCfg()
	consumer := queue.MakeNewConsumerFactory(consumerAmqpConn, config)
	err := consumer.CreateConsumer(config.ConsumerTag, false)
	if err != nil {
		return err
	}

	go consumer.Subscribe()

	go func() {
		for message := range consumer.NewMessage {
			instance.NewMessage <- message
			message = nil
		}
	}()

	return nil
}
