package orders

import (
	"payment_orders/adapters/db"
)

type DTOOrderItems struct {
	Description string `bson:"description" valid:"required~Descrição do item obrigatória"`
	Quantity int `bson:"quantity" valid:"required~Quantidade do item obrigatória"`
	PriceCents int `bson:"priceCents" valid:"required~Valor do item obrigatória"`
}

type DTOOrderPayerAddress struct {
	Street string `json:"street" bson:"street" valid:"required~Rua é obrigatório"`
	Complement string `json:"complement" bson:"complement" valid:"-"`
	District string `json:"district" bson:"district" valid:"required~Endereço é obrigatório"`
	City string `json:"city" bson:"city" valid:"required~Cidade é obrigatório"`
	State string `json:"state" bson:"state" valid:"required~Estado é obrigatório"`
	Country string `json:"country" bson:"country" valid:"required~País é obrigatório"`
	ZipCode string `json:"zipCode" bson:"zipCode" valid:"required~Cep é obrigatório"`
	Number string `json:"number" bson:"number" valid:"required~Número do endereço é obrigatório"`
}

type DTOOrderPayer struct {
	CpfCnpj string `json:"cpfCnpj" bson:"cpfCnpj" valid:"required~Documento é obrigatório"`
	Name string `json:"name" bson:"name" valid:"required~Nome é obrigatório"`
	PhonePrefix string `json:"phonePrefix" bson:"phonePrefix" valid:"required~DDD é obrigatório"`
	Phone string `json:"phone" bson:"phone" valid:"required~Telefone é obrigatório"`
	Email string `json:"email" bson:"email" valid:"required~Email é obrigatório"`
	Address *DTOOrderPayerAddress `json:"address" bson:"address" valid:"optional"`
}

type DTOOrders struct {
	db.DefaultModel `valid:"-"bson:",inline"`
	Status string `json:"status" bson:"status" valid:"required~Status obrigatório"`
	UniqueIdentifier string `json:"uniqueIdentifier" bson:"uniqueIdentifier" valid:"required~UniqueIdentifier obrigatório"`
	OwnId string `json:"ownId" bson:"ownId" valid:"required~OwnId obrigatório"`
	OrderId string `json:"orderId" bson:"orderId" valid:"required~orderId obrigatório"`
	Email string `json:"email" bson:"email" valid:"required~E-mail obrigatório"`
	DueDate string `json:"dueDate" bson:"dueDate" valid:"required~DueDate obrigatório"`
	EnsureWorkdayDueDate bool `json:"ensureWorkdayDueDate" bson:"ensureWorkdayDueDate" valid:"required~EnsureWorkdayDueDate obrigatório"`
	Items []*DTOOrderItems `json:"items" bson:"items" valid:"required~Items obrigatório"`
	ReturnUrl string `json:"returnUrl" bson:"returnUrl" valid:"optional"`
	ExpiredUrl string `json:"expiredUrl" bson:"expiredUrl" valid:"optional"`
	NotificationUrl string `json:"notificationUrl" bson:"notificationUrl" valid:"optional"`
	IgnoreCanceledEmail bool `json:"ignoreCanceledEmail" bson:"ignoreCanceledEmail" valid:"optional"`
	Fines bool `json:"fines" bson:"fines" valid:"optional"`
	LatePaymentFine int `json:"latePaymentFine" bson:"latePaymentFine" valid:"optional"`
	LatePaymentFineCents int `json:"latePaymentFineCents" bson:"latePaymentFineCents" valid:"optional"`
	PerDayInterest bool `json:"perDayInterest" bson:"perDayInterest" valid:"optional"`
	PerDayInterestValue int `json:"perDayInterestValue" bson:"perDayInterestValue" valid:"optional"`
	PerDayInterestCents int `json:"perDayInterestCents" bson:"perDayInterestCents" valid:"optional"`
	DiscountCents int `json:"discountCents" bson:"discountCents" valid:"optional"`
	CustomerId string `json:"customerId" bson:"customerId" valid:"required~CustomerId obrigatório"`
	IgnoreDueEmail bool `json:"ignoreDueEmail" bson:"ignoreDueEmail" valid:"optional"`
	SubscriptionId string `json:"subscriptionId" bson:"subscriptionId" valid:"optional"`
	PayableWith []string `json:"payableWith" bson:"payableWith" valid:"required~Payable With obrigatório"`
	EarlyPaymentDiscount bool `json:"earlyPaymentDiscount" bson:"earlyPaymentDiscount" valid:"optional"`
	EarlyPaymentDiscounts []string `json:"earlyPaymentDiscounts" bson:"earlyPaymentDiscounts" valid:"optional"`
	Payer *DTOOrderPayer `json:"payer" bson:"payer" valid:"required~Pagador é obrigatório"`
	CreditCard string `json:"creditCard" bson:"creditCard" valid:"optional"`
	Months int `json:"months" bson:"months" valid:"optional"`
	BankSlipExtraDays int `json:"bank_slip_extra_days" bson:"bank_slip_extra" valid:"optional"`
	KeepDunning bool `json:"keep_dunning" bson:"keep_dunning" valid:"optional"`
	DigitalLine string `json:"digitalLine" bson:"digitalLine" valid:"optional"`
	BarcodeData string `json:"barcodeData" bson:"barcodeData" valid:"optional"`
	BarcodeUrl string `json:"barcodeUrl" bson:"barcodeUrl" valid:"optional"`
	SecureUrl string `json:"secureUrl" bson:"secureUrl" valid:"optional"`
	SecureId string `json:"secureId" bson:"secureId" valid:"optional"`
	ChargePdf string `json:"pdf" bson:"pdf" valid:"optional"`
	ChargeMessage string `json:"chargeMessage" bson:"chargeMessage" valid:"optional"`
	ChargeStatus string `json:"chargeStatus" bson:"chargeStatus" valid:"optional"`
	ChargeToken string `json:"chargeToken" bson:"chargeToken" valid:"optional"`
	ChargeSuccess bool `json:"chargeSuccess" bson:"chargeSuccess" valid:"optional"`
	ChargeInfoMessage string `json:"chargeInfoMessage" bson:"chargeInfoMessage" valid:"optional"`

	PixStatus string `json:"pixStatus" bson:"pixStatus" valid:"optional"`
	PixQrCode string `json:"pixQrCode" bson:"pixQrCode" valid:"optional"`
	PixQrCodeText string `json:"pixQrCodeText" bson:"pixQrCodeText" valid:"optional"`
	PixPayerCpfCnpj string `json:"pixPayerCpfCnpj" bson:"pixPayerCpfCnpj" valid:"optional"`
	PixPayerName string `json:"pixPayerName" bson:"pixPayerName" valid:"optional"`
	PixEndToEndId string `json:"pixEndToEndId" bson:"pixEndToEndId" valid:"optional"`
	PixEndToEndRefundId string `json:"pixEndToEndRefundId" bson:"pixEndToEndRefundId" valid:"optional"`
}

func NewDTOOrders() *DTOOrders {
	order := DTOOrders{
		Status: "DRAFT",
		UniqueIdentifier: "AGUARDANDO",
		Items: make([]*DTOOrderItems, 0, 0),
		EarlyPaymentDiscounts: make([]string, 0, 0),
		PayableWith: []string{ ALL },
		IgnoreDueEmail: false,
		IgnoreCanceledEmail: false,
		EnsureWorkdayDueDate: true,
		CreditCard: "",
		Months: 1,
		BankSlipExtraDays: 0,
		KeepDunning: true,
		Payer: NewDTOOrderPayer(),
	}
	return &order
}

func NewDTOOrderPayerAddress() *DTOOrderPayerAddress {
	payerAddress := DTOOrderPayerAddress{
	}
	return &payerAddress
}

func NewDTOOrderPayer() *DTOOrderPayer {
	return &DTOOrderPayer{
		Address: NewDTOOrderPayerAddress(),
	}
}

func NewDTOOrderItems() *DTOOrderItems {
	return &DTOOrderItems{
		PriceCents: 0,
		Quantity: 0,
	}
}

const (
	ALL = "all"
	CREDIT_CARD = "credit_card"
	BANK_SLIP = "bank_slip"
	PIX = "pix"
)

const (
	PENDING = "PENDING"
	PAID = "PAID"
	AUTHORIZED = "AUTHORIZED"
	CANCELED = "CANCELED"
	IN_ANALYSIS = "IN_ANALYSIS"
	DRAFT = "DRAFT"
	PARTIALLY_PAID = "PARTIALLY_PAID"
	REFUNDED = "REFUNDED"
	EXPIRED = "EXPIRED"
	IN_PROTEST = "IN_PROTEST"
	CHARGEBACK = "CHARGEBACK"
	EXTERNALLY_PAID = "EXTERNALLY_PAID"
)

func NextStatus () map[string][]string {
	mapStatus := make(map[string][]string)

	mapStatus[PENDING] = []string{
		PAID,
		CANCELED,
		PARTIALLY_PAID,
		EXPIRED,
		AUTHORIZED,
		EXTERNALLY_PAID,
	}
	mapStatus[PAID] = []string{
		REFUNDED,
		IN_PROTEST,
		CHARGEBACK,
	}
	mapStatus[AUTHORIZED] = []string{
		PAID,
		CANCELED,
	}
	mapStatus[CANCELED] = []string{
		PAID,
	}
	mapStatus[IN_ANALYSIS] = []string{}
	mapStatus[DRAFT] = []string{}
	mapStatus[PARTIALLY_PAID] = []string{
		PAID,
	}
	mapStatus[REFUNDED] = []string{}
	mapStatus[EXPIRED] = []string{
		PAID,
	}
	mapStatus[IN_PROTEST] = []string{
		PAID,
		CHARGEBACK,
	}
	mapStatus[CHARGEBACK] = []string{}
	mapStatus[EXTERNALLY_PAID] = []string{}

	return mapStatus

}
