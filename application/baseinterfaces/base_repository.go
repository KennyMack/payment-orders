package baseinterfaces

type IBaseRepository interface {
	OpenConnection() error
	CloseConnection() error
}
