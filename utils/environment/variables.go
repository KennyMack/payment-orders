package environment

import "os"

func GetSERVER_PORT() string {
	return getEnvVar("SERVER_PORT")
}

func GetSERVER_API_VERSION() string {
	return getEnvVar("SERVER_API_VERSION")
}

func GetSERVER_NAME() string {
	return getEnvVar("SERVER_NAME")
}

func GetADMIN_EMAIL() string {
	return getEnvVar("ADMIN_EMAIL")
}

func GetSERVER_HOST() string {
	return getEnvVar("SERVER_HOST")
}

func GetAMPQ_URI() string {
	return getEnvVar("AMPQ_URI")
}

func GetVERBOSE_AMQP() string {
	return getEnvVar("VERBOSE_AMQP")
}

func GetMONGODB_URI() string {
	return getEnvVar("MONGODB_URI")
}

func GetMONGODB_DATABASE() string {
	return getEnvVar("MONGODB_DATABASE")
}

func GetIUGU_TOKEN() string {
	return getEnvVar("IUGU_TOKEN")
}

func GetIUGU_ACCOUNT_ID() string {
	return getEnvVar("IUGU_ACCOUNT_ID")
}

func GetIUGU_PRODUCTION() string {
	return getEnvVar("IUGU_PRODUCTION")
}

func GetIUGU_BASE_URL() string {
	return getEnvVar("IUGU_BASE_URL")
}

func GetNOTIFY_URL() string {
	return getEnvVar("NOTIFY_URL")
}

func GetSecret() string {
	return getEnvVar("SECRET")
}

func GetSecretKey() string {
	return getEnvVar("SECRETKEY")
}

func GetTesting() string {
	return getEnvVar("TESTING")
}

func getEnvVar(variable string) string {
	return os.Getenv(variable)
}



