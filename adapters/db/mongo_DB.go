package db

import (
	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"payment_orders/utils/environment"
)

type IMongoDBInstance interface {
	Open() error
	Close() error
	GetClient() *mongo.Client
	IsConnected() bool
	GetDbType() DataBaseType
	GetDataBase() *mongo.Database
}


type MongoDBInstance struct{
	ServerAPIOptions *options.ServerAPIOptions
	ClientOptions *options.ClientOptions
	Client *mongo.Client
	DataBaseName string
	DbType DataBaseType
	DataBase *mongo.Database
}

func (instance *MongoDBInstance) failOnError(err error, msg string) {
	if err != nil {
		log.Printf("%s: %s", msg, err)
	}
}

func NewMongoDB() *MongoDBInstance {
	return &MongoDBInstance{
		DataBaseName: environment.GetMONGODB_DATABASE(),
		DbType: MongoDB,
	}
}

func (instance *MongoDBInstance) GetDataBase() *mongo.Database {
	return instance.DataBase
}

func (instance *MongoDBInstance) GetDbType() DataBaseType {
	return instance.DbType
}

func (instance *MongoDBInstance) GetClient()  *mongo.Client {
	return instance.Client
}

func (instance *MongoDBInstance) IsConnected() bool {
	if instance.Client == nil {
		return false
	}

	err := instance.Client.Ping(mgm.Ctx(), readpref.Primary())
	if err != nil {
		return false
	}
	return true
}

func (instance *MongoDBInstance) Open() error {
	instance.ServerAPIOptions = options.ServerAPI(options.ServerAPIVersion1)
	instance.ClientOptions = options.Client().
		ApplyURI(environment.GetMONGODB_URI()).
		SetServerAPIOptions(instance.ServerAPIOptions)

	err := mgm.SetDefaultConfig(nil, instance.DataBaseName, instance.ClientOptions)
	instance.failOnError(err, "Não foi possível conectar ao bd.")
	if err != nil {
		return err
	}
	_, mongoClient, database, _ := mgm.DefaultConfigs()

	log.Printf("Conexão instance: %v", &mongoClient)

	instance.Client = mongoClient
	instance.DataBase = database

	return err
}

func (instance *MongoDBInstance) Close() error {
	if instance.Client != nil &&
		instance.IsConnected() {
		log.Printf("Desconectando bd da instance: %v", &instance.Client)
		err := instance.Client.Disconnect(mgm.Ctx())
		instance.Client = nil
		instance.failOnError(err, "Não foi possivel desconectar do bd")
		mgm.ResetDefaultConfig()
		return err
	}
	return nil
}
