package transactions

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"payment_orders/adapters/dto/transactions"
	"time"
)

type ServiceTransactions struct {
	repository ITransactionRepository
}

func NewServiceTransactions(repository ITransactionRepository) *ServiceTransactions {
	return &ServiceTransactions{
		repository: repository,
	}
}

func (instance *ServiceTransactions) OpenConnection() error {
	return instance.repository.OpenConnection()
}

func (instance *ServiceTransactions) CloseConnection() error {
	return instance.repository.CloseConnection()
}

func (instance *ServiceTransactions) Create(ctx context.Context, model *transactions.DTOTransactions) (*MTransactions, error) {
	dbModel := NewTransactionModel(model)

	_, err := dbModel.IsValid()

	if err != nil {
		return nil, err
	}

	if !dbModel.ID.IsZero() {
		return nil, errors.New("Id deve ser nulo para novos clientes")
	}

	newModel, err := instance.repository.Save(ctx, dbModel)

	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceTransactions) Change(ctx context.Context, id string, model *transactions.DTOTransactions) (*MTransactions, error) {
	dbModel := NewTransactionModel(model)

	_, err := dbModel.IsValid()

	if err != nil {
		return nil, err
	}

	if dbModel.ID.IsZero() {
		return nil, errors.New("Id deve ser informado")
	}

	oldModel, err := instance.repository.GetById(ctx, dbModel.ID.Hex())

	if oldModel == nil {
		return nil, errors.New( fmt.Sprintf("Nenhum registro encontrado para o id informado: Id: %v", dbModel.ID))
	}
	dbModel.CreatedAt = oldModel.CreatedAt

	newModel, err := instance.repository.Save(ctx, dbModel)

	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceTransactions) GetById(ctx context.Context, id string) (*MTransactions, error) {
	res, err:= instance.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (instance *ServiceTransactions) GetByTransactionId(ctx context.Context, transactionId, operation string, completed bool) (*MTransactions, error) {
	res, err := instance.repository.GetByTransactionId(ctx, transactionId, operation, completed)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (instance *ServiceTransactions) GetAll(ctx context.Context, start, limit int) ([]*MTransactions, error) {
	res, err:= instance.repository.GetAll(ctx, start, limit)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (instance *ServiceTransactions) StartTransaction(ctx context.Context, transactionId, operation string) (*MTransactions, error) {
	model, _ := instance.GetByTransactionId(ctx, transactionId, operation, false)

	if model == nil {
		model := &transactions.DTOTransactions{
			TransactionId: transactionId,
			Operation:     operation,
			MovementDate:  time.Now(),
			RetryCount:    0,
			ErrorMessage:  []string{},
		}

		return instance.Create(ctx, model)
	}
	return model, nil
}

func (instance *ServiceTransactions) UpdateCustomerId(ctx context.Context, transactionId, operation, customerId string) (*MTransactions, error) {
	transactionDb, err := instance.GetByTransactionId(ctx, transactionId, operation, false)
	if err != nil {
		return nil, err
	}

	customerIdObjId, err := primitive.ObjectIDFromHex(customerId)

	if err != nil {
		return nil, err
	}

	model := &transactions.DTOTransactions{
		TransactionId:  transactionDb.TransactionId,
		MovementDate:   transactionDb.MovementDate,
		Operation:      transactionDb.Operation,
		CustomerId:     customerIdObjId,
		PaymentId:      primitive.NilObjectID,
		OrderId:        primitive.NilObjectID,
		CancellationId: primitive.NilObjectID,
		Completed:      transactionDb.Completed,
		SentToServer:   transactionDb.SentToServer,
		Created:        transactionDb.Created,
		RetryCount:     transactionDb.RetryCount,
		ErrorMessage:   transactionDb.ErrorMessage,
	}
	model.ID = transactionDb.ID

	if !transactionDb.PaymentId.IsZero() {
		model.PaymentId = transactionDb.PaymentId
	}
	if !transactionDb.OrderId.IsZero() {
		model.OrderId = transactionDb.OrderId
	}
	if !transactionDb.CancellationId.IsZero() {
		model.CancellationId = transactionDb.CancellationId
	}

	return instance.Change(ctx, transactionDb.ID.Hex(), model)
}

func (instance *ServiceTransactions) UpdateCancellationId(ctx context.Context, transactionId, operation, cancellationId string) (*MTransactions, error) {
	transactionDb, err := instance.GetByTransactionId(ctx, transactionId, operation, false)
	if err != nil {
		return nil, err
	}

	cancellationIdObjId, err := primitive.ObjectIDFromHex(cancellationId)

	if err != nil {
		return nil, err
	}

	model := &transactions.DTOTransactions{
		TransactionId:  transactionDb.TransactionId,
		MovementDate:   transactionDb.MovementDate,
		Operation:      transactionDb.Operation,
		OrderId:        transactionDb.OrderId,
		CancellationId: cancellationIdObjId,
		CustomerId:     primitive.NilObjectID,
		PaymentId:      primitive.NilObjectID,
		Completed:      transactionDb.Completed,
		SentToServer:   transactionDb.SentToServer,
		Created:        transactionDb.Created,
		RetryCount:     transactionDb.RetryCount,
		ErrorMessage:   transactionDb.ErrorMessage,
	}
	model.ID = transactionDb.ID

	if !transactionDb.CustomerId.IsZero() {
		model.CustomerId = transactionDb.CustomerId
	}
	if !transactionDb.PaymentId.IsZero() {
		model.PaymentId = transactionDb.PaymentId
	}
	if !transactionDb.OrderId.IsZero() {
		model.OrderId = transactionDb.OrderId
	}

	return instance.Change(ctx, transactionDb.ID.Hex(), model)
}

func (instance *ServiceTransactions) UpdateOrderId(ctx context.Context, transactionId, operation, orderId string) (*MTransactions, error) {
	transactionDb, err := instance.GetByTransactionId(ctx, transactionId, operation, false)
	if err != nil {
		return nil, err
	}

	orderIdObjId, err := primitive.ObjectIDFromHex(orderId)

	if err != nil {
		return nil, err
	}

	model := &transactions.DTOTransactions{
		TransactionId:  transactionDb.TransactionId,
		MovementDate:   transactionDb.MovementDate,
		Operation:      transactionDb.Operation,
		OrderId:        orderIdObjId,
		CustomerId:     primitive.NilObjectID,
		PaymentId:      primitive.NilObjectID,
		CancellationId: primitive.NilObjectID,
		Completed:      transactionDb.Completed,
		SentToServer:   transactionDb.SentToServer,
		Created:        transactionDb.Created,
		RetryCount:     transactionDb.RetryCount,
		ErrorMessage:   transactionDb.ErrorMessage,
	}
	model.ID = transactionDb.ID

	if !transactionDb.CustomerId.IsZero() {
		model.CustomerId = transactionDb.CustomerId
	}
	if !transactionDb.PaymentId.IsZero() {
		model.PaymentId = transactionDb.PaymentId
	}
	if !transactionDb.CancellationId.IsZero() {
		model.CancellationId = transactionDb.CancellationId
	}

	return instance.Change(ctx, transactionDb.ID.Hex(), model)
}

func (instance *ServiceTransactions) UpdatePaymentId(ctx context.Context, transactionId, operation, paymentId string) (*MTransactions, error) {
	transactionDb, err := instance.GetByTransactionId(ctx, transactionId, operation, false)
	if err != nil {
		return nil, err
	}

	paymentIdObjId, err := primitive.ObjectIDFromHex(paymentId)

	if err != nil {
		return nil, err
	}

	model := &transactions.DTOTransactions{
		TransactionId:  transactionDb.TransactionId,
		MovementDate:   transactionDb.MovementDate,
		Operation:      transactionDb.Operation,
		PaymentId:      paymentIdObjId,
		CustomerId:     primitive.NilObjectID,
		CancellationId: primitive.NilObjectID,
		OrderId:        primitive.NilObjectID,
		Completed:      transactionDb.Completed,
		SentToServer:   transactionDb.SentToServer,
		Created:        transactionDb.Created,
		RetryCount:     transactionDb.RetryCount,
		ErrorMessage:   transactionDb.ErrorMessage,
	}
	model.ID = transactionDb.ID

	if !transactionDb.CustomerId.IsZero() {
		model.CustomerId = transactionDb.CustomerId
	}
	if !transactionDb.OrderId.IsZero() {
		model.OrderId = transactionDb.OrderId
	}
	if !transactionDb.CancellationId.IsZero() {
		model.CancellationId = transactionDb.CancellationId
	}

	return instance.Change(ctx, transactionDb.ID.Hex(), model)
}

func (instance *ServiceTransactions) UpdateToCompleted(ctx context.Context, transactionId, operation string) (*MTransactions, error) {
	transactionDb, err := instance.GetByTransactionId(ctx, transactionId, operation, false)
	if err != nil {
		return nil, err
	}

	model := &transactions.DTOTransactions{
		TransactionId:  transactionDb.TransactionId,
		MovementDate:   transactionDb.MovementDate,
		Operation:      transactionDb.Operation,
		CustomerId:     primitive.NilObjectID,
		OrderId:        primitive.NilObjectID,
		PaymentId:      primitive.NilObjectID,
		CancellationId: primitive.NilObjectID,
		Completed:      true,
		SentToServer:   transactionDb.SentToServer,
		Created:        transactionDb.Created,
		RetryCount:     transactionDb.RetryCount,
		ErrorMessage:   transactionDb.ErrorMessage,
	}
	model.ID = transactionDb.ID

	if !transactionDb.OrderId.IsZero() {
		model.OrderId = transactionDb.OrderId
	}
	if !transactionDb.CustomerId.IsZero() {
		model.CustomerId = transactionDb.CustomerId
	}
	if !transactionDb.PaymentId.IsZero() {
		model.PaymentId = transactionDb.PaymentId
	}
	if !transactionDb.CancellationId.IsZero() {
		model.CancellationId = transactionDb.CancellationId
	}

	return instance.Change(ctx, transactionDb.ID.Hex(), model)
}

func (instance *ServiceTransactions) UpdateToSent(ctx context.Context, transactionId, operation string) (*MTransactions, error) {
	transactionDb, err := instance.GetByTransactionId(ctx, transactionId, operation, false)
	if err != nil {
		return nil, err
	}

	model := &transactions.DTOTransactions{
		TransactionId:  transactionDb.TransactionId,
		MovementDate:   transactionDb.MovementDate,
		Operation:      transactionDb.Operation,
		CustomerId:     primitive.NilObjectID,
		OrderId:        primitive.NilObjectID,
		PaymentId:      primitive.NilObjectID,
		CancellationId: primitive.NilObjectID,
		Completed:      transactionDb.Completed,
		SentToServer:   true,
		Created:        transactionDb.Created,
		RetryCount:     transactionDb.RetryCount,
		ErrorMessage:   transactionDb.ErrorMessage,
	}
	model.ID = transactionDb.ID

	if !transactionDb.OrderId.IsZero() {
		model.OrderId = transactionDb.OrderId
	}
	if !transactionDb.CustomerId.IsZero() {
		model.CustomerId = transactionDb.CustomerId
	}
	if !transactionDb.PaymentId.IsZero() {
		model.PaymentId = transactionDb.PaymentId
	}
	if !transactionDb.CancellationId.IsZero() {
		model.CancellationId = transactionDb.CancellationId
	}

	return instance.Change(ctx, transactionDb.ID.Hex(), model)
}

func (instance *ServiceTransactions) UpdateToCreated(ctx context.Context, transactionId, operation string) (*MTransactions, error) {
	transactionDb, err := instance.GetByTransactionId(ctx, transactionId, operation, false)
	if err != nil {
		return nil, err
	}

	model := &transactions.DTOTransactions{
		TransactionId: transactionDb.TransactionId,
		MovementDate:  transactionDb.MovementDate,
		Operation:     transactionDb.Operation,
		CustomerId:    primitive.NilObjectID,
		OrderId:       primitive.NilObjectID,
		PaymentId:     primitive.NilObjectID,
		CancellationId: primitive.NilObjectID,
		Completed:     transactionDb.Completed,
		SentToServer:  transactionDb.SentToServer,
		Created:       true,
		RetryCount:    transactionDb.RetryCount,
		ErrorMessage:  transactionDb.ErrorMessage,
	}
	model.ID = transactionDb.ID

	if !transactionDb.OrderId.IsZero() {
		model.OrderId = transactionDb.OrderId
	}
	if !transactionDb.CustomerId.IsZero() {
		model.CustomerId = transactionDb.CustomerId
	}
	if !transactionDb.PaymentId.IsZero() {
		model.PaymentId = transactionDb.PaymentId
	}
	if !transactionDb.CancellationId.IsZero() {
		model.CancellationId = transactionDb.CancellationId
	}

	return instance.Change(ctx, transactionDb.ID.Hex(), model)
}

func (instance *ServiceTransactions) IncrementRetryCount(ctx context.Context, transactionId, operation string) (*MTransactions, error) {
	transactionDb, err := instance.GetByTransactionId(ctx, transactionId, operation, false)
	if err != nil {
		return nil, err
	}

	model := &transactions.DTOTransactions{
		TransactionId: transactionDb.TransactionId,
		MovementDate:  transactionDb.MovementDate,
		Operation:     transactionDb.Operation,
		Completed:     transactionDb.Completed,
		CustomerId:    primitive.NilObjectID,
		OrderId:       primitive.NilObjectID,
		PaymentId:     primitive.NilObjectID,
		CancellationId: primitive.NilObjectID,
		SentToServer:  transactionDb.SentToServer,
		Created:       transactionDb.Created,
		RetryCount:    transactionDb.RetryCount + 1,
		ErrorMessage:  transactionDb.ErrorMessage,
	}
	model.ID = transactionDb.ID

	if !transactionDb.OrderId.IsZero() {
		model.OrderId = transactionDb.OrderId
	}
	if !transactionDb.CustomerId.IsZero() {
		model.CustomerId = transactionDb.CustomerId
	}
	if !transactionDb.PaymentId.IsZero() {
		model.PaymentId = transactionDb.PaymentId
	}
	if !transactionDb.CancellationId.IsZero() {
		model.CancellationId = transactionDb.CancellationId
	}

	return instance.Change(ctx, transactionDb.ID.Hex(), model)
}

func (instance *ServiceTransactions) AddMessageError(ctx context.Context, transactionId, operation, errorMessage string) (*MTransactions, error) {
	transactionDb, err := instance.GetByTransactionId(ctx, transactionId, operation, false)
	if err != nil {
		return nil, err
	}

	model := &transactions.DTOTransactions{
		TransactionId: transactionDb.TransactionId,
		MovementDate:  transactionDb.MovementDate,
		Operation:     transactionDb.Operation,
		Completed:     transactionDb.Completed,
		CustomerId:    primitive.NilObjectID,
		OrderId:       primitive.NilObjectID,
		PaymentId:     primitive.NilObjectID,
		CancellationId: primitive.NilObjectID,
		SentToServer:  transactionDb.SentToServer,
		Created:       transactionDb.Created,
		RetryCount:    transactionDb.RetryCount + 1,
		ErrorMessage:  transactionDb.ErrorMessage,
	}
	model.ErrorMessage = append(model.ErrorMessage, errorMessage)
	model.ID = transactionDb.ID

	if !transactionDb.OrderId.IsZero() {
		model.OrderId = transactionDb.OrderId
	}
	if !transactionDb.CustomerId.IsZero() {
		model.CustomerId = transactionDb.CustomerId
	}
	if !transactionDb.PaymentId.IsZero() {
		model.PaymentId = transactionDb.PaymentId
	}
	if !transactionDb.CancellationId.IsZero() {
		model.CancellationId = transactionDb.CancellationId
	}

	return instance.Change(ctx, transactionDb.ID.Hex(), model)
}
