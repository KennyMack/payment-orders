package queue

import (
	"log"
	"time"
)

type ProducerFactoryInstance struct {
	parametersProducer RabbitMQConfig
	connection IRabbitConnection
	amqpInstanceProducer *RabbitMQInstance
}

func MakeNewProducerFactory(rabbitConn IRabbitConnection, config RabbitMQConfig) *ProducerFactoryInstance {
	return &ProducerFactoryInstance{
		parametersProducer: config,
		connection: rabbitConn,
	}
}

func (instance *ProducerFactoryInstance) CreateProducer(createDeadLetter bool) error {
	return instance.creator(1,  createDeadLetter)
}

func (instance *ProducerFactoryInstance) creator(generation int, createDeadLetter bool) error {
	instance.amqpInstanceProducer = MakeNewRabbitMQProducer(instance.connection, instance.parametersProducer)
	err := instance.amqpInstanceProducer.Producer(createDeadLetter)
	if err != nil && generation <= 3 {
		time.Sleep(time.Duration(generation*10) * time.Second)
		return instance.creator(generation + 1, createDeadLetter)
	} else if err != nil && generation > 3 {
		log.Panicf("%s: %v", "Não foi possível conectar ao RABBIT", err)
		return err
	}
	return nil
}

func (instance *ProducerFactoryInstance) PublishStringMessage(message string) {
	instance.amqpInstanceProducer.PublishMessage([]byte(message))
	_ = instance.amqpInstanceProducer.Channel.Close()
}

func (instance *ProducerFactoryInstance) PublishByteArrayMessage(message []byte) {
	instance.amqpInstanceProducer.PublishMessage(message)
	_ = instance.amqpInstanceProducer.Channel.Close()
}

func (instance *ProducerFactoryInstance) Close()  {
	if instance.amqpInstanceProducer != nil {
		_ = instance.amqpInstanceProducer.Channel.Close()
		instance.amqpInstanceProducer.Close()
	}
}
