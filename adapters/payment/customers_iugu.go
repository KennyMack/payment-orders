package payment

import (
	"context"
	"encoding/json"
	"fmt"
	"payment_orders/utils/environment"
	"payment_orders/utils/request"
)

const (
	createCustomerUrl  = "%s/customers?"
	changeCustomerUrl  = "%s/customers/%s?"
	removeCustomerUrl  = "%s/customers/%s?"
	getCustomerByIdUrl = "%s/customers/%s?"
	getAllCustomersUrl = "%s/customers?%s&%s"
)

type IUGUCustomers struct {
	baseIUGUURL string
}

func NewIUGUCustomers() *IUGUCustomers {
	return &IUGUCustomers{
		baseIUGUURL: environment.GetIUGU_BASE_URL(),
	}
}

func (instance *IUGUCustomers) CreateCustomer(context context.Context, client IuguClientBody) (interface{}, error) {
	url := fmt.Sprintf(createCustomerUrl, instance.baseIUGUURL)

	clientJson, err := json.Marshal(client)

	if err != nil {
		return nil, err
	}
	header := getIUGUAuthHeader()

	result, err := request.Post(context, url, clientJson, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}

func (instance *IUGUCustomers) ChangeCustomer(context context.Context, clientId string, client IuguClientBody) (interface{}, error) {
	url := fmt.Sprintf(changeCustomerUrl, instance.baseIUGUURL, clientId)

	clientJson, err := json.Marshal(client)

	if err != nil {
		return nil, err
	}

	header := getIUGUAuthHeader()

	result, err := request.Put(context, url, clientJson, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}
func (instance *IUGUCustomers) RemoveCustomer(context context.Context, clientId string) (interface{}, error) {
	url := fmt.Sprintf(removeCustomerUrl, instance.baseIUGUURL, clientId)

	header := getIUGUAuthHeader()

	result, err := request.Delete(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}
func (instance *IUGUCustomers) GetCustomerById(context context.Context, clientId string) (interface{}, error) {
	url := fmt.Sprintf(getCustomerByIdUrl, instance.baseIUGUURL, clientId)

	header := getIUGUAuthHeader()

	result, err := request.Get(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}
func (instance *IUGUCustomers) GetAllCustomers(context context.Context, start, limit int) (interface{}, error) {
	urlLimit := ""
	urlStart := fmt.Sprintf("start=%v", start)

	if limit > 0 {
		urlLimit = fmt.Sprintf("limit=%v", limit)
	}

	url := fmt.Sprintf(getAllCustomersUrl, instance.baseIUGUURL, urlLimit, urlStart)

	header := getIUGUAuthHeader()

	result, err := request.Get(context, url, header)

	if err != nil{
		// handle error
		return nil, err
	}

	return result, nil
}
