package queue

const (
	SYNC = "SYNC"
	CUSTOMER = "CUSTOMER"
	WEBHOOK = "WEBHOOK"
	ORDER = "ORDER"
	ORDER_BOLETO = "ORDER_BOLETO"
	ORDER_CARD = "ORDER_CARD"
	ORDER_CANCEL = "ORDER_CANCEL"
)

type RabbitMQConfig struct {
	Configuration string
	QueueName    string
	ExchangeName string
	ExchangeType string
	RoutingKey   string
	ConsumerTag  string
	ConsumerCount int
	ChannelAutoAck bool
	ChannelExclusive bool
	ChannelNoLocal bool
	ChannelNoWait bool

	QueueDurable bool
	QueueAutoDelete   bool
	QueueExclusive bool
	QueueNoWait bool

	MessageDeliveryMode uint8
}

func MakeCustomersRabbitMQCfg() RabbitMQConfig {
	return RabbitMQConfig{
		Configuration: CUSTOMER,
		QueueName:      "customers",
		ExchangeName:   "customers-exchange",
		ExchangeType:   "direct",
		ConsumerTag:    "customer-iugu",
		ConsumerCount:  1,
		QueueDurable:   true,
		ChannelAutoAck: false,
	}
}

func MakeSyncRabbitMQCfg() RabbitMQConfig {
	return RabbitMQConfig{
		Configuration: SYNC,
		QueueName:      "sync",
		ExchangeName:   "sync-exchange",
		ExchangeType:   "direct",
		ConsumerTag:    "synchronize-iugu",
		ConsumerCount:  1,
		QueueDurable:   true,
		ChannelAutoAck: false,
	}
}

func MakeWebHookRabbitMQCfg() RabbitMQConfig {
	return RabbitMQConfig{
		Configuration: WEBHOOK,
		QueueName:      "webhook",
		ExchangeName:   "webhook-exchange",
		ExchangeType:   "direct",
		ConsumerTag:    "webhook-iugu",
		ConsumerCount:  1,
		QueueDurable:   true,
		ChannelAutoAck: false,
	}
}

func MakeOrdersBoletoRabbitMQCfg() RabbitMQConfig {
	return RabbitMQConfig{
		Configuration: ORDER_BOLETO,
		QueueName:      "orders-boleto",
		ExchangeName:   "orders-boleto-exchange",
		ExchangeType:   "direct",
		ConsumerTag:    "orders-boleto-iugu",
		ConsumerCount:  1,
		QueueDurable:   true,
		ChannelAutoAck: false,
	}
}

func MakeOrdersCardRabbitMQCfg() RabbitMQConfig {
	return RabbitMQConfig{
		Configuration: ORDER_CARD,
		QueueName:      "orders-card",
		ExchangeName:   "orders-card-exchange",
		ExchangeType:   "direct",
		ConsumerTag:    "orders-card-iugu",
		ConsumerCount:  1,
		QueueDurable:   true,
		ChannelAutoAck: false,
	}
}

func MakeOrdersPixRabbitMQCfg() RabbitMQConfig {
	return RabbitMQConfig{
		Configuration: ORDER_CARD,
		QueueName:      "orders-pix",
		ExchangeName:   "orders-pix-exchange",
		ExchangeType:   "direct",
		ConsumerTag:    "orders-pix-iugu",
		ConsumerCount:  1,
		QueueDurable:   true,
		ChannelAutoAck: false,
	}
}

func MakeOrdersCancellationRabbitMQCfg() RabbitMQConfig {
	return RabbitMQConfig{
		Configuration: ORDER_CANCEL,
		QueueName:      "orders-cancel",
		ExchangeName:   "orders-cancel-exchange",
		ExchangeType:   "direct",
		ConsumerTag:    "orders-cancel-iugu",
		ConsumerCount:  1,
		QueueDurable:   true,
		ChannelAutoAck: false,
	}
}

func MakeOrdersCustomerRabbitMQCfg() RabbitMQConfig {
	return RabbitMQConfig{
		Configuration: ORDER,
		QueueName:      "orders",
		ExchangeName:   "orders-exchange",
		ExchangeType:   "direct",
		ConsumerTag:    "orders-iugu",
		ConsumerCount:  1,
		QueueDurable:   true,
		ChannelAutoAck: false,
	}
}
