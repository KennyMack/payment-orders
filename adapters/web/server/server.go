package web

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
	"log"
	"net/http"
	"payment_orders/adapters/db"
	"payment_orders/adapters/payment"
	"payment_orders/adapters/queue"
	repoTriggers "payment_orders/adapters/repository/triggers"
	repoCustomers "payment_orders/adapters/repository/customers"
	repoOrders "payment_orders/adapters/repository/orders"
	"payment_orders/application/triggers"
	servTriggers "payment_orders/application/triggers"
	servOrders "payment_orders/application/orders"
	servCustomers "payment_orders/application/customers"
	"payment_orders/utils"
	"payment_orders/utils/environment"
)

type WebServer struct {
}

func MakeNewWebServer() *WebServer {
	return &WebServer{
	}
}

func handleHealthCheck(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("{ \"status\": \"Eu estou no ar!!\"}\n"))
	if err != nil {
		log.Print("error handleMain", err)
	}
}

func processTrigger(trigger *triggers.MTriggerData) {
	instanceDb := db.NewMongoDB()
	errConn := instanceDb.Open()
	if errConn != nil {
		msgErr := fmt.Sprintf("Não foi possível processar a transação. err: %v", errConn)

		log.Printf(msgErr)
		return
	}

	repositoryTriggers := repoTriggers.NewRepositoryTriggers(instanceDb)
	repositoryOrders := repoOrders.NewRepositoryOrders(instanceDb)
	repositoryCustomers := repoCustomers.NewRepositoryCustomers(instanceDb)

	serviceTriggers := servTriggers.NewServiceTriggers(repositoryTriggers)
	serviceOrders := servOrders.NewServiceOrders(repositoryOrders)
	serviceCustomers := servCustomers.NewServiceCustomers(repositoryCustomers)

	publishAmqpConn := queue.MakeNewRabbitConnection()

	app := servTriggers.NewAppTriggers(publishAmqpConn, serviceTriggers, serviceOrders, serviceCustomers)

	ctx := context.Background()
	app.ProcessTrigger(ctx, trigger)
	_ = publishAmqpConn.CloseConnection()
	// app.CloseConnection(serviceTriggers)
}

func handlePostTriggerJson(w http.ResponseWriter, r *http.Request) {
	headerContentType := r.Header.Get("Content-Type")
	if headerContentType != "application/json" {
		w.WriteHeader(http.StatusUnsupportedMediaType)
		return
	}

	event, errEvent := payment.DecodeJsonToObject(r)
	if errEvent != nil {
		_, _ = w.Write([]byte("{ \"received\": true }\n"))
		return
	}

	go processTrigger(event)

	_, err := w.Write([]byte("{ \"received\": true }\n"))
	if err != nil {
		log.Print("error handleMain", err)
	}
}

func handlePostTrigger(w http.ResponseWriter, r *http.Request) {
	headerContentType := r.Header.Get("Content-Type")
	if headerContentType != "application/x-www-form-urlencoded" {
		w.WriteHeader(http.StatusUnsupportedMediaType)
		return
	}
	errForm := r.ParseForm()
	if errForm != nil {
		_, _ = w.Write([]byte("{ \"received\": true }\n"))
		return
	}

	event, errEvent := payment.ConvertToObject(payment.ParseTriggerBody(r.PostForm))
	if errEvent != nil {
		_, _ = w.Write([]byte("{ \"received\": true }\n"))
		return
	}

	go processTrigger(event)

	_, err := w.Write([]byte("{ \"received\": true }\n"))
	if err != nil {
		log.Print("error handleMain", err)
	}
}

func applicationJSON() negroni.Handler {
	return negroni.HandlerFunc(func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		w.Header().Set("Content-Type", "application/json")
		next(w, r)
	})
}

func basicAuth() negroni.Handler {
	return negroni.HandlerFunc(func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		if r.URL.Path == "/healthcheck" || r.URL.Path == "/healthcheck/" ||
			r.URL.Path == "" || r.URL.Path == "/" || r.URL.Path == "/favicon.ico" {
			next(w, r)
			return
		}
		secret := utils.Crypto(environment.GetSecret())
		secretKey := utils.Crypto(environment.GetSecretKey())

		log.Printf("secret: %v", secret)
		log.Printf("secret: %v", secretKey)

		user, pass, ok := r.BasicAuth()

		log.Printf("user: %v", user)
		log.Printf("pass: %v", pass)

		if !ok || user != secret || pass != secretKey {
			w.WriteHeader(http.StatusUnauthorized)
			fmt.Println(w, `{"error": "Unauthorized"}`)
			return
		}

		w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)
		next(w, r)
	})
}

func (web WebServer) Serve(port string) {
	n := negroni.Classic()
	n.Use(applicationJSON())
	n.Use(basicAuth())

	r := mux.NewRouter().StrictSlash(true)
	n.UseHandler(r)

	r.HandleFunc("/healthcheck", handleHealthCheck).Methods("GET")
	r.HandleFunc("/triggers", handlePostTrigger).Methods("POST")
	r.HandleFunc("/triggers/json", handlePostTriggerJson).Methods("POST")

	log.Print("http listening at: ", environment.GetSERVER_HOST(), ":", port)

	err := http.ListenAndServe(fmt.Sprintf("%v:%v", environment.GetSERVER_HOST(), port), n)
	if err != nil {
		log.Fatal(err)
	}
}
