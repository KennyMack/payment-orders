package transactions

import (
	"context"
	validator "github.com/asaskevich/govalidator"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"payment_orders/adapters/db"
	"payment_orders/adapters/dto/transactions"
	baseRepository "payment_orders/application/baseinterfaces"
	"time"
)

func init() {
	validator.SetFieldsRequiredByDefault(true)
}

type MTransactions struct {
	db.DefaultModel   `valid:"-"bson:",inline"`
	MovementDate       time.Time `bson:"movementDate" valid:"required~Data movimento"`
	TransactionId      string `bson:"transactionId" valid:"required~Id da transação"`
	Operation          string `bson:"operation" valid:"required~Operação"`
	OrderId            primitive.ObjectID `bson:"orderId,omitempty" valid:"-" bson:",omitempty"`
	CustomerId         primitive.ObjectID `bson:"customerId,omitempty" valid:"-" bson:",omitempty"`
	PaymentId          primitive.ObjectID `bson:"paymentId,omitempty" valid:"-" bson:",omitempty"`
	CancellationId     primitive.ObjectID `bson:"cancellationId,omitempty" valid:"-" bson:",omitempty"`
	Created            bool `bson:"created" valid:"-"`
	SentToServer       bool `bson:"sentToServer" valid:"-"`
	Completed          bool `bson:"completed" valid:"-"`
	RetryCount         int `bson:"retryCount" valid:"-"`
	ErrorMessage       []string `bson:"errorMessage" valid:"-"`
}

type ITransactions interface {
	IsValid() (bool, error)
	GetModel() *MTransactions
	GetDTO() *transactions.DTOTransactions
}

type ITransactionsService interface {
	baseRepository.IBaseRepository
	Create(ctx context.Context, transaction *transactions.DTOTransactions) (*MTransactions, error)
	Change(ctx context.Context, id string, transaction *transactions.DTOTransactions) (*MTransactions, error)
	StartTransaction(ctx context.Context, transactionId, operation string) (*MTransactions, error)
	UpdateCustomerId(ctx context.Context, transactionId, operation, customerId string) (*MTransactions, error)
	UpdateCancellationId(ctx context.Context, transactionId, operation, cancellationId string) (*MTransactions, error)
	UpdateOrderId(ctx context.Context, transactionId, operation, orderId string) (*MTransactions, error)
	UpdatePaymentId(ctx context.Context, transactionId, operation, paymentId string) (*MTransactions, error)
	UpdateToSent(ctx context.Context, transactionId, operation string) (*MTransactions, error)
	UpdateToCreated(ctx context.Context, transactionId, operation string) (*MTransactions, error)
	UpdateToCompleted(ctx context.Context, transactionId, operation string) (*MTransactions, error)
	IncrementRetryCount(ctx context.Context, transactionId, operation string) (*MTransactions, error)
	AddMessageError(ctx context.Context, transactionId, operation, errorMessage string) (*MTransactions, error)
	GetById(ctx context.Context, id string) (*MTransactions, error)
	GetByTransactionId(ctx context.Context, transactionId, operation string, completed bool) (*MTransactions, error)
}

type ITransactionsReader interface{
	GetById(ctx context.Context, id string) (*MTransactions, error)
	GetByTransactionId(ctx context.Context, transactionId, operation string, completed bool) (*MTransactions, error)
	GetAll(ctx context.Context, start, limit int) ([]*MTransactions, error)
}

type ITransactionsWriter interface {
	Save(ctx context.Context, transaction ITransactions) (*MTransactions, error)
	Remove(ctx context.Context, transaction ITransactions) (*MTransactions, error)
}

type ITransactionRepository interface {
	baseRepository.IBaseRepository
	ITransactionsReader
	ITransactionsWriter
}

func NewTransactionModel(dtoModel *transactions.DTOTransactions) *MTransactions {
	transaction := MTransactions{}

	if dtoModel != nil {
		transaction.ID = dtoModel.ID
		transaction.CreatedAt      = dtoModel.CreatedAt
		transaction.UpdatedAt      = dtoModel.UpdatedAt
		transaction.MovementDate   = dtoModel.MovementDate
		transaction.TransactionId  = dtoModel.TransactionId
		transaction.Operation      = dtoModel.Operation
		transaction.OrderId        = dtoModel.OrderId
		transaction.CustomerId     = dtoModel.CustomerId
		transaction.PaymentId      = dtoModel.PaymentId
		transaction.CancellationId = dtoModel.CancellationId
		transaction.ErrorMessage   = dtoModel.ErrorMessage
		transaction.RetryCount     = dtoModel.RetryCount
		transaction.Completed      = dtoModel.Completed
		transaction.Created        = dtoModel.Created
		transaction.SentToServer   = dtoModel.SentToServer
	}
	return &transaction
}

func (transaction *MTransactions) IsValid() (bool, error) {
	_, err := validator.ValidateStruct(transaction)
	if err != nil {
		return false, err
	}

	return true, nil
}

func (transaction *MTransactions) GetModel() *MTransactions {
	return transaction
}

func (transaction *MTransactions) GetDTO() *transactions.DTOTransactions {
	dto := transactions.DTOTransactions{
		DefaultModel: transaction.DefaultModel,
		MovementDate : transaction.MovementDate,
		TransactionId: transaction.TransactionId,
		Operation    : transaction.Operation,
		OrderId      : transaction.OrderId,
		CustomerId   : transaction.CustomerId,
		PaymentId    : transaction.PaymentId,
		CancellationId: transaction.CancellationId,
		ErrorMessage : transaction.ErrorMessage,
		Created : transaction.Created,
		SentToServer : transaction.SentToServer,
		Completed : transaction.Completed,
		RetryCount : transaction.RetryCount,
	}
	return &dto
}
