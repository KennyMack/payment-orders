package payment

import (
	"context"
	"time"
)

// ### General ###
type IuguCustomVariable struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}
// ### General ###

// ### Clients ###
type IuguClientBody struct {
	Email        string `json:"email"`
	Name         string `json:"name"`
	Notes        string `json:"notes,omitempty"`
	PhonePrefix  string `json:"phone_prefix"`
	Phone        string `json:"phone"`
	CpfCnpj      string `json:"cpf_cnpj"`
	CcEmails     string `json:"cc_emails,omitempty"`
	ZipCode      string `json:"zip_code"`
	Number     string `json:"number"`
	Street     string `json:"street"`
	City       string `json:"city"`
	State      string `json:"state"`
	District   string `json:"district"`
	Complement string `json:"complement,omitempty"`
	CustomVariables       []IuguCustomVariable `json:"custom_variables,omitempty"`
	DefaultPaymentMethodId string `json:"default_payment_method_id,omitempty"`
}

type ICustomersInterface interface {
	CreateCustomer(context context.Context, client IuguClientBody) (interface{}, error)
	ChangeCustomer(context context.Context, clientId string, client IuguClientBody) (interface{}, error)
	RemoveCustomer(context context.Context, clientId string) (interface{}, error)
	GetCustomerById(context context.Context, clientId string) (interface{}, error)
	GetAllCustomers(context context.Context, start, limit int) (interface{}, error)
}
// ### Clients ###

// ### Invoice ###
type IuguInvoiceItemBody struct {
	Description string `json:"description"`
	Quantity    int `json:"quantity"`
	PriceCents  int `json:"price_cents"`
}

type IuguEarlyPaymentDiscounts struct {
	Days       int `json:"days"`
	Percent    float64 `json:"percent"`
	ValueCents float64 `json:"value_cents"`
}

type IuguAddress struct {
	ZipCode    string `json:"zip_code"`
	Street     string `json:"street"`
	Number     string `json:"number"`
	District   string `json:"district"`
	City       string `json:"city"`
	State      string `json:"state"`
	Country    string `json:"country"`
	Complement string `json:"complement"`
}

type IuguPayer struct {
	CpfCnpj      string `json:"cpf_cnpj"`
	Name         string `json:"name"`
	PhonePrefix  string `json:"phone_prefix"`
	Phone        string `json:"phone"`
	Email        string `json:"email"`
	Address      IuguAddress `json:"address"`
}

type IuguInvoiceBody struct{
	Email                 string `json:"email"`
	CcEmails              string `json:"cc_emails,omitempty"`
	DueDate               string `json:"due_date"`
	EnsureWorkdayDueDate  bool `json:"ensure_workday_due_date"`
	Items                 []IuguInvoiceItemBody `json:"items,omitempty"`
	ReturnUrl             string `json:"return_url,omitempty"`
	ExpiredUrl            string `json:"expired_url,omitempty"`
	NotificationUrl       string `json:"notification_url,omitempty"`
	IgnoreCanceledEmail   bool `json:"ignore_canceled_email"`
	Fines                 bool `json:"fines,omitempty"`
	LatePaymentFine       int `json:"late_payment_fine,omitempty"`
	LatePaymentFineCents  int `json:"late_payment_fine_cents,omitempty"`
	PerDayInterest        bool `json:"per_day_interest,omitempty"`
	PerDayInterestValue   int `json:"per_day_interest_value,omitempty"`
	PerDayInterestCents   int `json:"per_day_interest_cents,omitempty"`
	DiscountCents         int `json:"discount_cents,omitempty"`
	CustomerId            string `json:"customer_id"`
	IgnoreDueEmail        bool `json:"ignore_due_email"`
	SubscriptionId        string `json:"subscription_id,omitempty"`
	PayableWith           []string `json:"payable_with"`
	CustomVariables       []IuguCustomVariable `json:"custom_variables,omitempty"`
	EarlyPaymentDiscount  bool `json:"early_payment_discount,omitempty"`
	EarlyPaymentDiscounts []IuguEarlyPaymentDiscounts `json:"early_payment_discounts,omitempty"`
	Payer                 *IuguPayer `json:"payer"`
	OrderId               string `json:"order_id,omitempty"`
	ExternalReference     string `json:"external_reference,omitempty"`
	MaxInstallmentsValue  int `json:"max_installments_value,omitempty"`
}

type IInvoicesInterface interface {
	CreateInvoice(context context.Context, invoice IuguInvoiceBody) (interface{}, error)
	DirectCharge(context context.Context, charge IuguChargeBody) (interface{}, error)
	CaptureInvoice(context context.Context, invoiceId string) (interface{}, error)
	RefundInvoice(context context.Context, invoiceId string, partialValue float64) (interface{}, error)
	CancelInvoice(context context.Context, invoiceId string) (interface{}, error)
	GenerateSecondInvoice(context context.Context, invoiceId string, newDueDate time.Time) (interface{}, error)
	GetInvoiceById(context context.Context, clientId string) (interface{}, error)
	GetInvoiceAll(context context.Context, start, limit int) (interface{}, error)
	SendInvoiceToEmail(context context.Context, invoiceId string) (interface{}, error)
}
// ### Invoice ###

// ### Direct Charge ###
type IuguChargeBody struct {
	CustomerPaymentMethodId string `json:"customer_payment_method_id"`
	//CustomerId              string `json:"customer_id"`
	InvoiceId               string `json:"invoice_id"`
	// Email                   string `json:"email"`
	// Months                  int `json:"months"`
	// DiscountCents           int `json:"discount_cents,omitempty"`
	// BankSlipExtraDays       int `json:"bank_slip_extra_days"`
	// KeepDunning             bool `json:"keep_dunning"`
	// Items                   []IuguInvoiceItemBody `json:"items,omitempty"`
	// Payer                   *IuguPayer `json:"payer"`
}
// ### Direct Charge ###

// ### Payment ###
type IuguCardData struct {
	Number            string `json:"number"`
	VerificationValue string `json:"verification_value"`
	FirstName         string `json:"first_name"`
	LastName          string `json:"last_name"`
	Month             string `json:"month"`
	Year              string `json:"year"`
}

type IuguTokenBody struct {
	AccountId string `json:"account_id"`
	Method    string `json:"method"`
	Test      bool   `json:"test"`
	Data      IuguCardData `json:"data"`
}

type IuguPaymentMethodBody struct {
	Description   string `json:"description"`
	Token         string `json:"token"`
	SetAsDefault  bool   `json:"set_as_default"`
}

type IPaymentTokenInterface interface {
	GenerateToken(context context.Context, token IuguTokenBody) (interface{}, error)
	CreatePaymentMethod(context context.Context, customerId string, paymentMethod IuguPaymentMethodBody) (interface{}, error)
	ChangePaymentMethod(context context.Context, customerId, paymentId, description string) (interface{}, error)
	RemovePaymentMethod(context context.Context, customerId, paymentId string) (interface{}, error)
	GetPaymentMethodById(context context.Context, customerId, paymentId string) (interface{}, error)
	GetAllPaymentMethod(context context.Context, customerId string) (interface{}, error)
}
// ### Payment ###

// ### Plans ###
type IuguPlanBody struct {
	Name         string `json:"name"`
	Identifier   string `json:"identifier"`
	Interval     int `json:"interval"`
	IntervalType string `json:"interval_type"`
	ValueCents   float64 `json:"value_cents"`
	PayableWith  []string `json:"payable_with"`
	BillingDays  int `json:"billing_days"`
	MaxCycles    float64 `json:"max_cycles"`
}

type IPlansInterface interface {
	CreatePlan(context context.Context, plan IuguPlanBody) (interface{}, error)
	ChangePlan(context context.Context, planId string, plan IuguPlanBody) (interface{}, error)
	RemovePlan(context context.Context, planId string) (interface{}, error)
	GetPlanById(context context.Context, planId string) (interface{}, error)
	GetPlanByIdentifier(context context.Context, identifier string) (interface{}, error)
	GetAllPlans(context context.Context, start, limit int) (interface{}, error)
}
// ### Plans ###

// ### Signature ###
type IuguSignatureItemBody struct {
	Id           string `json:"id,omitempty"`
	PriceCents   float64 `json:"price_cents"`
	Quantity     float64 `json:"quantity"`
	Description  string `json:"description"`
	Recurrent    bool `json:"recurrent"`
	Destroy      string `json:"_destroy,omitempty"`
}

type IuguSignatureBody struct {
	PlanIdentifier             string `json:"plan_identifier"`
	CustomerId                 string `json:"customer_id"`
	ExpiresAt                  time.Time `json:"expires_at"`
	OnlyOnChargeSuccess        bool `json:"only_on_charge_success"`
	IgnoreDueEmail             bool `json:"ignore_due_email"`
	PayableWith                []string `json:"payable_with"`
	PriceCents                 float64 `json:"price_cents"`
	SubItems                   []IuguSignatureItemBody `json:"subitems"`
	CustomVariables            []IuguCustomVariable `json:"custom_variables"`
	TwoStep                    bool `json:"two_step"`
	SuspendOnInvoiceExpired    bool `json:"suspend_on_invoice_expired"`
	OnlyChargeOnDueDate        bool `json:"only_charge_on_due_date"`
}

type IuguSignatureChangeBody struct {
	SkipCharge   bool `json:"skip_charge"`
	Suspended    bool `json:"suspended"`
}

type ISignatureInterface interface {
	CreateSignature(context context.Context, signature IuguSignatureBody) (interface{}, error)
	ActivateSignature(context context.Context, signatureId string) (interface{}, error)
	SuspendSignature(context context.Context, signatureId string) (interface{}, error)
	ChangeSignature(context context.Context, signatureId string, signature IuguSignatureBody, changes IuguSignatureChangeBody) (interface{}, error)
	ChangePlanOfSignature(context context.Context, signatureId, newPlanIdentifier string) (interface{}, error)
	RemoveSignature(context context.Context, signatureId string) (interface{}, error)
	GetSignatureById(context context.Context, signatureId string) (interface{}, error)
	GetAllSignatures(context context.Context, start, limit int) (interface{}, error)
}
// ### Signature ###
