package payments

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"payment_orders/adapters/db"
)

type DTOCreateCard struct {
	db.DefaultModel `valid:"-"bson:",inline"`
	CustomerId   primitive.ObjectID `bson:"customerId" valid:"required~Código do cliente obrigatório"`
	CardToken    string `bson:"cardToken" valid:"required~Token do cartão é obrigatório"`
	Description  string `bson:"description" valid:"required~Descrição é obrigatória"`
	SetAsDefault bool `bson:"setAsDefault" valid:"optional"`
}

type DTOCreditCard struct {
	db.DefaultModel `bson:",inline"`
	Brand string `valid:"required~Bandeira do cartão é obrigatória"`
	First6 string `valid:"required~Primeiros digitos do cartão é obrigatório"`
	Last4 string `valid:"required~Ultimos digitos do cartão é obrigatório"`
	HolderName string `valid:"required~Nome do dono do cartão é obrigatório"`
	Year string `valid:"required~Ano de expiração é obrigatório"`
	Month string `valid:"required~Mês de expiração é obrigatório"`
	Test bool `valid:"required~Tipo do cartão é obrigatório"`
	DisplayNumber string `valid:"required~Nome descrito no cartão é obrigatório"`
	SetAsDefault bool `valid:"required"`
	Description string `valid:"required~Descrição do cartão é obrigatório"`
	IdPaymentToken string `valid:"required~Token de pagamento é obrigatório"`
}

func NewDTOCreateCard() *DTOCreateCard {
	card := DTOCreateCard{
		SetAsDefault: false,
		Description: "Novo Cartão",
	}
	return &card
}

func NewDTOCreditCard() *DTOCreditCard {
	card := DTOCreditCard{
		SetAsDefault: false,
		Description: "Novo Cartão",
		Test: true,
	}
	return &card
}
