package orders

import (
	dtoCustomers "payment_orders/adapters/dto/customers"
	dtoCancellation "payment_orders/adapters/dto/cancellations"
)


type AppOrderBody struct {
	Order DTOOrders `json:"order" bson:"order"`
	Customer dtoCustomers.DTOCustomers `json:"customer" bson:"customer"`
	Cancellation dtoCancellation.DTOOrdersCancellation `json:"cancellation" bson:"cancellation"`
}

type DTOOrderCustomerCancellation struct{
	Transaction  string `json:"transaction" bson:"transaction"`
	Operation    string `json:"operation" bson:"operation"`
	CustomerProcessed bool `json:"customerProcessed" bson:"customerProcessed"`
	OrderProcessed bool `json:"orderProcessed" bson:"orderProcessed"`
	OrderError string `json:"orderError" bson:"orderError"`
	CustomerError string `json:"customerError" bson:"customerError"`
	CancellationProcessed bool `json:"cancellationProcessed" bson:"cancellationProcessed"`
	CancellationError string `json:"cancellationError" bson:"cancellationError"`
    Body AppOrderBody `json:"body" bson:"body"`
}
