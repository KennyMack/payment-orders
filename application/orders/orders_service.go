package orders

import (
	"context"
	"errors"
	"fmt"
	dtoOrders "payment_orders/adapters/dto/orders"
	"strings"
)

type ServiceOrders struct {
	repository IOrdersRepository
}

func NewServiceOrders(repository IOrdersRepository) *ServiceOrders {
	return &ServiceOrders{
		repository: repository,
	}
}

func (instance *ServiceOrders) OpenConnection() error {
	return instance.repository.OpenConnection()
}

func (instance *ServiceOrders) CloseConnection() error {
	return instance.repository.CloseConnection()
}

func (instance *ServiceOrders) Create(ctx context.Context, model *dtoOrders.DTOOrders) (*MOrders, error) {
	dbModel := NewOrderModel(model)

	_, err := dbModel.IsValid()

	if err != nil {
		return nil, err
	}

	if !dbModel.ID.IsZero() {
		return nil, errors.New("Id deve ser nulo para novos clientes")
	}

	newModel, err := instance.repository.Save(ctx, dbModel)

	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceOrders) Change(ctx context.Context, id string, model *dtoOrders.DTOOrders) (*MOrders, error) {
	dbModel := NewOrderModel(model)

	_, err := dbModel.IsValid()

	if err != nil {
		return nil, err
	}

	if dbModel.ID.IsZero() {
		return nil, errors.New("Id deve ser informado")
	}

	oldModel, err := instance.repository.GetById(ctx, dbModel.ID.Hex())

	if oldModel == nil {
		return nil, errors.New( fmt.Sprintf("Nenhum registro encontrado para o id informado: Id: %v", dbModel.ID))
	}
	dbModel.CreatedAt = oldModel.CreatedAt
	dbModel.Status = strings.ToUpper(dbModel.Status)
	dbModel.ChargeStatus = strings.ToUpper(dbModel.ChargeStatus)
	dbModel.PixStatus = strings.ToUpper(dbModel.PixStatus)

	if (len(oldModel.UniqueIdentifier) > 0 ||
		strings.ToUpper(oldModel.UniqueIdentifier) != "AGUARDANDO") &&
		strings.ToUpper(dbModel.UniqueIdentifier) == "AGUARDANDO" {
		dbModel.UniqueIdentifier = oldModel.UniqueIdentifier
	}

	newModel, err := instance.repository.Save(ctx, dbModel)

	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceOrders) UpdateUniqueIdentifier(ctx context.Context, id, uniqueIdentifier string) (*MOrders, error) {
	customerDb, err := instance.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	customerDb.UniqueIdentifier = uniqueIdentifier

	newModel, err := instance.repository.Save(ctx, customerDb)
	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceOrders) UpdateInfo(ctx context.Context, id, status,
	digitalLine, barcodeData, barcodeUrl, secureUrl, secureId string) (*MOrders, error) {
	customerDb, err := instance.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	if len(status) > 0 {
		customerDb.Status = strings.ToUpper(status)
	}
	if len(digitalLine) > 0 {
		customerDb.DigitalLine = digitalLine
	}
	if len(barcodeData) > 0 {
		customerDb.BarcodeData = barcodeData
	}
	if len(barcodeUrl) > 0 {
		customerDb.BarcodeUrl = barcodeUrl
	}
	if len(secureUrl) > 0 {
		customerDb.SecureUrl = secureUrl
	}
	if len(secureId) > 0 {
		customerDb.SecureId = secureId
	}

	newModel, err := instance.repository.Save(ctx, customerDb)
	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceOrders) UpdateTriggerInfo(ctx context.Context, uniqueIdentifier string, info *MOrderTriggerInfo) (*MOrders, error) {
	orderDb, err := instance.repository.GetByUniqueIdentifier(ctx, uniqueIdentifier)

	if err != nil {
		return nil, err
	}

	if len(info.Status) > 0 {
		orderDb.Status = strings.ToUpper(info.Status)
	}

	newModel, err := instance.repository.Save(ctx, orderDb)
	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceOrders) Remove(ctx context.Context, id string) (*MOrders, error) {
	oldModel, err := instance.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	if oldModel == nil {
		return nil, errors.New( fmt.Sprintf("Nenhum registro encontrado para o id informado: Id: %v", id))
	}

	return instance.repository.Remove(ctx, oldModel)
}

func (instance *ServiceOrders) GetById(ctx context.Context, id string) (*MOrders, error) {
	res, err := instance.repository.GetById(ctx, id)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (instance *ServiceOrders) GetByOwnId(ctx context.Context, id string) (*MOrders, error) {
	res, err := instance.repository.GetByOwnId(ctx, id)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (instance *ServiceOrders) GetByUniqueIdentifier(ctx context.Context, uniqueIdentifier string) (*MOrders, error) {
	res, err := instance.repository.GetByUniqueIdentifier(ctx, uniqueIdentifier)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (instance *ServiceOrders) GetNextStatus(currentStatus string) []string {
	status := strings.ToUpper(currentStatus)
	possibleStatus := dtoOrders.NextStatus()

	return possibleStatus[status]
}
