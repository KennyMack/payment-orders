package queue

import (
	"log"
	"time"
)

const (
	CREATE = "CREATE"
	CHANGE = "CHANGE"
	REMOVE = "REMOVE"
)

type ConsumerFactoryInstance struct {
	NewMessage   chan *ContentMessageInstance
	parametersConsumer RabbitMQConfig
	connection IRabbitConnection
	amqpInstanceConsumer *RabbitMQInstance
}

func MakeNewConsumerFactory(rabbitConn IRabbitConnection, config RabbitMQConfig) *ConsumerFactoryInstance {
	return &ConsumerFactoryInstance{
		parametersConsumer: config,
		connection: rabbitConn,
		NewMessage: make(chan *ContentMessageInstance),
	}
}

func (instance *ConsumerFactoryInstance) CreateConsumer(consumerTag string, createDeadLetter bool) error {
	return instance.creator(1, consumerTag, createDeadLetter)
}

func (instance *ConsumerFactoryInstance) creator(generation int, consumerTag string, createDeadLetter bool) error {
	instance.amqpInstanceConsumer = MakeNewRabbitMQConsumer(instance.connection, instance.parametersConsumer)
	err := instance.amqpInstanceConsumer.Consumer(createDeadLetter)
	if err != nil && generation <= 3 {
		time.Sleep(time.Duration(generation*10) * time.Second)
		return instance.creator(generation + 1, consumerTag, createDeadLetter)
	} else if err != nil && generation > 3 {
		log.Panicf("%s: %v", "Não foi possível conectar ao RABBIT", err)
		return err
	}
	return nil
}

func (instance *ConsumerFactoryInstance) Subscribe() {
	if instance.amqpInstanceConsumer != nil {
		go func() {
			for range instance.amqpInstanceConsumer.FailOnQueue {
				log.Printf("Tentando reconectar ao channel do RABBIT")
				var err error
				for i := 0; i < 3; i++ {
					time.Sleep(time.Duration(i+1*10) * time.Second)
					err = instance.connection.OpenConnection()
					if err != nil {
						continue
					}

					err = instance.amqpInstanceConsumer.OpenChannel()
					if err == nil {
						go instance.amqpInstanceConsumer.Subscribe()
						break
					}
				}
				if err != nil {
					log.Panicf("Não foi possível reconectar ao RABBIT, Err: %v", err)
				}
			}
		}()
	}

	if instance.amqpInstanceConsumer != nil {
		go instance.amqpInstanceConsumer.Subscribe()

		go func () {
			for message := range instance.amqpInstanceConsumer.NewMessage {
				instance.NewMessage <- message
			}
		}()
	}
}

func (instance *ConsumerFactoryInstance) Close()  {
	if instance.amqpInstanceConsumer != nil {
		_ = instance.amqpInstanceConsumer.Channel.Close()
		instance.amqpInstanceConsumer.Close()
	}
}
