package payments

import (
	"context"
	"fmt"
	"github.com/kamva/mgm/v3"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"payment_orders/adapters/db"
	"payment_orders/application/payments"
)

type RepositoryCardClientToken struct{
	database db.IMongoDBInstance
	collectionName string
}

func (instance *RepositoryCardClientToken) OpenConnection() error {
	return instance.database.Open()
}

func (instance *RepositoryCardClientToken) CloseConnection() error {
	return instance.database.Close()
}

func NewRepositoryCardClientToken(database db.IMongoDBInstance) *RepositoryCardClientToken {
	return &RepositoryCardClientToken{
		database: database,
		collectionName: "CreditCardTokens",
	}
}

func (instance *RepositoryCardClientToken) GetByCardToken(ctx context.Context, CardToken string, processed bool) (*payments.MCardClientToken, error) {
	var result payments.MCardClientToken

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	cur := collection.FindOne(ctx, bson.M{ "cardToken": CardToken, "processed": processed})

	err := cur.Decode(&result)

	collection = nil
	if err != nil {
		return nil, nil
	}

	if result.ID.IsZero() {
		return nil, nil
	}

	return &result, nil
}

func (instance *RepositoryCardClientToken) GetByCustomerId(ctx context.Context, customerId primitive.ObjectID) ([]*payments.MCardClientToken, error) {
	var results []*payments.MCardClientToken

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	cur, err := collection.Find(ctx, bson.M{ "customerId": customerId, "processed": false})

	if err != nil {
		return nil, err
	}

	for cur.Next(ctx) {
		var el payments.MCardClientToken

		err := cur.Decode(&el)

		if err != nil {
			log.Fatal(err)
		}
		results = append(results, &el)
	}

	err = cur.Close(ctx)

	collection = nil
	if err != nil {
		return results, err
	}

	return results, nil
}

func (instance *RepositoryCardClientToken) GetById(ctx context.Context, id string) (*payments.MCardClientToken, error) {
	result := &payments.MCardClientToken{}

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	_ = collection.FindByIDWithCtx(ctx, id, result)

	collection = nil
	if result.ID.IsZero() {
		return nil, errors.New(fmt.Sprintf("Nenhum registro encontrado para o id: %v", id))
	}

	return result, nil
}

func (instance *RepositoryCardClientToken) Save(ctx context.Context, customer payments.ICardClientToken) (*payments.MCardClientToken, error) {
	if instance.database.GetDbType() == db.MongoDB {
		collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

		newModel := customer.GetModel()

		var err error
		if newModel.ID.IsZero() {
			err = collection.CreateWithCtx(ctx, newModel)
		} else {
			err = collection.UpdateWithCtx(ctx, newModel)
		}
		collection = nil

		if err != nil {
			return nil, err
		}

		return newModel.GetModel(), nil
	}

	return nil, nil
}

func (instance *RepositoryCardClientToken) Remove(ctx context.Context, customer payments.ICardClientToken) (*payments.MCardClientToken, error) {
	if instance.database.GetDbType() == db.MongoDB {
		collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

		newModel := customer.GetModel()

		err := collection.DeleteWithCtx(ctx, newModel)

		collection = nil
		if err != nil {
			return nil, err
		}

		return newModel.GetModel(), nil
	}
	return nil, nil
}
