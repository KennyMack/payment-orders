package validations

import "strconv"

func IsCPF(cpf string) bool {
	valClear := ClearString(cpf, "\\d")

	if len(valClear) != 11 {
		return false
	}

	if valClear == "00000000000" ||
		valClear == "11111111111" ||
		valClear == "22222222222" ||
		valClear == "33333333333" ||
		valClear == "44444444444" ||
		valClear == "55555555555" ||
		valClear == "66666666666" ||
		valClear == "77777777777" ||
		valClear == "88888888888" ||
		valClear == "99999999999" {
		return false
	}

	add := 0
	for i := 0; i < 9; i++ {
		pos, _ := strconv.Atoi(valClear[i:i+1])
		add +=  int(pos) * (10 - i)
	}

	rev := 11 - (add % 11)

	if rev == 10 || rev == 11 {
		rev = 0
	}

	pos, _ := strconv.Atoi(valClear[9:10])

	if rev != int(pos) {
		return false
	}

	add = 0
	for i := 0; i < 10; i++ {
		pos, _ := strconv.Atoi(valClear[i:i+1])
		add += int(pos) * (11 - i)
	}

	rev = 11 - (add % 11)
	if rev == 10 || rev == 11 {
		rev = 0
	}

	pos, _ = strconv.Atoi(valClear[10:11])

	if rev != int(pos) {
		return false
	}

	return true
}
