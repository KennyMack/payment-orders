package customers

import (
	"payment_orders/adapters/queue"
)

type AppCustomersQueue struct {
	NewMessage   chan *queue.ContentMessageInstance
}

func MakeNewAppCustomersQueue() *AppCustomersQueue {
	return &AppCustomersQueue{
		NewMessage: make(chan *queue.ContentMessageInstance),
	}
}

func (instance *AppCustomersQueue) ConnectToAMQP(consumerAmqpConn queue.IRabbitConnection) error {
	config := queue.MakeCustomersRabbitMQCfg()
	consumer := queue.MakeNewConsumerFactory(consumerAmqpConn, config)
	err := consumer.CreateConsumer(config.ConsumerTag, false)
	if err != nil {
		return err
	}

	go consumer.Subscribe()

	go func() {
		for message := range consumer.NewMessage {
			instance.NewMessage <- message
			message = nil
		}
	}()

	return nil
}
