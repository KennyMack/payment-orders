package request

import (
	"context"
	"github.com/monaco-io/request"
)

func Get(ctx context.Context, url string, header map[string]string) (interface{}, error) {
	var result interface{}
	c := request.Client{
		Context: ctx,
		URL: url,
		Method: "GET",
		Header: make(map[string]string),
	}

	if header != nil {
		c.Header = header
	}

	if _, ok := c.Header["Content-type"]; !ok {
		c.Header["Content-type"] = "application/json; charset=UTF-8"
	}

	resp := c.Send().Scan(&result)
	if !resp.OK(){
		return result, resp.Error()
	}
	return result, nil
}

func Post(ctx context.Context, url string,
	body []byte,
	header map[string]string) (interface{}, error) {
	var result interface{}
	c := request.Client{
		Context: ctx,
		Method: "POST",
		Header: make(map[string]string),
		URL: url,
		JSON: body,
	}

	if header != nil {
		c.Header = header
	}

	if _, ok := c.Header["Content-type"]; !ok {
		c.Header["Content-type"] = "application/json; charset=UTF-8"
	}

	resp := c.Send().Scan(&result)
	if !resp.OK(){
		return result, resp.Error()
	}
	return result, nil
}

func Put(ctx context.Context, url string,
	body []byte,
	header map[string]string) (interface{}, error) {
	var result interface{}
	c := request.Client{
		Context: ctx,
		Method: "PUT",
		Header: make(map[string]string),
		URL: url,
		JSON: body,
	}

	if header != nil {
		c.Header = header
	}

	if _, ok := c.Header["Content-type"]; !ok {
		c.Header["Content-type"] = "application/json;"
	}

	resp := c.Send().Scan(&result)
	if !resp.OK(){
		return result, resp.Error()
	}
	return result, nil
}

func Delete(ctx context.Context, url string, header map[string]string) (interface{}, error) {
	var result interface{}
	c := request.Client{
		Context: ctx,
		Method: "DELETE",
		Header: make(map[string]string),
		URL: url,
	}

	if header != nil {
		c.Header = header
	}

	if _, ok := c.Header["Content-type"]; !ok {
		c.Header["Content-type"] = "application/json; charset=UTF-8"
	}

	resp := c.Send().Scan(&result)
	if !resp.OK(){
		return result, resp.Error()
	}
	return result, nil
}
