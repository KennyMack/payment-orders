package utils

func FindIndex(items int, predicate func(i int) bool) int {
    for i := 0; i < items; i++ {
        if predicate(i) {
            return i
        }
    }
    return -1
}

/*
func FindIndex(data []interface{}, predicate func(i interface{}) bool) int {
	for i := 0; i < len(data); i++ {
		if predicate(data[i]) {
			return i
		}
	}
    return -1
}
*/
