package baseinterfaces

type IBaseService interface {
	OpenConnection() error
	CloseConnection() error
}

