package db

import (
	"github.com/pkg/errors"
	"log"
	"time"
)

type DataBaseType = int

const (
	MongoDB DataBaseType = iota
)

type DataBaseFactoryInstance struct {
	DbType DataBaseType
	DataBaseMongoDB *MongoDBInstance
}

func MakeNewDatabaseFactory(dbType DataBaseType) *DataBaseFactoryInstance {
	if dbType != MongoDB {
		log.Panic("O banco de dados informado não foi implementado")
	}

	return &DataBaseFactoryInstance{
		DbType: dbType,
	}
}

func (instance *DataBaseFactoryInstance) creator(generation int) error {
	if instance.DbType == MongoDB {
		instance.DataBaseMongoDB = NewMongoDB()
		err := instance.DataBaseMongoDB.Open()
		if err != nil && generation <= 3 {
			log.Printf("Não foi possível conectar, reconectando novamente em: %v: Err %s", time.Duration(generation*10) * time.Second, err)
			time.Sleep(time.Duration(generation*10) * time.Second)
			return instance.creator(generation + 1)
		} else if err != nil && generation > 3 {
			log.Panicf("%s: %v", "Não foi possível conectar ao banco de dados", err)
			return err
		}
	} else {
		log.Panic("O banco de dados informado não foi implementado")
	}
	// log.Print("Conectado ao banco de dados com sucesso!")
	return nil
}

func (instance *DataBaseFactoryInstance) Open() error {
	return instance.creator(1)
}

func (instance *DataBaseFactoryInstance) Close() error {
	if instance.DbType == MongoDB {
		log.Printf("Desconectado do banco de dados com sucesso! %v",  &instance.DataBaseMongoDB)
		return instance.DataBaseMongoDB.Close()
	}

	return errors.New("O banco de dados informado não foi implementado")
}
