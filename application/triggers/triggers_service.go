package triggers

import (
	"context"
	"errors"
)

type ServiceTriggers struct {
	repository ITriggerRepository
}

func NewServiceTriggers(repository ITriggerRepository) *ServiceTriggers {
	return &ServiceTriggers{
		repository: repository,
	}
}

func (instance *ServiceTriggers) OpenConnection() error {
	return instance.repository.OpenConnection()
}

func (instance *ServiceTriggers) CloseConnection() error {
	return instance.repository.CloseConnection()
}

func (instance *ServiceTriggers) Create(ctx context.Context, model *MTriggerData) (*MTriggerData, error) {
	dbModel := NewTriggerDataModel(model)

	_, err := dbModel.IsValid()

	if err != nil {
		return nil, err
	}

	if !dbModel.ID.IsZero() {
		return nil, errors.New("id deve ser nulo para novos eventos")
	}

	newModel, err := instance.repository.Save(ctx, dbModel)

	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceTriggers) UpdateToProcessed(ctx context.Context, model *MTriggerData) (*MTriggerData, error) {
	model.Processed = true

	newModel, err := instance.repository.Save(ctx, model)

	if err != nil {
		return nil, err
	}

	return newModel, nil
}

func (instance *ServiceTriggers) GetByControlId(ctx context.Context, id string) (*MTriggerData, error) {
	res, err := instance.repository.GetByControlId(ctx, id)

	if err != nil {
		return nil, err
	}

	return res, nil
}
