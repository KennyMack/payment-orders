package cancellations

import (
	"context"
	"fmt"
	"github.com/kamva/mgm/v3"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"payment_orders/adapters/db"
	"payment_orders/application/cancellations"
)

type RepositoryOrdersCancellation struct{
	database db.IMongoDBInstance
	collectionName string
}

func (instance *RepositoryOrdersCancellation) OpenConnection() error {
	return instance.database.Open()
}

func (instance *RepositoryOrdersCancellation) CloseConnection() error {
	return instance.database.Close()
}

func NewRepositoryOrdersCancellation(database db.IMongoDBInstance) *RepositoryOrdersCancellation {
	return &RepositoryOrdersCancellation{
		database: database,
		collectionName: "OrdersCancellation",
	}
}

func (instance *RepositoryOrdersCancellation) GetById(ctx context.Context, id string) (*cancellations.MOrdersCancellation, error) {
	result := &cancellations.MOrdersCancellation{}

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	_ = collection.FindByIDWithCtx(ctx, id, result)

	collection = nil
	if result.ID.IsZero() {
		return nil, errors.New(fmt.Sprintf("Nenhum registro encontrado para o id: %v", id))
	}

	return result, nil
}

func (instance *RepositoryOrdersCancellation) GetByUniqueIdentifier(ctx context.Context, uniqueIdentifier string, processed bool) (*cancellations.MOrdersCancellation, error) {
	var result cancellations.MOrdersCancellation

	collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)
	cur := collection.FindOne(ctx, bson.M{ "uniqueIdentifier": uniqueIdentifier, "processed": processed })

	err := cur.Decode(&result)

	collection = nil
	if err != nil {
		return nil, nil
	}

	if result.ID.IsZero() {
		return nil, nil
	}

	return &result, nil
}

func (instance *RepositoryOrdersCancellation) Save(ctx context.Context, orderToCancel cancellations.IOrdersCancellation) (*cancellations.MOrdersCancellation, error) {
	if instance.database.GetDbType() == db.MongoDB {
		collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

		newModel := orderToCancel.GetModel()

		var err error
		if newModel.ID.IsZero() {
			err = collection.CreateWithCtx(ctx, newModel)
		} else {
			err = collection.UpdateWithCtx(ctx, newModel)
		}
		collection = nil

		if err != nil {
			return nil, err
		}

		return newModel.GetModel(), nil
	}

	return nil, nil
}

func (instance *RepositoryOrdersCancellation) Remove(ctx context.Context, orderToCancel cancellations.IOrdersCancellation) (*cancellations.MOrdersCancellation, error) {
	if instance.database.GetDbType() == db.MongoDB {
		collection := mgm.NewCollection(instance.database.GetDataBase(), instance.collectionName)

		newModel := orderToCancel.GetModel()

		err := collection.DeleteWithCtx(ctx, newModel)

		collection = nil
		if err != nil {
			return nil, err
		}

		return newModel.GetModel(), nil
	}
	return nil, nil
}
