package payments

import (
	"context"
	"errors"
	validator "github.com/asaskevich/govalidator"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"payment_orders/adapters/db"
	"payment_orders/adapters/dto/payments"
	baseRepository "payment_orders/application/baseinterfaces"
)

func init () {
	validator.SetFieldsRequiredByDefault(true)
}

func NewCardClientTokenModel(dtoModel *payments.DTOCardClientToken) *MCardClientToken {
	creditCard := MCardClientToken{
		SetAsDefault: false,
		Description: "Novo Cartão",
		Remove: false,
	}

	if dtoModel != nil{
		creditCard.DefaultModel = dtoModel.DefaultModel
		creditCard.SetAsDefault = dtoModel.SetAsDefault
		creditCard.Description = dtoModel.Description
		creditCard.CardToken = dtoModel.CardToken
		creditCard.CustomerId = dtoModel.CustomerId
		creditCard.Remove = dtoModel.Remove
		creditCard.Processed = dtoModel.Processed
	}

	return &creditCard
}

type MCardClientToken struct {
	db.DefaultModel `valid:"-"bson:",inline"`
	CustomerId primitive.ObjectID `bson:"customerId" valid:"optional"`
	CardToken    string `bson:"cardToken" valid:"required~Token do cartão é obrigatório"`
	Description  string `bson:"description" valid:"required~Descrição é obrigatória"`
	SetAsDefault bool `bson:"setAsDefault" valid:"optional"`
	Remove bool `bson:"remove" valid:"optional"`
	Processed bool `bson:"processed" valid:"optional"`
}

type ICardClientToken interface {
	IsValid() (bool, error)
	GetModel() *MCardClientToken
	GetDTO() *payments.DTOCardClientToken
}

type ICardClientTokenReader interface{
	GetById(ctx context.Context, id string) (*MCardClientToken, error)
	GetByCardToken(ctx context.Context, CardToken string, processed bool) (*MCardClientToken, error)
	GetByCustomerId(ctx context.Context, CustomerId primitive.ObjectID) ([]*MCardClientToken, error)
}

type ICardClientTokenWriter interface {
	Save(ctx context.Context, card ICardClientToken) (*MCardClientToken, error)
	Remove(ctx context.Context, card ICardClientToken) (*MCardClientToken, error)
}

type ICardClientTokenRepository interface {
	baseRepository.IBaseRepository
	ICardClientTokenReader
	ICardClientTokenWriter
}

type ICardClientTokenService interface {
	baseRepository.IBaseService
	Create(ctx context.Context, model *payments.DTOCardClientToken) (*MCardClientToken, error)
	Change(ctx context.Context, id string, model *payments.DTOCardClientToken) (*MCardClientToken, error)
	Remove(ctx context.Context, id string) (*MCardClientToken, error)
	GetById(ctx context.Context, id string) (*MCardClientToken, error)
	GetByCardToken(ctx context.Context, CardToken string, processed bool) (*MCardClientToken, error)
	GetByCustomerId(ctx context.Context, CustomerId primitive.ObjectID) ([]*MCardClientToken, error)
	UpdateToProcessed(ctx context.Context, id string) (*MCardClientToken, error)
}

func (createCard *MCardClientToken) IsValid() (bool, error) {
	_, err := validator.ValidateStruct(createCard)
	if err != nil {
		return false, err
	}

	if createCard.CustomerId.IsZero()  {
		return false, errors.New("Código do cliente obrigatório")
	}

	return true, nil
}

func (createCard *MCardClientToken) GetModel() *MCardClientToken {
	return createCard
}

func (createCard *MCardClientToken) GetDTO() *payments.DTOCardClientToken {
	dto := payments.DTOCardClientToken{
		DefaultModel: createCard.DefaultModel,
		CustomerId: createCard.CustomerId,
		CardToken: createCard.CardToken,
		Description: createCard.Description,
		SetAsDefault: createCard.SetAsDefault,
		Remove: createCard.Remove,
		Processed: createCard.Processed,
	}

	return &dto
}
