package sync

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	dtoCustomers "payment_orders/adapters/dto/customers"
	dtoOrders "payment_orders/adapters/dto/orders"
	dtoCancellation "payment_orders/adapters/dto/cancellations"
	"payment_orders/adapters/payment"
	"payment_orders/adapters/queue"
	"payment_orders/application/cancellations"
	"payment_orders/application/customers"
	"payment_orders/application/orders"
	cardToken "payment_orders/application/payments"
	"payment_orders/application/transactions"
	"payment_orders/utils"
	"payment_orders/utils/environment"
	"strings"
)

type MSyncErrorItem struct{
	Email []string `json:"email"`
	Name []string `json:"name"`
	PhonePrefix []string `json:"phone_prefix"`
	Phone []string `json:"phone"`
	CpfCnpj []string `json:"cpf_cnpj"`
	ZipCode []string `json:"zip_code"`
	Number []string `json:"number"`
	Street []string `json:"street"`
	District []string `json:"district"`
	DueDate string `json:"due_date"`
}

type MSyncErrorMessage struct {
	Errors string `json:"errors"`
}

type MSyncErrors struct {
	Errors     MSyncErrorItem
}

type MOrderBankSlip struct {
	DigitableLine string `json:"digitable_line"`
	BarcodeData string `json:"barcode_data"`
	Barcode string `json:"barcode"`
}

type MOrderCreditCardTransaction struct {
	Status string `json:"status"`
	Token string `json:"token"`
	LR string `json:"lr"`
	Message string `json:"message"`
}

type MOrderPixTransaction struct {
	QrCode string `json:"qrcode"`
	QrCodeText string `json:"qrcode_text"`
	Status string `json:"status"`
	PayerCpfCnpj string `json:"payer_cpf_cnpj"`
	PayerName string `json:"payer_name"`
	EndToEndId string `json:"end_to_end_id"`
	EndToEndRefundId string `json:"end_to_end_refund_id"`
}

type AppSync struct {
	publishAmqpConn queue.IRabbitConnection
	serviceCustomer customers.ICustomersService
	serviceOrder orders.IOrdersService
	serviceReturnCodes orders.IReturnCodeService
	serviceCardClientToken cardToken.ICardClientTokenService
	serviceOrdersCancellation cancellations.IOrdersCancellationService
	serviceTransaction transactions.ITransactionsService
	iuguCustomer payment.IUGUCustomers
	iuguPayment payment.IUGUPayment
	iuguInvoice payment.IuguInvoicesService
}

func NewAppSync(
	publishAmqpConn queue.IRabbitConnection,
	serviceCustomer customers.ICustomersService,
	serviceOrder orders.IOrdersService,
	serviceReturnCodes orders.IReturnCodeService,
	serviceCardClientToken cardToken.ICardClientTokenService,
	serviceOrdersCancellation cancellations.IOrdersCancellationService,
	serviceTransaction transactions.ITransactionsService) *AppSync {
	return &AppSync{
		publishAmqpConn: publishAmqpConn,
		serviceCustomer: serviceCustomer,
		serviceTransaction: serviceTransaction,
		serviceCardClientToken: serviceCardClientToken,
		serviceReturnCodes: serviceReturnCodes,
		serviceOrdersCancellation: serviceOrdersCancellation,
		serviceOrder: serviceOrder,
		iuguCustomer: *payment.NewIUGUCustomers(),
		iuguPayment: *payment.NewIUGUPayment(),
		iuguInvoice: *payment.NewIUGUInvoices(),
	}
}

func (instance *AppSync) NewMessage(message *queue.ContentMessageInstance) {
	messageApp := dtoOrders.DTOOrderCustomerCancellation{}

	errParse := json.Unmarshal(message.Body, &messageApp)
	if errParse != nil {
		log.Printf("Falha ao converter o JSON. err: %v", errParse)
		log.Printf("Body: %v ", string(message.Body))
		_ = message.Message.Nack(false, false)
		return
	}

	transactionId := messageApp.Transaction
	operation := messageApp.Operation
	customerProcessed := messageApp.CustomerProcessed
	orderProcessed := messageApp.OrderProcessed
	cancellationProcessed := messageApp.CancellationProcessed

	log.Printf("Sincronizando a transação: %v", transactionId)

	ctx := context.Background()

	transactionDb, err := instance.serviceTransaction.GetByTransactionId(ctx, transactionId, operation, false)

	if err != nil {
		transactionDb, err = instance.serviceTransaction.GetByTransactionId(ctx, transactionId, operation, true)
		if transactionDb != nil {
			_ = message.Message.Ack(false)
			err = errors.New("transação já processada")
		}
	}

	if err != nil {
		msgErr := fmt.Sprintf("Não foi possível sincronizar a transação %s. err: %v", transactionId, err)

		log.Printf(msgErr)
	}

	var ackMessage bool
	if err == nil &&
		!transactionDb.SentToServer &&
		!transactionDb.CustomerId.IsZero() &&
		!customerProcessed &&
		transactionDb.Operation != queue.REMOVE {
		ackMessage, err = instance.syncCustomerToServer(ctx, transactionDb.CustomerId)
		instance.AddErrorToTransaction(ctx, transactionDb, err)
		customerHookDb, errFind := instance.serviceCustomer.GetById(ctx, transactionDb.CustomerId.Hex())

		if errFind == nil {
			messageApp.Body.Customer.UniqueIdentifier = customerHookDb.UniqueIdentifier
			messageApp.Body.Order.CustomerId = customerHookDb.UniqueIdentifier
			messageApp.CustomerProcessed = true
			customerProcessed = true
			messageApp.CustomerError = ""
			if err != nil {
				messageApp.CustomerError = utils.ScapeChars(err.Error())
			}
			s, _ := json.Marshal(messageApp)
			_ = instance.publishMessageToOrder(ctx, string(s))

			log.Printf("Webhook - Customer %s - UniqueIdentifier %s", customerHookDb.ID.Hex(), customerHookDb.UniqueIdentifier)
			jsonCustomerDb, errFind := json.Marshal(customerHookDb.GetDTO())

			if errFind == nil {
				instance.sendToWebHook(ctx, "CUSTOMER.REGISTER", string(jsonCustomerDb), err)
			}
		}
	}

	if err == nil &&
		!transactionDb.SentToServer &&
		!transactionDb.OrderId.IsZero() &&
		!orderProcessed &&
		transactionDb.Operation != queue.REMOVE {
		ackMessage, err = instance.processOrder(ctx, transactionDb.OrderId)
		instance.AddErrorToTransaction(ctx, transactionDb, err)
		orderHookDb, errFind := instance.serviceOrder.GetById(ctx, transactionDb.OrderId.Hex())

		if errFind == nil {
			messageApp.Body.Order.UniqueIdentifier = orderHookDb.UniqueIdentifier
			messageApp.OrderProcessed = true
			orderProcessed = true
			messageApp.OrderError = ""
			if err != nil {
				messageApp.OrderError = utils.ScapeChars(err.Error())
			}
			s, _ := json.Marshal(messageApp)
			_ = instance.publishMessageToOrder(ctx, string(s))

			log.Printf("Webhook - Order %s - UniqueIdentifier %s", orderHookDb.ID.Hex(), orderHookDb.UniqueIdentifier)
			jsonHook, errFind := json.Marshal(orderHookDb.GetDTO())

			if errFind == nil {
				instance.sendToWebHook(ctx, "ORDER.REGISTER", string(jsonHook), err)
			}
		}
	}

	if err == nil &&
		!transactionDb.SentToServer &&
		!transactionDb.CancellationId.IsZero() &&
		!cancellationProcessed &&
		transactionDb.Operation != queue.REMOVE {
		ackMessage, err = instance.processCancellation(ctx, transactionDb.CancellationId)
		instance.AddErrorToTransaction(ctx, transactionDb, err)
		cancellationDb, errFind := instance.serviceOrdersCancellation.GetById(ctx, transactionDb.CancellationId.Hex())

		if errFind == nil {
			messageApp.Body.Cancellation.UniqueIdentifier = cancellationDb.UniqueIdentifier
			messageApp.Body.Cancellation.CurrentStatus = cancellationDb.CurrentStatus
			messageApp.Body.Cancellation.FinalStatus = cancellationDb.FinalStatus
			messageApp.Body.Cancellation.CancellationId = cancellationDb.CancellationId
			messageApp.Body.Cancellation.Refunded = cancellationDb.Refunded
			messageApp.Body.Cancellation.Canceled = cancellationDb.Canceled
			messageApp.Body.Cancellation.Error = cancellationDb.Error

			messageApp.CancellationProcessed = true
			cancellationProcessed = true
			messageApp.CancellationError = ""
			if err != nil {
				messageApp.CancellationError = utils.ScapeChars(err.Error())
			}
			s, _ := json.Marshal(messageApp)
			_ = instance.publishMessageToOrder(ctx, string(s))

			log.Printf("Webhook - Order Cancellation %s - UniqueIdentifier %s", cancellationDb.ID.Hex(), cancellationDb.UniqueIdentifier)
			jsonHook, errFind := json.Marshal(cancellationDb.GetDTO())

			if errFind == nil {
				eventName := "ORDER.CANCELED"
				if cancellationDb.Refunded {
					eventName = "ORDER.REFUNDED"
				}

				instance.sendToWebHook(ctx, eventName, string(jsonHook), err)
			}
		}
	}

	if err == nil || ackMessage {
		if (customerProcessed && orderProcessed) || cancellationProcessed {
			_, _ = instance.serviceTransaction.UpdateToSent(ctx, transactionId, operation)
			_, _ = instance.serviceTransaction.UpdateToCompleted(ctx, transactionId, operation)
		}
	}

	if err == nil || ackMessage {
		_ = message.Message.Ack(false)
	} else {
		_ = message.Message.Nack(false, !(message.Message.DeliveryTag >= message.DeliveryLimit))
	}
}

func (instance *AppSync) sendToWebHook(ctx context.Context, event, message string, err error) {
	errText := ""
	if err != nil {
		errText = utils.ScapeChars(err.Error())
	}
	s := fmt.Sprintf(`{"event": "%v", "resource": %v, "error": "%v" }`, event, message, errText)

	_ =  instance.PublishMessage(ctx, s)
}

func (instance *AppSync) AddErrorToTransaction(
	ctx context.Context,
	transaction *transactions.MTransactions, err error) {
	if err != nil {
		msgErr := fmt.Sprintf("Não foi possível sincronizar %s. err: %v", transaction.TransactionId, err)

		log.Printf(msgErr)
		_, _ = instance.serviceTransaction.AddMessageError(ctx, transaction.TransactionId, transaction.Operation, msgErr)
		return
	}
}

func (instance *AppSync) PublishMessage(ctx context.Context, message string) error {
	configProducer := queue.MakeWebHookRabbitMQCfg()
	producer := queue.MakeNewProducerFactory(instance.publishAmqpConn, configProducer)
	err := producer.CreateProducer(false)

	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}
	producer.PublishStringMessage(message)

	_ = instance.publishAmqpConn.CloseConnection()
	return nil
}

func (instance *AppSync) publishMessageToOrder(ctx context.Context, message string) error {
	configProducer := queue.MakeOrdersCustomerRabbitMQCfg()
	producer := queue.MakeNewProducerFactory(instance.publishAmqpConn, configProducer)
	err := producer.CreateProducer(false)

	if err != nil {
		log.Printf("Problema ao publicar a mensagem. err: %v", err)
		return err
	}
	producer.PublishStringMessage(message)

	_ = instance.publishAmqpConn.CloseConnection()
	return nil
}

func (instance *AppSync) syncCardTokenUserToServer(ctx context.Context, customerId, customerUniqueIdentifier string, tokens []*cardToken.MCardClientToken) error {
	for i := 0; i < len(tokens);i++ {
		iuguCardObj := payment.IuguPaymentMethodBody{
			Description:  tokens[i].Description,
			Token:        tokens[i].CardToken,
			SetAsDefault: tokens[i].SetAsDefault,
		}

		_, _ = instance.serviceCardClientToken.UpdateToProcessed(ctx, tokens[i].ID.Hex())
		if !tokens[i].Remove {
			var result interface{}
			result, err := instance.iuguPayment.CreatePaymentMethod(ctx, customerUniqueIdentifier, iuguCardObj)

			if err != nil {
				return err
			}

			cardResult := result.(map[string]interface{})

			if _, ok := cardResult["id"]; ok {
				dataCardResult := cardResult["data"].(map[string]interface{})
				displayNumber := fmt.Sprint(dataCardResult["display_number"])

				cardNew := dtoCustomers.DTOCustomerCreditCard{
					IdPaymentToken: fmt.Sprint(cardResult["id"]),
					Description:    fmt.Sprint(cardResult["description"]),
					SetAsDefault:   false,
					DisplayNumber:  displayNumber,
					Test:           false,
					Month:          fmt.Sprint(dataCardResult["month"]),
					Year:           fmt.Sprint(dataCardResult["year"]),
					Brand:          fmt.Sprint(dataCardResult["brand"]),
					HolderName:     fmt.Sprint(dataCardResult["holder_name"]),
					First6:         fmt.Sprint(dataCardResult["bin"]),
					Last4:          displayNumber[len(displayNumber) - 4:],
				}

				_, _ = instance.serviceCustomer.AddCardToCustomer(ctx, customerId, &cardNew)
			}
		} else {
			var result interface{}
			result, err := instance.iuguPayment.RemovePaymentMethod(ctx, customerUniqueIdentifier, tokens[i].CardToken)

			if err != nil {
				return err
			}

			cardResult := result.(map[string]interface{})

			if _, ok := cardResult["id"]; ok {
				_, _ = instance.serviceCustomer.RemoveCardFromCustomer(ctx, customerId, tokens[i].CardToken)
			}
		}
	}

	return nil
}

func (instance *AppSync) syncCustomerToServer(ctx context.Context, customerId primitive.ObjectID) (bool, error) {
	customerDb, err := instance.serviceCustomer.GetById(ctx, customerId.Hex())

	if err != nil {
		return false, err
	}

	cardCustomerTokenList, _ := instance.serviceCardClientToken.GetByCustomerId(ctx, customerId)

	log.Printf("Customer %s", customerDb.ID.Hex())

	iuguCustomerObj := payment.IuguClientBody{}
	iuguCustomerObj.CustomVariables = []payment.IuguCustomVariable{}

	iuguCustomerObj.CustomVariables = append(iuguCustomerObj.CustomVariables,
		payment.IuguCustomVariable{
		Name: "OwnId",
		Value: customerDb.ID.Hex(),
	})

	iuguCustomerObj.CpfCnpj = utils.OnlyNumbers(customerDb.NumberDoc)
	iuguCustomerObj.Email = customerDb.Email
	iuguCustomerObj.Name = customerDb.FullName
	iuguCustomerObj.Notes = ""
	iuguCustomerObj.CcEmails = ""

	if customerDb.Address != nil {
		if len(customerDb.Address.PhoneNumber) > 0 {
			phoneNumber := utils.OnlyNumbers(customerDb.Address.PhoneNumber)

			iuguCustomerObj.PhonePrefix = phoneNumber[:2]
			iuguCustomerObj.Phone = phoneNumber[2:]
		}
		iuguCustomerObj.ZipCode = utils.OnlyNumbers(customerDb.Address.ZipCode)
		iuguCustomerObj.Street = customerDb.Address.Street
		iuguCustomerObj.Complement = customerDb.Address.Complement
		iuguCustomerObj.District = customerDb.Address.Neighborhood
		iuguCustomerObj.City = customerDb.Address.City
		iuguCustomerObj.State = customerDb.Address.State
		iuguCustomerObj.Number = customerDb.Address.Number
	}

	var result interface{}

	if len(customerDb.UniqueIdentifier) <= 0 ||
		strings.ToUpper(customerDb.UniqueIdentifier) == "AGUARDANDO" {
		result, err = instance.iuguCustomer.CreateCustomer(ctx, iuguCustomerObj)
	} else {
		result, err = instance.iuguCustomer.ChangeCustomer(ctx, customerDb.UniqueIdentifier, iuguCustomerObj)
	}

	if err != nil {
		return false, err
	}

	customerResult := result.(map[string]interface{})

	if _, ok := customerResult["errors"]; ok {
		log.Printf("Error: %v", customerResult["errors"])

		errorMessage := make([]string, 0)
		byteData, _ := json.Marshal(customerResult["errors"])

		syncErrors := MSyncErrorItem{}
		errParse := json.Unmarshal(byteData, &syncErrors)

		if errParse == nil {
			if len(syncErrors.Email) > 0 {
				errorMessage = append(errorMessage, fmt.Sprintf("email: %s", strings.Join(syncErrors.Email, ",")))
			}
			if len(syncErrors.Name) > 0 {
				errorMessage = append(errorMessage, fmt.Sprintf("name: %s", strings.Join(syncErrors.Name, ",")))
			}
			if len(syncErrors.PhonePrefix) > 0 {
				errorMessage = append(errorMessage, fmt.Sprintf("phonePrefix: %s", strings.Join(syncErrors.PhonePrefix, ",")))
			}
			if len(syncErrors.Phone) > 0 {
				errorMessage = append(errorMessage, fmt.Sprintf("phone: %s", strings.Join(syncErrors.Phone, ",")))
			}
			if len(syncErrors.CpfCnpj) > 0 {
				errorMessage = append(errorMessage, fmt.Sprintf("cpfCnpj: %s", strings.Join(syncErrors.CpfCnpj, ",")))
			}
			if len(syncErrors.ZipCode) > 0 {
				errorMessage = append(errorMessage, fmt.Sprintf("zipcode: %s", strings.Join(syncErrors.ZipCode, ",")))
			}
			if len(syncErrors.Number) > 0 {
				errorMessage = append(errorMessage, fmt.Sprintf("number: %s", strings.Join(syncErrors.Number, ",")))
			}
			if len(syncErrors.Street) > 0 {
				errorMessage = append(errorMessage, fmt.Sprintf("street: %s", strings.Join(syncErrors.Street, ",")))
			}
			if len(syncErrors.District) > 0 {
				errorMessage = append(errorMessage, fmt.Sprintf("district: %s", strings.Join(syncErrors.District, ",")))
			}
		} else {
			syncError := MSyncErrorMessage{}
			byteData, _ = json.Marshal(customerResult)
			errParse = json.Unmarshal(byteData, &syncError)

			if errParse != nil {
				return false, errParse
			}

			errorMessage = append(errorMessage, fmt.Sprintf("customer: %s", syncError.Errors))
		}

		return true, errors.New(strings.Join(errorMessage, " "))
	}

	if len(customerDb.UniqueIdentifier) <= 0 ||
		strings.ToUpper(customerDb.UniqueIdentifier) == "AGUARDANDO" {
		log.Printf("Customer %s - UniqueIdentifier %s", customerDb.ID.Hex(), fmt.Sprint(customerResult["id"]))
		_, err = instance.serviceCustomer.UpdateUniqueIdentifier(ctx, customerDb.ID.Hex(), fmt.Sprint(customerResult["id"]))
	}
	if err != nil {
		return true, err
	}

	if len(cardCustomerTokenList) > 0 {
		log.Printf("Customer %s - Cartoes %v", customerDb.ID.Hex(), len(cardCustomerTokenList))
		_ = instance.syncCardTokenUserToServer(ctx, customerDb.ID.Hex(), fmt.Sprint(customerResult["id"]), cardCustomerTokenList)
	}

	return true, nil
}

func (instance *AppSync) processOrder(ctx context.Context, orderId primitive.ObjectID) (bool, error) {
	orderDb, err := instance.serviceOrder.GetById(ctx, orderId.Hex())

	if err != nil {
		return false, errors.New(fmt.Sprintf("%s %s", "Find-order: ", err.Error()))
	}

	log.Printf("Sync Order %s to IUGU", orderDb.ID.Hex())

	iuguInvoice := instance.createIuguInvoiceObj(orderDb.GetDTO())
	var orderOlderResult map[string]interface{}

	if strings.ToUpper(orderDb.UniqueIdentifier) != "AGUARDANDO" &&
		len(orderDb.UniqueIdentifier) > 0 {
		orderOlderResult, _ = instance.getInvoiceIuguById(ctx, orderDb.UniqueIdentifier)

		if _, ok := orderOlderResult["errors"]; !ok {
			if orderOlderResult["status"] == "canceled" ||
				orderOlderResult["status"] == "refunded" ||
				orderOlderResult["status"] == "expired" ||
				orderOlderResult["status"] == "chargeback" {
				orderDb.UniqueIdentifier = "AGUARDANDO"
				orderDb.DigitalLine = ""
				orderDb.BarcodeData = ""
				orderDb.BarcodeUrl = ""
				orderDb.SecureUrl = ""
				orderDb.SecureId = ""
				orderDb.ChargePdf = ""
				orderDb.ChargeMessage = ""
				orderDb.ChargeStatus = ""
				orderDb.ChargeToken = ""
				orderDb.ChargeSuccess = false
				orderDb.ChargeInfoMessage = ""
			}
			orderBankSlip := MOrderBankSlip{}
			orderDb.Status = fmt.Sprint(orderOlderResult["status"])
			orderDb.UniqueIdentifier = fmt.Sprint(orderOlderResult["id"])
			orderDb.SecureUrl = fmt.Sprint(orderOlderResult["secure_url"])
			orderDb.SecureId = fmt.Sprint(orderOlderResult["secure_id"])

			if orderOlderResult["bank_slip"] != nil {
				byteData, _ := json.Marshal(orderOlderResult["bank_slip"])
				_ = json.Unmarshal(byteData, &orderBankSlip)

				orderDb.DigitalLine = orderBankSlip.DigitableLine
				orderDb.BarcodeData = orderBankSlip.BarcodeData
				orderDb.BarcodeUrl = orderBankSlip.Barcode
			}

			orderCreditCardTransaction := MOrderCreditCardTransaction{}

			if orderOlderResult["credit_card_transaction"] != nil {
				byteData, _ := json.Marshal(orderOlderResult["credit_card_transaction"])
				_ = json.Unmarshal(byteData, &orderCreditCardTransaction)

				orderDb.ChargeStatus = orderCreditCardTransaction.Status
				orderDb.ChargeToken = orderCreditCardTransaction.Token
			}

			orderPixTransaction :=MOrderPixTransaction{}

			if orderOlderResult["pix"] != nil {
				byteData, _ := json.Marshal(orderOlderResult["pix"])
				_ = json.Unmarshal(byteData, &orderPixTransaction)

				orderDb.PixStatus = orderPixTransaction.Status
				orderDb.PixQrCode = orderPixTransaction.QrCode
				orderDb.PixQrCodeText = orderPixTransaction.QrCodeText
				orderDb.PixPayerCpfCnpj = orderPixTransaction.PayerCpfCnpj
				orderDb.PixPayerName = orderPixTransaction.PayerName
				orderDb.PixEndToEndId = orderPixTransaction.EndToEndId
				orderDb.PixEndToEndRefundId = orderPixTransaction.EndToEndRefundId
			}

		}
	}

	if orderDb.UniqueIdentifier == "AGUARDANDO" ||
		len(orderDb.UniqueIdentifier) <= 0 {
		var result interface{}

		result, err = instance.iuguInvoice.CreateInvoice(ctx, iuguInvoice)
		orderResult := result.(map[string]interface{})

		if _, ok := orderResult["errors"]; ok {
			log.Printf("Error: %v", orderResult["errors"])
			byteData, _ := json.Marshal(orderResult["errors"])

			err = errors.New(fmt.Sprintf("order: %s", string(byteData)))
		} else if !ok {
			orderDb.UniqueIdentifier = fmt.Sprint(orderResult["id"])
			orderDb.Status = fmt.Sprint(orderResult["status"])
			orderDb.SecureUrl = fmt.Sprint(orderResult["secure_url"])
			orderDb.SecureId = fmt.Sprint(orderResult["secure_id"])

			orderBankSlip := MOrderBankSlip{}
			if orderResult["bank_slip"] != nil {
				byteData, _ := json.Marshal(orderResult["bank_slip"])
				_ = json.Unmarshal(byteData, &orderBankSlip)
				orderDb.DigitalLine = orderBankSlip.DigitableLine
				orderDb.BarcodeData = orderBankSlip.BarcodeData
				orderDb.BarcodeUrl = orderBankSlip.Barcode
			}

			orderPix := MOrderPixTransaction{}
			if orderResult["pix"] != nil {
				byteData, _ := json.Marshal(orderResult["pix"])
				_ = json.Unmarshal(byteData, &orderPix)
				orderDb.PixStatus = orderPix.Status
				orderDb.PixQrCode = orderPix.QrCode
				orderDb.PixQrCodeText = orderPix.QrCodeText
				orderDb.PixPayerCpfCnpj = orderPix.PayerCpfCnpj
				orderDb.PixPayerName = orderPix.PayerName
				orderDb.PixEndToEndId = orderPix.EndToEndId
				orderDb.PixEndToEndRefundId = orderPix.EndToEndRefundId
			}
		}
	}

	hasPixTransaction := orderOlderResult["pix"]
	hasCardTransaction := orderOlderResult["credit_card_transaction"]

	if utils.StringInSlice(orders.CREDIT_CARD, orderDb.PayableWith) &&
		err == nil &&
		hasCardTransaction == nil &&
		orderDb.ChargeStatus == "" {
		iuguChargeObj := instance.createIuguChargeObj(orderDb.GetDTO())

		var resultCharge interface{}

		resultCharge, err = instance.iuguInvoice.DirectCharge(ctx, iuguChargeObj)

		orderChangeResult := resultCharge.(map[string]interface{})

		if _, ok := orderChangeResult["token"]; !ok {
			log.Printf("Error: %v", orderChangeResult["errors"])
			byteData, _ := json.Marshal(orderChangeResult["errors"])

			err = errors.New(fmt.Sprintf("order: %s", string(byteData)))
		} else if ok {
			orderDb.ChargePdf = fmt.Sprint(orderChangeResult["pdf"])
			orderDb.ChargeToken = fmt.Sprint(orderChangeResult["token"])
			orderDb.ChargeStatus = fmt.Sprint(orderChangeResult["status"])
			orderDb.ChargeMessage = fmt.Sprint(orderChangeResult["message"])
			orderDb.ChargeInfoMessage = fmt.Sprint(orderChangeResult["info_message"])
			orderDb.ChargeSuccess = fmt.Sprint(orderChangeResult["success"]) == "true"
			orderDb.LR = fmt.Sprint(orderChangeResult["LR"])
		}

		if err == nil &&
			(orderDb.LR == "00" ||
			orderDb.LR == "0" ||
			orderDb.LR == "11") {
			orderPaidResult, _ := instance.getInvoiceIuguById(ctx, orderDb.UniqueIdentifier)

			if _, ok := orderPaidResult["errors"]; !ok {
				orderDb.Status = fmt.Sprint(orderPaidResult["status"])
				orderDb.SecureUrl = fmt.Sprint(orderPaidResult["secure_url"])
				orderDb.SecureId = fmt.Sprint(orderPaidResult["secure_id"])

				orderCreditCardTransaction := MOrderCreditCardTransaction{}

				if orderOlderResult["credit_card_transaction"] != nil {
					byteData, _ := json.Marshal(orderOlderResult["credit_card_transaction"])
					_ = json.Unmarshal(byteData, &orderCreditCardTransaction)

					orderDb.ChargeStatus = orderCreditCardTransaction.Status
					orderDb.ChargeToken = orderCreditCardTransaction.Token
					orderDb.LR = orderCreditCardTransaction.LR
					orderDb.Message = orderCreditCardTransaction.Message
				}
			}
		} else if err == nil {
			orderDb.Status = "UNAUTHORIZED"
			lrItem, errDbLr := instance.serviceReturnCodes.GetByLR(ctx, orderDb.LR)

			if errDbLr == nil {
				orderDb.Message = lrItem.Description
				err = errors.New(lrItem.Description)
			}

		}
	}

	if utils.StringInSlice(orders.PIX, orderDb.PayableWith) &&
		err == nil &&
		hasPixTransaction == nil &&
		orderDb.ChargeStatus == "" {

	}

	_, errDb := instance.serviceOrder.Change(ctx, orderDb.ID.Hex(), orderDb.GetDTO())

	if errDb != nil {
		return true, errDb
	}

	if err != nil {
		return true, err
	}

	return true, nil
}

func (instance *AppSync) getInvoiceIuguById(ctx context.Context, uniqueIdentifier string) (map[string]interface{}, error) {
	resultOlder, err := instance.iuguInvoice.GetInvoiceById(ctx, uniqueIdentifier)

	if err != nil {
		return nil, err
	}

	return resultOlder.(map[string]interface{}), nil
}

func (instance *AppSync) createIuguInvoiceObj(orderDb *dtoOrders.DTOOrders) payment.IuguInvoiceBody {
	iuguOrderObj := payment.IuguInvoiceBody{}
	iuguOrderObj.CustomVariables = []payment.IuguCustomVariable{}

	iuguOrderObj.CustomVariables = append(iuguOrderObj.CustomVariables,
		payment.IuguCustomVariable{
		Name: "OwnId",
		Value: orderDb.ID.Hex(),
	})

	iuguOrderObj.CustomVariables = append(iuguOrderObj.CustomVariables,
		payment.IuguCustomVariable{
		Name: "OrderId",
		Value: orderDb.OrderId,
	})
	emailInvoice := orderDb.Email
	emailPayerInvoice := orderDb.Payer.Email

	if environment.GetTesting() == "true" {
		emailInvoice = environment.GetADMIN_EMAIL()
		emailPayerInvoice = environment.GetADMIN_EMAIL()
	}

	iuguOrderObj.Email = emailInvoice
	iuguOrderObj.DueDate = orderDb.DueDate
	iuguOrderObj.EnsureWorkdayDueDate = orderDb.EnsureWorkdayDueDate
	iuguOrderObj.ReturnUrl = orderDb.ReturnUrl
	iuguOrderObj.ExpiredUrl = orderDb.ExpiredUrl
	iuguOrderObj.NotificationUrl = orderDb.NotificationUrl
	iuguOrderObj.IgnoreCanceledEmail = orderDb.IgnoreCanceledEmail
	iuguOrderObj.Fines = orderDb.Fines
	iuguOrderObj.LatePaymentFine = orderDb.LatePaymentFine
	iuguOrderObj.LatePaymentFineCents = orderDb.LatePaymentFineCents
	iuguOrderObj.PerDayInterest = orderDb.PerDayInterest
	iuguOrderObj.PerDayInterestValue = orderDb.PerDayInterestValue
	iuguOrderObj.PerDayInterestCents = orderDb.PerDayInterestCents
	iuguOrderObj.DiscountCents = orderDb.DiscountCents
	iuguOrderObj.CustomerId = orderDb.CustomerId
	IgnoreDueEmail := orderDb.IgnoreDueEmail

	if utils.StringInSlice(orders.CREDIT_CARD, orderDb.PayableWith) {
		IgnoreDueEmail = true
	}

	iuguOrderObj.IgnoreDueEmail = IgnoreDueEmail // orderDb.IgnoreDueEmail
	iuguOrderObj.SubscriptionId = orderDb.SubscriptionId
	iuguOrderObj.PayableWith = orderDb.PayableWith
	iuguOrderObj.EarlyPaymentDiscount = orderDb.EarlyPaymentDiscount
	iuguOrderObj.MaxInstallmentsValue = 1
	iuguOrderObj.Items = make([]payment.IuguInvoiceItemBody, 0, 0)

	iuguPayerObj := payment.IuguPayer{
		CpfCnpj: orderDb.Payer.CpfCnpj,
		Name: orderDb.Payer.Name,
		PhonePrefix: orderDb.Payer.PhonePrefix,
		Phone: orderDb.Payer.Phone,
		Email: emailPayerInvoice,
	}

	iuguPayerObj.Address = payment.IuguAddress{
		ZipCode: orderDb.Payer.Address.ZipCode,
		Street: orderDb.Payer.Address.Street,
		Number: orderDb.Payer.Address.Number,
		District: orderDb.Payer.Address.District,
		City: orderDb.Payer.Address.City,
		State: orderDb.Payer.Address.State,
		Complement: orderDb.Payer.Address.Complement,
		Country: orderDb.Payer.Address.Country,
	}
	iuguOrderObj.Payer = &iuguPayerObj

	for i := 0; i < len(orderDb.Items); i++ {
		item := orderDb.Items[i]

		iuguItemObj := payment.IuguInvoiceItemBody{
			Description: item.Description,
			Quantity: item.Quantity,
			PriceCents: item.PriceCents,
		}

		iuguOrderObj.Items = append(iuguOrderObj.Items, iuguItemObj)
	}

	return iuguOrderObj
}

func (instance *AppSync) createIuguChargeObj(orderDb *dtoOrders.DTOOrders) payment.IuguChargeBody {
	iuguDirectObj := payment.IuguChargeBody{}
	iuguDirectObj.CustomerPaymentMethodId = orderDb.CreditCard
	iuguDirectObj.InvoiceId = orderDb.UniqueIdentifier

	return iuguDirectObj
}

func (instance *AppSync) processCancellation(ctx context.Context, cancellationId primitive.ObjectID) (bool, error) {
	cancellationTransactionDb, err := instance.serviceOrdersCancellation.GetById(ctx, cancellationId.Hex())

	if err != nil {
		return true, errors.New(fmt.Sprintf("%s %s", "Find-Cancelation: ", err.Error()))
	}

	orderCancellationDb, err := instance.serviceOrdersCancellation.GetByUniqueIdentifier(ctx,
		cancellationTransactionDb.UniqueIdentifier, false)

	if err != nil {
		return true, errors.New(fmt.Sprintf("%s %s", "Get-Cancellation-DB: ", err.Error()))
	}


	if orderCancellationDb == nil {
		createCancellation := dtoCancellation.NewDTOOrdersCancellation()
		createCancellation.UniqueIdentifier = cancellationTransactionDb.UniqueIdentifier
		createCancellation.Processed = false

		createCancellationDb, err := instance.serviceOrdersCancellation.Create(ctx,
			createCancellation)

		if err != nil {
			return true, errors.New(fmt.Sprintf("%s %s", "Create-Cancellation-DB: ", err.Error()))
		}

		orderCancellationDb = createCancellationDb
	}

	orderOlderResult, err := instance.getInvoiceIuguById(ctx, cancellationTransactionDb.UniqueIdentifier)
	// orderDb, err := instance.serviceOrder.GetByUniqueIdentifier(ctx, cancellationDb.UniqueIdentifier)
	message := ""
	if err != nil {
		message = fmt.Sprintf("%s %s", "Find-Order-IUGU: ", err.Error())
		_, _ = instance.serviceOrdersCancellation.UpdateToProcessed(ctx, orderCancellationDb.ID.Hex())
		_, _ = instance.serviceOrdersCancellation.UpdateErrorMessage(ctx, orderCancellationDb.ID.Hex(), message)
		return true, errors.New(message)
	}

	if _, ok := orderOlderResult["errors"]; ok {
		message = fmt.Sprintf("%s %s", "Order-IUGU-Error: ", orderOlderResult["errors"])
		_, _ = instance.serviceOrdersCancellation.UpdateToProcessed(ctx, orderCancellationDb.ID.Hex())
		_, _ = instance.serviceOrdersCancellation.UpdateErrorMessage(ctx, orderCancellationDb.ID.Hex(), message)

		return true, errors.New(message)
	}

	currentStatus := fmt.Sprint(orderOlderResult["status"])

	nextPossibleStatus := instance.serviceOrder.GetNextStatus(currentStatus)

	orderCancellationDb.CurrentStatus = currentStatus
	orderCancellationDb.FinalStatus = strings.Join(nextPossibleStatus, ",")

	_, _ = instance.serviceOrdersCancellation.UpdateStatus(ctx,
		orderCancellationDb.ID.Hex(), orderCancellationDb.CurrentStatus, orderCancellationDb.FinalStatus)

	if utils.StringInSlice(dtoOrders.REFUNDED, nextPossibleStatus) {
		err = instance.refundOrderIUGU(ctx, orderCancellationDb.ID.Hex(), orderCancellationDb.UniqueIdentifier)
		message = "Refund"
	}

	if utils.StringInSlice(dtoOrders.CANCELED, nextPossibleStatus) {
		err = instance.cancelOrderIUGU(ctx, orderCancellationDb.ID.Hex(), orderCancellationDb.UniqueIdentifier)
		message = "Cancel"
	}

	if err != nil {
		message = fmt.Sprintf("%s%s %s", message, "-Order-IUGU: ", err.Error())
		_, _ = instance.serviceOrdersCancellation.UpdateToProcessed(ctx, orderCancellationDb.ID.Hex())
		_, _ = instance.serviceOrdersCancellation.UpdateErrorMessage(ctx, orderCancellationDb.ID.Hex(), message)
		return true, errors.New(message)
	}


	log.Printf("Success %s ORDER-IUGU: %s", message, orderCancellationDb.UniqueIdentifier)
	_, _ = instance.serviceOrdersCancellation.UpdateToProcessed(ctx, orderCancellationDb.ID.Hex())

	return true, nil
}

func (instance *AppSync) cancelOrderIUGU(ctx context.Context, cancellationId string,
	uniqueIdentifier string) error {
	result, err := instance.iuguInvoice.CancelInvoice(ctx, uniqueIdentifier)

	if err != nil {
		return err
	}

	cancelResult := result.(map[string]interface{})

	if _, ok := cancelResult["errors"]; ok {
		log.Printf("Refund-IUGU-Order-Error: %v", cancelResult["errors"])
		byteData, _ := json.Marshal(cancelResult["errors"])
		return errors.New(string(byteData))
	}

	log.Printf("Cancel Result: %v", cancelResult)

	_, _ = instance.serviceOrdersCancellation.UpdateToCanceled(ctx, cancellationId)
	return nil
}

func (instance *AppSync) refundOrderIUGU(ctx context.Context, cancellationId string,
	uniqueIdentifier string) error {
	result, err := instance.iuguInvoice.RefundInvoice(ctx, uniqueIdentifier, 0)

	if err != nil {
		return err
	}

	refundResult := result.(map[string]interface{})

	if _, ok := refundResult["errors"]; ok {
		log.Printf("Refund-IUGU-Order-Error: %v", refundResult["errors"])
		byteData, _ := json.Marshal(refundResult["errors"])
		return errors.New(string(byteData))
	}

	log.Printf("Refund Result %v", refundResult)

	_, _ = instance.serviceOrdersCancellation.UpdateToRefunded(ctx, cancellationId)
	return nil
}
