package payments

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"payment_orders/adapters/db"
)

type DTOCardClientToken struct {
	db.DefaultModel `valid:"-"bson:",inline"`
	CustomerId   primitive.ObjectID `bson:"customerId" valid:"required~Código do cliente obrigatório"`
	CardToken    string `bson:"cardToken" valid:"required~Token do cartão é obrigatório"`
	Description  string `bson:"description" valid:"required~Descrição é obrigatória"`
	SetAsDefault bool `bson:"setAsDefault" valid:"optional"`
	Remove bool `bson:"remove" valid:"optional"`
	Processed bool `bson:"processed" valid:"optional"`
}

func NewDTOCardClientToken() *DTOCardClientToken {
	card := DTOCardClientToken{
		SetAsDefault: false,
		Description: "Novo Cartão",
		Remove: false,
		Processed: false,
	}
	return &card
}
