package cancellations

import "payment_orders/adapters/queue"

type AppOrdersCancellationQueue struct {
	NewMessage chan *queue.ContentMessageInstance
}

func MakeNewAppOrdersCancellationQueue() * AppOrdersCancellationQueue {
	return &AppOrdersCancellationQueue{
		NewMessage: make(chan *queue.ContentMessageInstance),
	}
}

func (instance *AppOrdersCancellationQueue) ConnectToAMQP(consumerAmqpConn queue.IRabbitConnection) error {
	config := queue.MakeOrdersCancellationRabbitMQCfg()
	consumer := queue.MakeNewConsumerFactory(consumerAmqpConn, config)
	err := consumer.CreateConsumer(config.ConsumerTag, false)
	if err != nil {
		return err
	}

	go consumer.Subscribe()

	go func() {
		for message := range consumer.NewMessage {
			instance.NewMessage <- message
			message = nil
		}
	}()

	return nil
}
